local BasePlugin = require "kong.plugins.base_plugin"
local profiling = require("kong.plugins.commons.profiling")
local clair = require("kong.plugins.commons.clair")

local trace_obj = profiling.trace_obj

local ClairHandler = BasePlugin:extend()

ClairHandler.PRIORITY = 199001
ClairHandler.VERSION = "1.0.0"

local NAME = "x-clair"

function ClairHandler:new()
  ClairHandler.super.new(self, NAME)
end

function ClairHandler:rewrite(conf)
  ClairHandler.super.rewrite(self)

  clair.updateMaxNumberOfTimers(conf.max_number_of_timers)

  local rand_num = math.random(1, 100)
  if rand_num <= (conf.shadowed_percentage or 0) then
    clair.captureRequest(kong)
  end
end

trace_obj(NAME, ClairHandler, {"rewrite"})
return ClairHandler
