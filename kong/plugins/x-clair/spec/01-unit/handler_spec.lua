package.loaded["kong.plugins.commons.profiling"] = require("kong.spec.utils.profiling-mock")

local kong_log_mock = require("kong.spec.utils.kong-log-mock")
package.loaded["kong.plugins.commons.log"] = kong_log_mock

package.loaded["kong.plugins.commons.clair"] = {}
local clair = require("kong.plugins.commons.clair")

local ClairHandler = require("kong.plugins.x-clair.handler")

describe(
  "x-clair handler",
  function()
    local handler
    before_each(
      function(...)
        handler = ClairHandler()

        -- mock clair package
        stub(clair, "captureRequest")
        stub(clair, "updateMaxNumberOfTimers")
      end
    )

    it(
      "should capture request during rewrite phase if shadowed percentage is 100%",
      function()
        local conf = {
          shadowed_percentage = 100,
          max_number_of_timers = 10
        }
        handler:rewrite(conf)

        assert.spy(clair.captureRequest).was_called()
        assert.spy(clair.updateMaxNumberOfTimers).was_called()
        assert.equal(conf.max_number_of_timers, clair.updateMaxNumberOfTimers.calls[1].vals[1])
      end
    )

    it(
      "should not capture request during rewrite phase if shadowed percentage is 0%",
      function()
        local conf = {
          shadowed_percentage = 0
        }
        handler:rewrite(conf)

        assert.spy(clair.captureRequest).was_not_called()
        assert.spy(clair.updateMaxNumberOfTimers).was_called()
      end
    )

    it(
      "should not capture request during rewrite phase if shadowed percentage is nil",
      function()
        local conf = {}
        handler:rewrite(conf)

        assert.spy(clair.captureRequest).was_not_called()
        assert.spy(clair.updateMaxNumberOfTimers).was_called()
      end
    )
  end
)
