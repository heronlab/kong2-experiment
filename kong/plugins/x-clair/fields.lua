local typedefs = require "kong.db.schema.typedefs"

local fields = {
    {
        keepalive = {
            type = "number",
            default = 60000
        }
    },
    {
        http_endpoint = typedefs.url {
            default = "http://localhost:8088/partnergwagent"
        }
    },
    {
        timeout = typedefs.timeout {
            default = 500
        }
    }
}

return fields
