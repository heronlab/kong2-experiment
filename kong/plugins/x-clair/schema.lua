-- Dependencies
local typedefs = require("kong.db.schema.typedefs")

local clair_fields = {
  {
    shadowed_percentage = {
      type = "integer",
      default = 0,
      between = {
        0,
        100
      }
    }
  },
  {
    max_number_of_timers = {
      type = "integer"
    }
  }
}
-- sets variables for other clair-ralated plugins to use.
-- decides whether or not send a current request to Clair
local ClairSchema = {
  name = "x-clair",
  {
    consumer = typedefs.no_consumer
  },
  {
    service = typedefs.no_service
  },
  {
    route = typedefs.no_route
  },
  fields = {
    {
      run_on = typedefs.run_on_first
    },
    {
      config = {
        type = "record",
        fields = clair_fields
      }
    }
  }
}

-- Export the schema
return ClairSchema
