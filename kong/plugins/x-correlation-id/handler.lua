local BasePlugin = require "kong.plugins.base_plugin"
local uuid = require "kong.tools.utils".uuid
local logger = require("kong.plugins.commons.log")

local kong = kong

local generators

do
  generators = {
    ["uuid"] = function()
      return uuid()
    end
  }
end

local CorrelationIdHandler = BasePlugin:extend()

CorrelationIdHandler.PRIORITY = 200000
CorrelationIdHandler.VERSION = "1.0.0"

function CorrelationIdHandler:new()
  CorrelationIdHandler.super.new(self, "x-correlation-id")
end

function CorrelationIdHandler:rewrite(conf)
  CorrelationIdHandler.super.rewrite(self)

  -- Set header for upstream
  local original_correlation_id = kong.request.get_header(conf.header_name)
  local correlation_id = original_correlation_id
  if not correlation_id or not conf.accept_incoming_header then
    -- Generate the header value
    correlation_id = generators[conf.generator]()
    if correlation_id then
      kong.service.request.set_header(conf.header_name, correlation_id)
    end
  end

  logger.info("map correlation id from ", original_correlation_id, " to ", correlation_id)
  if conf.echo_downstream then
    -- For later use, to echo it back downstream
    kong.ctx.plugin.correlation_id = correlation_id
  end
end

function CorrelationIdHandler:header_filter(conf)
  CorrelationIdHandler.super.header_filter(self)

  if not conf.echo_downstream then
    return
  end

  local correlation_id = kong.ctx.plugin.correlation_id
  if correlation_id then
    kong.response.set_header(conf.header_name, correlation_id)
  end
end

return CorrelationIdHandler
