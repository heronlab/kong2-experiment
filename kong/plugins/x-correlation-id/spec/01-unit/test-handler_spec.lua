local kong_log = require("kong.spec.utils.kong-log-mock")
package.loaded["kong.plugins.commons.log"] = kong_log
local match = require("luassert.match")
local kong_mock = {}
_G["kong"] = kong_mock
local uuid = "uuid_number"
package.loaded["kong.tools.utils"] = {
  uuid = function()
    return uuid
  end
}
local Handler = require("kong.plugins.x-correlation-id.handler")
describe(
  "x-correlation-id handler",
  function()
    before_each(
      function()
        kong_mock.request = {
          get_header = spy.new(
            function()
            end
          )
        }
        kong_mock.response = {
          set_header = spy.new(
            function()
            end
          )
        }
        kong_mock.service = {
          request = {
            set_header = spy.new(
              function()
              end
            )
          }
        }
        kong_mock.ctx = {
          plugin = {}
        }
      end
    )
    it(
      "should set uuid as correlation header if accept_incoming_header is false",
      function()
        local conf = {
          accept_incoming_header = false,
          header_name = "X-Request-ID",
          generator = "uuid",
          echo_downstream = true
        }
        local handler = Handler()
        handler:rewrite(conf)
        assert.spy(kong_mock.request.get_header).was_called_with(conf.header_name)
        assert.spy(kong_mock.service.request.set_header).was_called_with(conf.header_name, uuid)
        handler:header_filter(conf)
        assert.spy(kong_mock.response.set_header).was_called_with(conf.header_name, uuid)
      end
    )
    it(
      "should override correlation header if accept_incoming_header is false",
      function()
        local conf = {
          accept_incoming_header = false,
          header_name = "X-Request-ID",
          generator = "uuid",
          echo_downstream = true
        }
        local handler = Handler()
        local existing_correlation_header = "existing-correlation-header-value"
        kong_mock.request.get_header =
          spy.new(
          function()
            return existing_correlation_header
          end
        )
        handler:rewrite(conf)
        assert.spy(kong_mock.service.request.set_header).was_called_with(conf.header_name, uuid)
        assert.spy(kong_mock.service.request.set_header).was_not_called_with(
          conf.header_name,
          existing_correlation_header
        )
        handler:header_filter(conf)
        assert.spy(kong_mock.response.set_header).was_called_with(conf.header_name, uuid)
        assert.spy(kong_mock.response.set_header).was_not_called_with(conf.header_name, existing_correlation_header)
      end
    )
    it(
      "should use existing correlation id if accept_incoming_header is true",
      function()
        local conf = {
          accept_incoming_header = true,
          header_name = "X-Request-ID",
          generator = "uuid",
          echo_downstream = true
        }
        local handler = Handler()
        local existing_correlation_header = "existing-correlation-header-value"
        kong_mock.request.get_header =
          spy.new(
          function()
            return existing_correlation_header
          end
        )
        handler:rewrite(conf)
        assert.spy(kong_mock.service.request.set_header).was_not_called_with(conf.header_name, match.is_not_nil())
        handler:header_filter(conf)
        assert.spy(kong_mock.response.set_header).was_called_with(conf.header_name, existing_correlation_header)
      end
    )
    it(
      "should generate correlation id if accept_incoming_header is true, but there is no correlation id",
      function()
        local conf = {
          accept_incoming_header = true,
          header_name = "X-Request-ID",
          generator = "uuid",
          echo_downstream = true
        }
        local handler = Handler()
        handler:rewrite(conf)
        assert.spy(kong_mock.service.request.set_header).was_called_with(conf.header_name, uuid)
        handler:header_filter(conf)
        assert.spy(kong_mock.response.set_header).was_called_with(conf.header_name, uuid)
      end
    )
    it(
      "should not echo correlation id if echo_downstream is false",
      function()
        local conf = {
          header_name = "X-Request-ID",
          generator = "uuid",
          echo_downstream = false
        }
        local handler = Handler()
        handler:rewrite(conf)
        assert.spy(kong_mock.service.request.set_header).was_called_with(conf.header_name, uuid)
        handler:header_filter(conf)
        assert.spy(kong_mock.response.set_header).was_not_called_with(conf.header_name, match.is_not_nil())
      end
    )
  end
)
