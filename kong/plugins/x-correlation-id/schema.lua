local typedefs = require "kong.db.schema.typedefs"

return {
  name = "x-correlation-id",
  fields = {
    {
      consumer = typedefs.no_consumer
    },
    {
      service = typedefs.no_service
    },
    {
      route = typedefs.no_route
    },
    {
      config = {
        type = "record",
        fields = {
          {header_name = {type = "string", default = "X-Request-ID"}},
          {
            generator = {
              type = "string",
              default = "uuid",
              one_of = {"uuid"}
            }
          },
          {accept_incoming_header = {type = "boolean", default = false}},
          {echo_downstream = {type = "boolean", default = false}}
        }
      }
    }
  }
}
