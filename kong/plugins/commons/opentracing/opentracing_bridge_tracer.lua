local opentracing_bridge_tracer = {
  not_found = true
}

local ok, result = pcall(require, "opentracing_bridge_tracer")

if ok then
  opentracing_bridge_tracer = result
end

return opentracing_bridge_tracer
