describe(
  "opentracing_bridge_tracer",
  function()
    before_each(
      function(...)
        package.loaded["kong.plugins.commons.opentracing.opentracing_bridge_tracer"] = nil
      end
    )

    it(
      "should return an object with truthy not_found if opentracing_bridge_tracer is not found",
      function(...)
        local pcall_mock =
          spy.new(
          function(...)
            return false, "module not found"
          end
        )
        local old_pcall = pcall
        _G["pcall"] = pcall_mock

        local opentracing_bridge_tracer = require("kong.plugins.commons.opentracing.opentracing_bridge_tracer")

        _G["pcall"] = old_pcall

        assert.are.equal(true, opentracing_bridge_tracer.not_found)
        assert.spy(pcall_mock).was_called_with(require, "opentracing_bridge_tracer")
      end
    )

    it(
      "should return opentracing_bridge_tracer if found",
      function(...)
        local bridge_tracer_module = {
          name = "bride_tracer"
        }
        local pcall_mock =
          spy.new(
          function(...)
            return true, bridge_tracer_module
          end
        )
        local old_pcall = pcall
        _G["pcall"] = pcall_mock

        local opentracing_bridge_tracer = require("kong.plugins.commons.opentracing.opentracing_bridge_tracer")

        _G["pcall"] = old_pcall

        assert.are.equal(bridge_tracer_module, opentracing_bridge_tracer)
        assert.are.equal(nil, opentracing_bridge_tracer.not_found)
        assert.spy(pcall_mock).was_called_with(require, "opentracing_bridge_tracer")
      end
    )
  end
)
