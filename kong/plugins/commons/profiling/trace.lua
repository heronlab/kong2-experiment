local bridge_tracer = require("kong.plugins.commons.opentracing.opentracing_bridge_tracer")
local pl_table = require("pl.tablex")

-- Constants
local pack = table.pack
local unpack = table.unpack

local REFERENCES = "references"
local CHILD_OF_RELATIONSHIP = "child_of"
local START_SPAN_OPERATION = "start_span"
local FINISH_SPAN_OPERATION = "finish_span"
local ENABLE_ALL = "*"

local is_opentracing_enabled = function(opentracing_ctx)
  if not opentracing_ctx or not opentracing_ctx.binary_context or bridge_tracer.not_found then
    return false
  end

  return true
end

local start_span = function(label)
  local opentracing_ctx = kong.ctx.shared.x_opentracing
  if not is_opentracing_enabled(opentracing_ctx) then
    return
  end

  local enabled_operation_names = opentracing_ctx.enabled_operation_names or {}
  if pl_table.find(enabled_operation_names, label) == nil and pl_table.find(enabled_operation_names, ENABLE_ALL) == nil then
    return
  end

  local binary_context = opentracing_ctx.binary_context
  local track_span_actions = opentracing_ctx.track_span_actions
  local tracer = bridge_tracer.new_from_global()
  local parent_context = tracer:binary_extract(binary_context)
  local outer_span = nil
  if track_span_actions then
    outer_span = tracer:start_span(START_SPAN_OPERATION, {[REFERENCES] = {{CHILD_OF_RELATIONSHIP, parent_context}}})
  end

  local span = tracer:start_span(label, {[REFERENCES] = {{CHILD_OF_RELATIONSHIP, parent_context}}})

  if outer_span then
    outer_span:finish()
  end

  return span
end

local stop_span = function(span)
  local opentracing_ctx = kong.ctx.shared.x_opentracing
  if not is_opentracing_enabled(opentracing_ctx) or not span then
    return
  end

  local binary_context = opentracing_ctx.binary_context
  local track_span_actions = opentracing_ctx.track_span_actions
  local outer_span = nil
  if track_span_actions then
    local tracer = bridge_tracer.new_from_global()
    local parent_context = tracer:binary_extract(binary_context)
    outer_span = tracer:start_span(FINISH_SPAN_OPERATION, {[REFERENCES] = {{CHILD_OF_RELATIONSHIP, parent_context}}})
  end

  span:finish()

  if outer_span then
    outer_span:finish()
  end
end

-- trace: wrap a function with opentracing
local function trace(label, fn)
  return function(...)
    local span = start_span(label)

    local arguments = {...}
    local results = pack(fn(unpack(arguments)))

    stop_span(span)
    return unpack(results)
  end
end

-- Exports
return trace
