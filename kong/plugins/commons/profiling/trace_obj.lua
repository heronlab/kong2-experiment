-- Imports
local trace = require("kong.plugins.commons.profiling.trace")

-- trace_obj: wrap obj methods with traced replacement
local function trace_obj(label, obj, methods)
  local keys = methods and methods or {}

  for _, k in ipairs(keys) do
    if type(obj[k]) == "function" then
      obj[k] = trace(label .. ":" .. k, obj[k])
    end
  end

  return obj
end

-- Exports
return trace_obj
