-- Imports
local trace     = require("kong.plugins.commons.profiling.trace")
local trace_obj = require("kong.plugins.commons.profiling.trace_obj")

-- Exports
return {
  trace     = trace,
  trace_obj = trace_obj,
}
