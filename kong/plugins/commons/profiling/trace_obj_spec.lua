-- Imports
local profiling = require("kong.plugins.commons.profiling")

-- Constants
local trace_obj = profiling.trace_obj
local pack = table.pack
local unpack = table.unpack

_G.kong = {
  ctx = {
    shared = {}
  }
}

-- Test
describe(
  "kong:plugins:commons:profiling:trace_obj",
  function()
    it(
      "should call methods correctly",
      function()
        local fn1 =
          spy.new(
          function()
          end
        )
        local fn2 =
          spy.new(
          function()
          end
        )
        local fn3 =
          spy.new(
          function()
          end
        )

        local obj = {
          method1 = function(...)
            fn1(...)
          end,
          method2 = function(...)
            fn2(...)
          end,
          method3 = function(...)
            fn3(...)
          end
        }

        trace_obj("obj", obj, {"method1", "method2"})

        local arguments = pack(1, "a", {t = 1})
        obj.method1(unpack(arguments))
        obj.method2(unpack(arguments))
        obj.method3(unpack(arguments))

        assert.spy(fn1).was.called_with(unpack(arguments))
        assert.spy(fn2).was.called_with(unpack(arguments))
        assert.spy(fn3).was.called_with(unpack(arguments))
      end
    )

    it(
      "should only trace specified methods",
      function()
        local fn1 = function()
        end
        local fn2 = function()
        end
        local fn3 = function()
        end

        local obj = {
          method1 = fn1,
          method2 = fn2,
          method3 = fn3
        }

        trace_obj("obj", obj, {"method1", "method2"})

        assert.are_not.equals(fn1, obj.method1) -- Replaced with trace func
        assert.are_not.equals(fn2, obj.method2) -- Replaced with trace func
        assert.are.equals(fn3, obj.method3)
      end
    )

    it(
      "should leave all methods untouched if no method is specified",
      function()
        local fn1 = function()
        end

        local obj = {
          method1 = fn1
        }

        trace_obj("obj", obj)

        assert.are.equals(fn1, obj.method1)
      end
    )

    it(
      "should leave all non-function property untouched",
      function()
        local value1 = {name = "Object"}
        local value2 = 23
        local value3 = "Hello World"

        local obj = {
          value1 = value1,
          value2 = value2,
          value3 = value3,
          method1 = function()
          end,
          method2 = function()
          end
        }

        trace_obj("obj", obj)

        assert.are.equals(value1, obj.value1)
        assert.are.equals(value2, obj.value2)
        assert.are.equals(value3, obj.value3)
      end
    )
  end
)
