-- Imports
local match = require("luassert.match")

local trace_mock

package.loaded["kong.plugins.commons.opentracing.opentracing_bridge_tracer"] = {
  new_from_global = function()
    return trace_mock
  end
}
local opentracing_bridge_tracer = require("kong.plugins.commons.opentracing.opentracing_bridge_tracer")

local profiling = require("kong.plugins.commons.profiling")

-- Constants
local trace = profiling.trace
local pack = table.pack
local unpack = table.unpack

-- Test
describe(
  "kong:plugins:commons:profiling:trace",
  function()
    local label = "originalFunc"
    local span_mock
    local root_context = {
      id = 1
    }
    before_each(
      function(...)
        _G["kong"] = {
          ctx = {
            shared = {
              x_opentracing = {
                binary_context = "binary_ctx",
                enabled_operation_names = {label}
              }
            }
          }
        }
        span_mock = {
          finish = spy.new(
            function(...)
            end
          )
        }
        trace_mock = {
          start_span = spy.new(
            function(...)
              return span_mock
            end
          ),
          binary_extract = spy.new(
            function(...)
              return root_context
            end
          )
        }
        opentracing_bridge_tracer.not_found = nil
      end
    )

    it(
      "should start span, call  the original function and finish span",
      function()
        local fn =
          spy.new(
          function(...)
            return ...
          end
        )
        local tracedFn = trace(label, fn)

        local arguments = pack(1, "a", {t = 1})
        local results = pack(tracedFn(unpack(arguments)))

        assert.are.same(results, arguments)

        assert.spy(fn).was.called_with(unpack(arguments))

        -- actions on span
        assert.spy(trace_mock.start_span).was.called(1)
        assert.spy(trace_mock.binary_extract).was.called_with(
          match.is_not_nil(),
          kong.ctx.shared.x_opentracing.binary_context
        )
        assert.spy(trace_mock.start_span).was.called_with(
          match.is_not_nil(),
          label,
          {["references"] = {{"child_of", root_context}}}
        )
        assert.spy(span_mock.finish).was.called_with(match.is_not_nil())
      end
    )

    it(
      "should not create spans if x-opentracing is not enabled",
      function()
        kong.ctx.shared.x_opentracing = nil
        local tracedFn =
          trace(
          label,
          function()
          end
        )
        tracedFn()

        assert.spy(trace_mock.binary_extract).was_not_called()
        assert.spy(trace_mock.start_span).was_not_called()
        assert.spy(span_mock.finish).was_not_called()
      end
    )

    it(
      "should not create spans if opentracing cpp is not on",
      function()
        kong.ctx.shared.x_opentracing = {}
        local tracedFn =
          trace(
          label,
          function()
          end
        )
        tracedFn()

        assert.spy(trace_mock.binary_extract).was_not_called()
        assert.spy(trace_mock.start_span).was_not_called()
        assert.spy(span_mock.finish).was_not_called()
      end
    )

    it(
      "should not create spans if lua_bridge_tracer module is not_found",
      function()
        opentracing_bridge_tracer.not_found = true

        local tracedFn =
          trace(
          label,
          function()
          end
        )
        tracedFn()

        assert.spy(trace_mock.binary_extract).was_not_called()
        assert.spy(trace_mock.start_span).was_not_called()
        assert.spy(span_mock.finish).was_not_called()
      end
    )

    it(
      "should also track opentracing performance if enabled",
      function()
        local start_span_action = {
          finish = spy.new(
            function(...)
            end
          )
        }

        local finish_span_action = {
          finish = spy.new(
            function(...)
            end
          )
        }

        kong.ctx.shared.x_opentracing.track_span_actions = true
        trace_mock.start_span =
          spy.new(
          function(_, label, _)
            if label == "start_span" then
              return start_span_action
            end
            if label == "finish_span" then
              return finish_span_action
            end
            return span_mock
          end
        )
        local fn =
          spy.new(
          function(...)
            return ...
          end
        )
        local tracedFn = trace(label, fn)

        local arguments = pack(1, "a", {t = 1})
        local results = pack(tracedFn(unpack(arguments)))

        assert.are.same(results, arguments)

        assert.spy(fn).was.called_with(unpack(arguments))

        -- actions on span
        assert.spy(trace_mock.binary_extract).was.called_with(
          match.is_not_nil(),
          kong.ctx.shared.x_opentracing.binary_context
        )
        assert.spy(trace_mock.start_span).was.called_with(
          match.is_not_nil(),
          label,
          {["references"] = {{"child_of", root_context}}}
        )
        assert.spy(span_mock.finish).was.called_with(match.is_not_nil())

        assert.spy(trace_mock.start_span).was.called_with(
          match.is_not_nil(),
          "start_span",
          {["references"] = {{"child_of", root_context}}}
        )
        assert.spy(start_span_action.finish).was.called_with(match.is_not_nil())

        assert.spy(trace_mock.start_span).was.called_with(
          match.is_not_nil(),
          "finish_span",
          {["references"] = {{"child_of", root_context}}}
        )
        assert.spy(start_span_action.finish).was.called_with(match.is_not_nil())
      end
    )

    it(
      "should not create spans for disabled span names",
      function()
        local fn =
          spy.new(
          function(...)
            return ...
          end
        )
        local tracedFn = trace("a disabled span name", fn)

        local arguments = pack(1, "a", {t = 1})
        local results = pack(tracedFn(unpack(arguments)))

        assert.are.same(results, arguments)

        assert.spy(fn).was.called_with(unpack(arguments))

        -- actions on span
        assert.spy(trace_mock.binary_extract).was_not_called()
        assert.spy(trace_mock.start_span).was_not_called()
        assert.spy(span_mock.finish).was_not_called()
      end
    )

    it(
      "should work if nil enabled_operation_names",
      function()
        kong.ctx.shared.x_opentracing.enabled_operation_names = nil
        local fn =
          spy.new(
          function(...)
            return ...
          end
        )
        local tracedFn = trace("a disabled span name", fn)

        local arguments = pack(1, "a", {t = 1})
        local results = pack(tracedFn(unpack(arguments)))

        assert.are.same(results, arguments)

        assert.spy(fn).was.called_with(unpack(arguments))

        -- actions on span
        assert.spy(trace_mock.binary_extract).was_not_called()
        assert.spy(trace_mock.start_span).was_not_called()
        assert.spy(span_mock.finish).was_not_called()
      end
    )

    it(
      "should enable all span names if * is provided",
      function()
        kong.ctx.shared.x_opentracing.enabled_operation_names = {"non-existing", "*"}
        local fn =
          spy.new(
          function(...)
            return ...
          end
        )
        local tracedFn = trace(label, fn)

        local arguments = pack(1, "a", {t = 1})
        local results = pack(tracedFn(unpack(arguments)))

        assert.are.same(results, arguments)

        assert.spy(fn).was.called_with(unpack(arguments))

        -- actions on span
        assert.spy(trace_mock.start_span).was.called(1)
        assert.spy(trace_mock.binary_extract).was.called_with(
          match.is_not_nil(),
          kong.ctx.shared.x_opentracing.binary_context
        )
        assert.spy(trace_mock.start_span).was.called_with(
          match.is_not_nil(),
          label,
          {["references"] = {{"child_of", root_context}}}
        )
        assert.spy(span_mock.finish).was.called_with(match.is_not_nil())
      end
    )
  end
)
