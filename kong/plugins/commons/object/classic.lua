-- this is a wrap over kong.vender.classic because the package isn't part of Kong PDK.
local classic = require("kong.vendor.classic")

return classic
