local Noop = require("kong.plugins.commons.clair.client")
local http_implementation = require("kong.plugins.commons.clair.http.client")
local logger = require("kong.plugins.commons.log")

local implementations = {
  http = http_implementation
}

local ERRORS = {
  invalid_client_name = "invalid client name"
}

local CTX_NAME = "x-clair"
local DEFAULT_IMPL = "http"
local WATCHER_NAME = "partner-gw"
local CORRELATION_ID = "correlation-id"

-- gets identifier of current request. If a request hasn't been marked as to be sent to Clair, the function returns nil.
-- this function requires kong.ctx so it can only be used where applicable to get kong.ctx
-- @param kong kong global object.
local function getIdentifier(kong)
  local clair_ctx = kong.ctx.shared[CTX_NAME] or {}
  if clair_ctx.captured then
    return kong.request.get_header("x-request-id")
  end
end

-- marks current request as to be sent to Clair.
-- this function requires kong.ctx so it can only be used where applicable to get kong.ctx
-- @param kong kong global object.
local function captureRequest(kong)
  local clair_ctx = kong.ctx.shared[CTX_NAME] or {}
  clair_ctx.captured = true
  kong.ctx.shared[CTX_NAME] = clair_ctx
end

local function getClient(conf, name)
  local name = name or DEFAULT_IMPL

  local Impl = implementations[name]
  if Impl == nil then
    logger.warn("invalid implementation. Noop implementation is used")
    return Noop(conf), ERRORS.invalid_client_name
  end

  return Impl(conf)
end

return {
  getIdentifier = getIdentifier,
  captureRequest = captureRequest,
  getClient = getClient,
  updateMaxNumberOfTimers = http_implementation.updateMaxNumberOfTimers,
  WATCHER_NAME = WATCHER_NAME,
  CORRELATION_ID = CORRELATION_ID
}
