local Object = require "kong.plugins.commons.object.classic"

-- Clair SDK interface
local Clair = Object:extend()

-- sends track data to clair
-- @param track_data_request an object to be sent to Clair with following format
-- {
--   WatcherName string;
--   KeyValuePairs = {{
--    Key = "some key"
--    Value = "some value"
--   }}
-- }
-- @return track_data_response an object with following format
-- {
--   Message = "message";
-- }
-- @return error if failed to send data to Clair

function Clair:track(track_data_request)
end

return Clair
