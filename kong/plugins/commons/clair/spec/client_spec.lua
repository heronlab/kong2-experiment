local logger_mock = require("kong.spec.utils.kong-log-mock")
_G["kong"] = {
  log = logger_mock
}

local clair = require("kong.plugins.commons.clair")

describe(
  "clair",
  function()
    before_each(
      function(...)
      end
    )
    it(
      "newClient should return a implementation",
      function(...)
        local client, err = clair.getClient({})
        assert.not_nil(client)
        assert.is_nil(err)
      end
    )
  end
)
