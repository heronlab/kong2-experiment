local logger_mock = require("kong.spec.utils.kong-log-mock")
_G["kong"] = {
    log = logger_mock
}

local clair = require("kong.plugins.commons.clair")

describe(
    "clair",
    function()
        it(
            "captureRequest should mark current request as to be sent to Clair",
            function(...)
                local kong = {ctx = {shared = {}}}
                clair.captureRequest(kong)

                local expected_kong = {ctx = {shared = {["x-clair"] = {captured = true}}}}
                assert.are.same(expected_kong, kong)
            end
        )

        it(
            "captureRequest shouldn't override other ctx values",
            function(...)
                local kong = {ctx = {shared = {["x-clair"] = {others = "others"}, others = "others"}}}
                clair.captureRequest(kong)

                local expected_kong = {
                    ctx = {shared = {["x-clair"] = {others = "others", captured = true}, others = "others"}}
                }
                assert.are.same(expected_kong, kong)
            end
        )

        it(
            "getIdentifier should return x-request-id header if current request is marked as to be sent to Clair",
            function(...)
                local kong = {
                    ctx = {shared = {["x-clair"] = {captured = true}}},
                    request = {
                        get_header = spy.new(
                            function()
                                return "request id"
                            end
                        )
                    }
                }
                local identifier = clair.getIdentifier(kong)

                assert.are.same(kong.request.get_header("x-request-id"), identifier)
                assert.spy(kong.request.get_header).was_called()
                assert.are.same("x-request-id", kong.request.get_header.calls[1].vals[1])
            end
        )

        it(
            "getIdentifier should return nil if current request isn't marked as to be sent to Clair",
            function(...)
                local kong = {
                    ctx = {shared = {}}
                }
                local identifier = clair.getIdentifier(kong)

                assert.is_nil(identifier)
            end
        )

        it(
            "getIdentifier should return nil if current request isn't marked as to be sent to Clair",
            function(...)
                local kong = {
                    ctx = {shared = {["x-clair"] = {capture = false}}}
                }
                local identifier = clair.getIdentifier(kong)

                assert.is_nil(identifier)
            end
        )
    end
)
