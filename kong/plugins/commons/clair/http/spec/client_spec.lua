local ngx_log_mock = require("kong.spec.utils.ngx-log-mock")
local ngx_timer_mock = require("kong.spec.utils.nginx-timer-mock")
local logger_mock = require("kong.spec.utils.kong-log-mock")

package.loaded["kong.plugins.commons.clair.http.sender"] = {}
package.loaded["kong.plugins.commons.log"] = logger_mock
package.loaded["kong.plugins.commons.clair.http.sender"] = {}

_G["ngx"] = {
    log = ngx_log_mock,
    timer = {}
}
_G["kong"] = {
    log = logger_mock
}

local HttpSenderMock = require("kong.plugins.commons.clair.http.sender")

describe(
    "http client",
    function()
        local sender_mock
        before_each(
            function(...)
                ngx_timer_mock.mock(spy)
                sender_mock = {
                    send = spy.new(
                        function(...)
                            -- body
                        end
                    )
                }
                HttpSenderMock.new =
                    spy.new(
                    function(...)
                        return sender_mock
                    end
                )
            end
        )

        insulate(
            "Default test env",
            function(...)
                local Client = require("kong.plugins.commons.clair.http.client")

                before_each(
                    function(...)
                        Client.updateMaxNumberOfTimers(10)
                    end
                )

                it(
                    "should send track request to Clair",
                    function(...)
                        local conf = {
                            http_endpoint = "https://localhost:80000",
                            keepalive = true,
                            timeout = 1000
                        }
                        local client = Client(conf)

                        local track_data_request = {WatcherName = "name"}
                        local track_data_string = '{"WatcherName":"name"}'

                        client:track(track_data_request)
                        ngx.timer.runAllTimers()

                        local expected_conf = {
                            method = "POST",
                            http_endpoint = conf.http_endpoint .. "/track",
                            keepalive = true,
                            timeout = 1000
                        }
                        assert.same(expected_conf, HttpSenderMock.new.calls[1].vals[1])
                        assert.same(track_data_string, sender_mock.send.calls[1].vals[2])
                    end
                )

                it(
                    "should do nothing if the timer is not called",
                    function(...)
                        local conf = {
                            http_endpoint = "https://localhost:80000",
                            keepalive = true,
                            timeout = 1000,
                            method = "POST"
                        }
                        local client = Client(conf)

                        local track_data_request = {WatcherName = "name"}
                        client:track(track_data_request)

                        assert.spy(HttpSenderMock.new).was_not_called()
                    end
                )

                it(
                    "should call Clair asynchronously",
                    function(...)
                        local conf = {
                            http_endpoint = "https://localhost:80000",
                            keepalive = true,
                            timeout = 1000,
                            method = "POST"
                        }
                        local client = Client(conf)

                        local track_data_request = {WatcherName = "name"}
                        client:track(track_data_request)

                        assert.same(0, ngx.timer.at.calls[1].vals[1])
                        -- Should do nothing because ngx.timer.at does nothing.
                        assert.spy(HttpSenderMock.new).was_not_called()
                    end
                )
            end
        )

        insulate(
            "Test Max Number Of Timers Env1",
            function(...)
                local Client = require("kong.plugins.commons.clair.http.client")

                it(
                    "should not create more than maximum number of timers",
                    function(...)
                        ngx.timer.at =
                            spy.new(
                            function(timer, func, ...)
                                -- Do nothing.
                                return true, nil
                            end
                        )

                        Client.updateMaxNumberOfTimers(3)

                        local conf = {
                            http_endpoint = "https://localhost:80000",
                            keepalive = true,
                            timeout = 1000,
                            method = "POST"
                        }
                        local client1 = Client(conf)

                        local track_data_request = {WatcherName = "name"}
                        -- call 1
                        client1:track(track_data_request)

                        -- call 2
                        client1:track(track_data_request)

                        local client2 = Client(conf)
                        -- call 3
                        client2:track(track_data_request)

                        -- call 4, should be ignored
                        client2:track(track_data_request)

                        assert.spy(ngx.timer.at).was.called(3)
                    end
                )
            end
        )

        insulate(
            "Test Max Number Of Timers Env2",
            function(...)
                local Client = require("kong.plugins.commons.clair.http.client")

                it(
                    "should keep track number of timers",
                    function(...)
                        Client.updateMaxNumberOfTimers(1)

                        local conf = {
                            http_endpoint = "https://localhost:80000",
                            keepalive = true,
                            timeout = 1000,
                            method = "POST"
                        }
                        local client1 = Client(conf)

                        local track_data_request = {WatcherName = "name"}
                        -- call 1
                        client1:track(track_data_request)
                        ngx.timer.runAllTimers()

                        -- call 2
                        client1:track(track_data_request)
                        ngx.timer.runAllTimers()

                        assert.spy(ngx.timer.at).was.called(2)
                    end
                )
            end
        )
    end
)
