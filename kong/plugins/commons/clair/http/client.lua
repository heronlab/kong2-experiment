local Clair = require "kong.plugins.commons.clair.client"
local HttpSender = require "kong.plugins.commons.clair.http.sender"
local cjson = require("cjson")
local log = ngx.log
local ngx_timer = ngx.timer
local tablex = require("pl.tablex")

-- TODO: use partner-gw logger
local logger = kong.log

-- An implementation of Clair SDK
local HttpImpl = Clair:extend()

function HttpImpl:new(conf)
    self.conf = {
        http_endpoint = conf.http_endpoint,
        keepalive = conf.keepalive,
        timeout = conf.timeout,
        method = conf.method
    }
end

local DEFAULT_MAX_NUMBER_OF_TIMERS = 500
local MAX_NUMBER_OF_TIMER = DEFAULT_MAX_NUMBER_OF_TIMERS

local numberOfTimer = 0

local function track(premature, conf, track_data_request)
    numberOfTimer = numberOfTimer - 1
    if numberOfTimer < 0 then
        numberOfTimer = 0
    end

    if premature then
        return
    end

    local track_conf =
        tablex.union(
        conf,
        {
            method = "POST",
            http_endpoint = (conf.http_endpoint or "") .. "/track"
        }
    )

    local sender = HttpSender.new(track_conf, log)
    local data = cjson.encode(track_data_request)
    local was_sent = sender:send(data)
    if not was_sent then
        logger.err("can't send track data request")
    end
end

function HttpImpl:track(track_data_request)
    HttpImpl.super.track(self)

    if numberOfTimer >= MAX_NUMBER_OF_TIMER then
        return
    end

    local ok, err = ngx_timer.at(0, track, self.conf, track_data_request)
    if not ok then
        logger.err("failed to create timer: ", err)
    end

    numberOfTimer = numberOfTimer + 1
end

function HttpImpl.updateMaxNumberOfTimers(max_number_of_timers)
    if not max_number_of_timers then
        max_number_of_timers = DEFAULT_MAX_NUMBER_OF_TIMERS
    end

    if max_number_of_timers ~= MAX_NUMBER_OF_TIMER then
        MAX_NUMBER_OF_TIMER = max_number_of_timers
    end
end

return HttpImpl
