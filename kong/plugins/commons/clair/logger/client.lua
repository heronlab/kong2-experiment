local Clair = require "kong.plugins.commons.clair.client"
local logger = require("kong.plugins.commons.log")

-- An implementation of Clair SDK which logs all params
local LoggerImplementation = Clair:extend()

function LoggerImplementation:track(track_data_request)
  LoggerImplementation.super.track(self)
  logger.inspect("executing LoggerImplementation:track ", track_data_request)
end

return LoggerImplementation
