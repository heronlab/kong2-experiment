local kong_log_mock = require("kong.spec.utils.kong-log-mock")
package.loaded["kong.plugins.commons.log"] = kong_log_mock

local Client = require("kong.plugins.commons.clair.logger.client")

describe(
  "client",
  function()
    before_each(
      function(...)
        stub(kong_log_mock, "inspect")
      end
    )
    it(
      "should log requests",
      function(...)
        local client = Client()
        local track_request = {body = "this is request body"}
        client:track(track_request)
        assert.spy(kong_log_mock.inspect).was.called_with("executing LoggerImplementation:track ", track_request)
      end
    )
  end
)
