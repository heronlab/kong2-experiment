-- Imports
local datadog = require("kong.plugins.commons.analytics.datadog")

-- Exports
return {
  get_datadog_reporter = function()
    return datadog
  end
}
