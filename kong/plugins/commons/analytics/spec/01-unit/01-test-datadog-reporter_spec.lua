local datadog_reporter = require("kong.plugins.commons.analytics").get_datadog_reporter()

describe(
  "Datadog reporter",
  function()
    before_each(
      function()
        _G["kong"] = {
          ctx = {
            shared = {}
          }
        }
      end
    )
    test(
      "should tag requests",
      function()
        local tags = {key = "val"}
        datadog_reporter.tag_request(tags)
        assert.are.same(tags, kong.ctx.shared.x_datadog.tags)
      end
    )

    test(
      "should work if nil tags",
      function()
        kong.ctx.shared.x_datadog = {}
        local tags = {key = "val"}
        datadog_reporter.tag_request(tags)
        assert.are.same(tags, kong.ctx.shared.x_datadog.tags)
      end
    )

    test(
      "should not override exising datadog data when tagging requests",
      function()
        kong.ctx.shared.x_datadog = {
          data = "123",
          tags = {
            existing_tag = "existing_val"
          }
        }
        local tags = {key = "val"}

        datadog_reporter.tag_request(tags)

        local expected_x_datadog = {
          data = "123",
          tags = {
            existing_tag = "existing_val",
            key = "val"
          }
        }
        assert.are.same(expected_x_datadog, kong.ctx.shared.x_datadog)
      end
    )
  end
)
