local pl_table = require("pl.tablex")

local function tag_request(tags)
  local datadog_ctx = kong.ctx.shared.x_datadog or {}
  local dd_tags = datadog_ctx.tags or {}
  local new_tags = pl_table.union(dd_tags, tags)
  kong.ctx.shared.x_datadog = pl_table.union(datadog_ctx, {tags = new_tags})
end

return {
  tag_request = tag_request
}
