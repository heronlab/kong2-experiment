local kong = {}
_G["kong"] = kong

local log = require("kong.plugins.commons.log")

describe(
  "log",
  function()
    local x_request_id = "request123"

    before_each(
      function()
        kong.log = {}
        stub(kong.log, "err")
        stub(kong.log, "alert")
        stub(kong.log, "crit")
        stub(kong.log, "warn")
        stub(kong.log, "notice")
        stub(kong.log, "info")
        stub(kong.log, "debug")
        stub(kong.log, "inspect")
        stub(kong.log, "unknown")

        kong.request = {
          get_header = spy.new(
            function()
              return x_request_id
            end
          )
        }
      end
    )

    it(
      "should add correlation-id into proxied methods",
      function()
        for _, k in ipairs(log.PROXIED_METHODS) do
          local msg = "message"
          log[k](msg)
          local expected_correlation_info = "[" .. "correlation-id: " .. x_request_id .. "] "
          assert.spy(kong.log[k]).was_called_with(expected_correlation_info, msg)
        end
      end
    )

    it(
      "should not return error if can't get header from kong.request",
      function()
        kong.request.get_header =
          spy.new(
          function()
            error("can not get header")
          end
        )

        for _, k in ipairs(log.PROXIED_METHODS) do
          kong.log.err =
            spy.new(
            function()
            end
          )
          local msg = "message"

          log[k](msg)

          local expected_correlation_info = "[" .. "correlation-id: na] "
          assert.spy(kong.log[k]).was_called_with(expected_correlation_info, msg)
          assert.spy(kong.log.err).was_called()
        end
      end
    )

    it(
      "should add correlation-id info into proxied methods with context",
      function()
        local x_request_id_2 = "request_id_2"
        for _, k in ipairs(log.PROXIED_METHODS) do
          local msg = "message"
          local context = {
            request = {
              headers = {
                ["x-request-id"] = x_request_id_2
              }
            }
          }
          log[k .. log.WITH_CONTEXT_SUFFIX](context, msg)
          local expected_correlation_info = "[" .. "correlation-id: " .. x_request_id_2 .. "] "
          assert.spy(kong.log[k]).was_called_with(expected_correlation_info, msg)
        end
      end
    )

    it(
      "should call coressponding methods of kong.log even if not proxied",
      function()
        local tab = {prop = "prop msg"}
        log.inspect(tab)
        assert.spy(kong.log.inspect).was_called_with(tab)
        assert.spy(kong.log.warn).was_not_called()
      end
    )

    it(
      "should warn if unknow log method",
      function()
        local msg = "message"
        log.unknown(msg)
        assert.spy(kong.log.warn).was_called_with("Unknown log method ", "unknown")
        assert.spy(kong.log.unknown).was_called_with(msg)
      end
    )
  end
)
