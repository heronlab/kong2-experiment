-- Imports
local pl_table = require("pl.tablex")
local kong = kong

local PROXIED_METHODS = {"alert", "crit", "err", "warn", "notice", "info", "debug"}
local ALL_METHODS = pl_table.union(PROXIED_METHODS, {"inspect", "set_format"})
local WITH_CONTEXT_SUFFIX = "_with_context"
local CORRELATION_HEADER = "x-request-id"

local _mt = {
  __index = function(_, key)
    if pl_table.find(ALL_METHODS, key) == nil then
      kong.log.warn("Unknown log method ", key)
    end

    return kong.log[key]
  end
}

local logger = {
  PROXIED_METHODS = pl_table.copy(PROXIED_METHODS),
  WITH_CONTEXT_SUFFIX = WITH_CONTEXT_SUFFIX
}

local log = function(key, correlation_id, ...)
  return kong.log[key]("[correlation-id: " .. (correlation_id or "") .. "] ", ...)
end

for _, k in ipairs(PROXIED_METHODS) do
  logger[k] = function(...)
    local correlation_id = "na"
    local ok, result = pcall(kong.request.get_header, CORRELATION_HEADER)
    if not ok then
      kong.log.err("invalid use of logger. Can not get correlation header. ", result)
    else
      correlation_id = result
    end
    return log(k, correlation_id, ...)
  end
  logger[k .. WITH_CONTEXT_SUFFIX] = function(ctx, ...)
    local correlation_id = ctx.request.headers[CORRELATION_HEADER]
    return log(k, correlation_id, ...)
  end
end

return setmetatable(logger, _mt)
