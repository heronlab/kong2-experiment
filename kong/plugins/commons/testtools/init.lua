-- Imports
local validate_plugin_config = require("kong.plugins.commons.testtools.validate_plugin_config")

-- Exports
return {
  validate_plugin_config = validate_plugin_config,
}
