-- Dependencies
local Schema = require("kong.db.schema")
local Entity = require("kong.db.schema.entity")
local consumers_schema_def = require("kong.db.schema.entities.consumers")
local services_schema_def = require("kong.db.schema.entities.services")
local plugins_schema_def = require("kong.db.schema.entities.plugins")
local routes_schema_def = require("kong.db.schema.entities.routes")
local utils = require("kong.tools.utils")

-- Prepopulate Schema's cache
Schema.new(consumers_schema_def)
Schema.new(services_schema_def)
Schema.new(routes_schema_def)

-- Globals
local plugins_schema = assert(Entity.new(plugins_schema_def))

-- Testtools:validate_plugin_config
local function validate_plugin_config(config, schema)
  assert(plugins_schema:new_subschema(schema.name, schema))
  local entity = {
    id = utils.uuid(),
    name = schema.name,
    config = config
  }
  local entity_to_insert, err = plugins_schema:process_auto_fields(entity, "insert")
  if err then
    return nil, err
  end
  local _, err = plugins_schema:validate_insert(entity_to_insert)
  if err then return
    nil, err
  end
  return entity_to_insert
end

-- Export validate_plugin_config
return validate_plugin_config
