local BasePlugin = require "kong.plugins.base_plugin"
local profiling = require("kong.plugins.commons.profiling")
local CoreHandler = require("kong.plugins.x-clair-request-log-base.handler")

local trace_obj = profiling.trace_obj

local ClairHandler = BasePlugin:extend()

ClairHandler.PRIORITY = -400
ClairHandler.VERSION = "1.0.0"

local NAME = "x-clair-upstream-request-log"

function ClairHandler:new()
  ClairHandler.super.new(self, NAME)
  self._coreHandler = CoreHandler("UPSTREAM_REQUEST")
end

function ClairHandler:access(conf)
  ClairHandler.super.access(self)

  self._coreHandler:access(conf)
end

trace_obj(NAME, ClairHandler, {"access"})
return ClairHandler
