package.loaded["kong.plugins.commons.profiling"] = require("kong.spec.utils.profiling-mock")
package.loaded["kong.plugins.commons.opentracing.opentracing_bridge_tracer"] =
  require("kong.spec.utils.opentracing-bridge-tracer-mock")

local kong_log_mock = require("kong.spec.utils.kong-log-mock")
package.loaded["kong.plugins.commons.log"] = kong_log_mock

_G["kong"] = {}

package.loaded["kong.plugins.commons.clair"] = {}
local clair = require("kong.plugins.commons.clair")

local ClairHandler = require("kong.plugins.x-clair-upstream-request-log.handler")

local headers = {
  ["x-request-id"] = "x-request-id"
}

describe(
  "x-clair-upstream-request handler",
  function()
    local client_mock = {}
    local clairHandler

    before_each(
      function(...)
        clairHandler = ClairHandler("clair")

        -- mock clair package
        clair.getIdentifier = function()
          return "abc-def"
        end

        stub(client_mock, "track")
        clair.getClient = function()
          return client_mock
        end

        -- mock kong.request
        _G.kong.request = {
          get_method = function(...)
            return "GET"
          end,
          get_path = function(...)
            return "/track"
          end,
          get_raw_query = function()
            return "x=debug"
          end,
          get_raw_body = function()
            return '{"body": "body"}'
          end,
          get_headers = function()
            return headers
          end
        }
      end
    )

    it(
      "should send request to Clair during access phase",
      function()
        clairHandler:access({})

        local expected_clair_params = {
          Type = "UPSTREAM_REQUEST",
          WatcherName = clair.WATCHER_NAME,
          KeyValuePairs = {
            {
              ["Key"] = clair.CORRELATION_ID,
              ["Value"] = clair.getIdentifier(kong)
            },
            {
              ["Key"] = "method",
              ["Value"] = "GET"
            },
            {
              ["Key"] = "path",
              ["Value"] = "/track"
            },
            {
              ["Key"] = "headers",
              ["Value"] = '{"x-request-id":"x-request-id"}'
            },
            {
              ["Key"] = "body",
              ["Value"] = '{"body": "body"}'
            },
            {
              ["Key"] = "query",
              ["Value"] = "x=debug"
            }
          }
        }

        assert.spy(client_mock.track).was_called_with(match.not_nil(), match._)
        assert.are.same(expected_clair_params, client_mock.track.calls[1].vals[2])
      end
    )

    it(
      "should not send request to Clair if there is no Clair identifier",
      function()
        clair.getIdentifier = function()
          return nil
        end
        clairHandler:access({})

        assert.spy(client_mock.track).was_not_called()
      end
    )
  end
)
