local BasePlugin = require "kong.plugins.base_plugin"
local profiling = require("kong.plugins.commons.profiling")
local BaseHandler = require("kong.plugins.x-clair-response-log-base.handler")

local trace_obj = profiling.trace_obj

local ClairHandler = BasePlugin:extend()

ClairHandler.PRIORITY = -500
ClairHandler.VERSION = "1.0.0"

local NAME = "x-clair-client-response-log"

function ClairHandler:new()
  ClairHandler.super.new(self, NAME)

  self._baseHandler = BaseHandler("PARTNER_RESPONSE")
end

function ClairHandler:header_filter(conf)
  ClairHandler.super.header_filter(self)

  self._baseHandler:header_filter(conf)
end

function ClairHandler:body_filter(conf)
  ClairHandler.super.body_filter(self)

  self._baseHandler:body_filter(conf)
end

trace_obj(NAME, ClairHandler, {"header_filter", "body_filter"})
return ClairHandler
