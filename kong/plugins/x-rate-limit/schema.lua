local Errors = require "kong.db.errors"

local REDIS = "redis"

return {
  fields = {
    second = {type = "number"},
    minute = {type = "number"},
    hour = {type = "number"},
    day = {type = "number"},
    month = {type = "number"},
    year = {type = "number"},
    limit_by = {type = "string", enum = {"service", "route", "consumer", "credential", "ip", "userID", "serviceUserID"}, default = "service"},
    policy = {type = "string", enum = {"local", REDIS}, default = "local"},
    fault_tolerant = {type = "boolean", default = true},
    redis_host = {type = "string"},
    redis_port = {type = "number", default = 6379},
    redis_password = {type = "string"},
    redis_timeout = {type = "number", default = 2000},
    redis_database = {type = "number", default = 0},
    hide_client_headers = {type = "boolean", default = false}
  },
  self_check = function(schema, plugin_t, dao, is_update)
    local ordered_periods = {"second", "minute", "hour", "day", "month", "year"}
    local has_value
    local invalid_order
    local invalid_value

    for i, v in ipairs(ordered_periods) do
      if plugin_t[v] then
        has_value = true
        if plugin_t[v] ~= ngx.null then
          if plugin_t[v] <= 0 then
            invalid_value = "Value for " .. v .. " must be greater than zero"
          else
            for t = i + 1, #ordered_periods do
              if
                plugin_t[ordered_periods[t]] and plugin_t[ordered_periods[t]] ~= ngx.null and
                  plugin_t[ordered_periods[t]] < plugin_t[v]
               then
                invalid_order = "The limit for " .. ordered_periods[t] .. " cannot be lower than the limit for " .. v
              end
            end
          end
        end
      end
    end

    if not has_value then
      local err = {
        periods = "You need to set at least one limit: second, minute, hour, day, month, year"
      }
      return false, Errors:schema_violation(err)
    elseif invalid_value then
      local err = {
        periods = invalid_value
      }
      return false, Errors:schema_violation(err)
    elseif invalid_order then
      local err = {
        periods = invalid_order
      }
      return false, Errors:schema_violation(err)
    end

    if plugin_t.policy == REDIS then
      if not plugin_t.redis_host then
        local err = {
          redis_host = "You need to specify a Redis host"
        }
        return false, Errors:schema_violation(err)
      elseif not plugin_t.redis_port then
        local err = {
          redis_port = "You need to specify a Redis port"
        }
        return false, Errors:schema_violation(err)
      elseif not plugin_t.redis_timeout then
        local err = {
          redis_timeout = "You need to specify a Redis timeout"
        }
        return false, Errors:schema_violation(err)
      end
    end

    return true
  end
}
