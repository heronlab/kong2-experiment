# Change Log

* [PG-471](https://jira.grab.com/browse/PG-471) Prioritize using service/route name over service/route id when compute rate-limit identifier
* [PG-508](https://jira.grab.com/browse/PG-508) Work around to fix Rate Limit Plugin Doesn't Correctly Increase Counter In Redis (following guide from issue Github https://github.com/Kong/kong/issues/4379#issuecomment-489295396)

