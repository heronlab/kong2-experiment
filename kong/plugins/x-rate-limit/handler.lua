-- Imports
local profiling = require("kong.plugins.commons.profiling")
local policies = require("kong.plugins.x-rate-limit.policies")
local response = require("kong.plugins.utils.response")
local BasePlugin = require("kong.plugins.base_plugin")
local logger = require("kong.plugins.commons.log")
local pl_tablex = require("pl.tablex")

-- Globals
local kong = kong
local ngx = ngx
local max = math.max
local time = ngx.time
local pairs = pairs
local tostring = tostring
local timer_at = ngx.timer.at
local trace_obj = profiling.trace_obj

-- Constants
local RATELIMIT_LIMIT = "X-RateLimit-Limit"
local RATELIMIT_REMAINING = "X-RateLimit-Remaining"

-- Instantiate RateLimitHandler plugin
local RateLimitHandler = BasePlugin:extend()

-- Plugin priority and version
RateLimitHandler.PRIORITY = 903
RateLimitHandler.VERSION = "1.0.0"

local function get_identifier(conf)
  if conf.limit_by == "userID" then
    local auth_session = kong.ctx.shared.session_user
    return auth_session and auth_session["$ctx.session.UserID"]
  end

  if conf.limit_by == "serviceUserID" then
    local auth_session = kong.ctx.shared.session_user
    return auth_session and auth_session["$ctx.session.ServiceUserID"]
  end

  if conf.limit_by == "service" then
    local service = kong.router.get_service()
    return service and (service.name or service.id)
  end

  if conf.limit_by == "route" then
    local route = kong.router.get_route()
    return route and (route.name or route.id)
  end

  if conf.limit_by == "consumer" then
    local consumer = kong.client.get_consumer()
    return consumer and consumer.id
  end

  if conf.limit_by == "credential" then
    local credential = kong.client.get_credential()
    return credential and credential.id
  end

  -- conf.limit_by == "ip"
  -- use this ngx.var.remote_addr instead of kong.client.get_forwarded_ip() because the second function returns ELB ip
  return ngx.var.remote_addr
end

local function get_usage(conf, identifier, current_timestamp, limits)
  local usage = {}
  local stop

  for period, limit in pairs(limits) do
    local current_usage, err = policies[conf.policy].usage(conf, identifier, period, current_timestamp)
    if err then
      return nil, nil, err
    end

    -- What is the current usage for the configured limit name?
    local remaining = limit - current_usage

    -- Recording usage
    usage[period] = {
      limit = limit,
      remaining = remaining
    }

    if remaining <= 0 then
      stop = period
    end
  end

  return usage, stop
end

local function increment(premature, conf, ...)
  if premature then
    return
  end

  policies[conf.policy].increment(conf, ...)
end

function RateLimitHandler:new()
  RateLimitHandler.super.new(self, "x-rate-limit")
end

function RateLimitHandler:access(conf)
  RateLimitHandler.super.access(self)

  -- Extend config object with service and route object
  conf = pl_tablex.union(conf, {
    service = kong.router.get_service(),
    route = kong.router.get_route(),
  })

  -- Get current timestamp
  local current_timestamp = time() * 1000

  -- Consumer is identified by ip address or authenticated_credential id
  local identifier = get_identifier(conf)
  local fault_tolerant = conf.fault_tolerant

  -- Load current metric for configured period
  local limits = {
    second = conf.second,
    minute = conf.minute,
    hour = conf.hour,
    day = conf.day,
    month = conf.month,
    year = conf.year
  }

  if identifier == nil or identifier == "" then
    logger.err("failed to get rate limit identifier (" .. conf.limit_by .. ")")
    return
  end

  if not type(identifier) == "string" then
    logger.err(
      "invalid type of rate limit identifier (" ..
        conf.limit_by .. ') expected "string", got "' .. type(identifier) .. '"'
    )
    return
  end

  local usage, stop, err = get_usage(conf, identifier, current_timestamp, limits)
  if err then
    if fault_tolerant then
      logger.err("failed to get usage: ", tostring(err))
    else
      logger.err(err)
      return kong.response.exit(ngx.HTTP_INTERNAL_SERVER_ERROR, response.newMessage(ngx.HTTP_INTERNAL_SERVER_ERROR))
    end
  end

  if usage then
    -- Adding headers
    if not conf.hide_client_headers then
      local headers = {}
      for k, v in pairs(usage) do
        if stop == nil or stop == k then
          v.remaining = v.remaining - 1
        end

        headers[RATELIMIT_LIMIT .. "-" .. k] = v.limit
        headers[RATELIMIT_REMAINING .. "-" .. k] = max(0, v.remaining)
      end

      kong.ctx.plugin.headers = headers
    end

    -- If limit is exceeded, terminate the request
    if stop then
      return kong.response.exit(ngx.HTTP_TOO_MANY_REQUESTS, response.newMessage(nil, "API rate limit exceeded"))
    end
  end

  --kong.ctx.plugin.timer = function()
  local ok, err = timer_at(0, increment, conf, limits, identifier, current_timestamp, 1)
  if not ok then
    logger.err("failed to create timer: ", err)
  end
  --end
end

function RateLimitHandler:header_filter(_)
  RateLimitHandler.super.header_filter(self)

  local headers = kong.ctx.plugin.headers
  if headers then
    kong.response.set_headers(headers)
  end
end

--function RateLimitHandler:log(_)
--  if kong.ctx.plugin.timer then
--    kong.ctx.plugin.timer()
--  end
--end

trace_obj("x-rate-limit", RateLimitHandler, {"access"})
return RateLimitHandler
