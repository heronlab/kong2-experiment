-- Imports
local profiling = require("kong.plugins.commons.profiling")
local BaseAnalyticsPlugin = require("kong.plugins.x-base-analytics.handler")

-- Constants
local trace_obj = profiling.trace_obj

local DatadogHandler = BaseAnalyticsPlugin:extend()
DatadogHandler.PRIORITY = 10
DatadogHandler.VERSION = "0.1.0"

function DatadogHandler:new()
  DatadogHandler.super.new(self, "x-datadog")
end

trace_obj("x-datadog", DatadogHandler, {"log"})
return DatadogHandler
