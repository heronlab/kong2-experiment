package.loaded["kong.plugins.commons.profiling"] = require("kong.spec.utils.profiling-mock")

local DDHandler = require "kong.plugins.x-datadog.handler"

describe(
  "DataDog handler",
  function()
    _G.kong = {
      request = {
        get_header = spy.new(
          function()
          end
        )
      }
    }

    it(
      "should not throw exception",
      function()
        DDHandler()
      end
    )
  end
)
