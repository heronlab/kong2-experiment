local BasePlugin = require "kong.plugins.base_plugin"
local logger = require("kong.plugins.commons.log")

-- Migrator plugin is used to migrate traffic from API Gateway to the Partner Gateway on Kong

local MigratorHandler = BasePlugin:extend()

--
-- This plugin handler should be executed before other plugin handlers
-- following plugins should check kong.ctx.shared.migrated. If it is true, they should not make change to the request
--

MigratorHandler.PRIORITY = 9999
MigratorHandler.VERSION = "0.1.0"

function MigratorHandler:new()
  MigratorHandler.super.new(self, "x-migrator")
end

function MigratorHandler:access(conf)
  MigratorHandler.super.access(self)

  if math.random() >= conf.rollout_ratio then
    -- fallback
    local ok, err = kong.service.set_upstream(conf.fallback_upstream)
    if not ok then
      logger.err(err)
      return
    end
    kong.ctx.shared.is_fallback = true
  end
end

return MigratorHandler
