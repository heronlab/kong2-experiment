-- Copyright (c) 2012-2017 Grab Taxi Holdings PTE LTD (GRAB), All Rights Reserved. NOTICE: All information contained herein
-- is, and remains the property of GRAB. The intellectual and technical concepts contained herein are confidential, proprietary
-- and controlled by GRAB and may be covered by patents, patents in process, and are protected by trade secret or copyright law.

-- You are strictly forbidden to copy, download, store (in any medium), transmit, disseminate, adapt or change this material
-- in any way unless prior written permission is obtained from GRAB. Access to the source code contained herein is hereby
-- forbidden to anyone except current GRAB employees or contractors with binding Confidentiality and Non-disclosure agreements
-- explicitly covering such access.

-- The copyright notice above does not evidence any actual or intended publication or disclosure of this source code,
-- which includes information that is confidential and/or proprietary, and is a trade secret, of GRAB.

-- ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE
-- CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF GRAB IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND
-- INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY
-- OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING
-- THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

package.loaded.kong = nil

local handler = require "kong.plugins.x-migrator.handler"

describe(
  "UNITTEST - Migrator Handler",
  function()
    local old_kong = _G.kong
    local mock_config

    local snapshot
    local spy_on_set_upstream
    local stubbed_kong
    local migrator_handler

    before_each(
      function()
        snapshot = assert:snapshot()
        spy_on_set_upstream = spy.new( function(...) end)
        stubbed_kong = {
          ctx = {
            shared = {}
          },
          service = {
            set_upstream = function(upstream) 
              spy_on_set_upstream(upstream)  
              return true, nil
            end
          },
          log = {
              err = function(...) end
          }
        }
        _G.kong = setmetatable(stubbed_kong, {__index = old_kong})
        migrator_handler = handler()
      end
    )

    after_each(
      function()
        snapshot:revert()
        _G.kong = old_kong
      end
    )

    describe(
      "Test if the fallback flag is set",
      function()
        local match = require("luassert.match")
        local _ = match._
        it(
          "When the request is rolled out",
          function()
            mock_config = {
              rollout_ratio = 1
            }
            migrator_handler:access(mock_config)
            assert.is_not_true(kong.ctx.shared.is_fallback)
            
            assert.spy(spy_on_set_upstream).was_not_called_with(_) 
          end
        )
        it(
          "When the request is not rolled out",
          function()
            mock_config = {
              rollout_ratio = 0,
              fallback_upstream = "api-gateway"
            }
            migrator_handler:access(mock_config)
            assert.is_true(kong.ctx.shared.is_fallback)
            assert.spy(spy_on_set_upstream).was_called_with("api-gateway")
          end
        )
      end
    )
  end
)
