# x-migrator plugin
migrate endpoint by endpoint using percentage rollout. This involves following steps:
1. Set a global fallback route, which will fallback to api-gateway if the route is not found.(https://docs.konghq.com/0.14.x/proxy/#configuring-a-fallback-route)
2. partner start to switch to partner-api.grab.com
3. add the new route and enable the migrator plugin  for the route on Partner-GW. by default, the partner gateway still send 100% requests to GW, and 0% to the upstream service;
The migrator plugin will do the following:
    - Check if the request should be sent to upstream service. If so, go on with following plugins.
    - If not, rewrite the upstream to GW, and mark it as fallback in context;
    - If the request is marked as being fell back, the following plugins should skip processing and return;

4. change the percentage flag in the migrator plugin until 100% traffic is going to the upstream service.
5. remove the migrator plugin from the path; 