return {
  no_consumer = true,
  fields = {
    rollout_ratio = { type = "number", required = true, default = "0"},
    fallback_upstream = { type = "string", required = true, default = "api-gateway"}
  }
}