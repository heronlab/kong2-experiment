-- Imports
local cjson = require("cjson")
local client = require("kong.plugins.grabid.client")
local constants = require("kong.constants")
local singletons = require("kong.singletons")
local profiling = require("kong.plugins.commons.profiling")
local logger = require("kong.plugins.commons.log")
local get_correlation_id = require("kong.plugins.grabid.correlation_id").get_correlation_id
local pl_table = require("pl.tablex")
local cache = require "kong.plugins.grabid.cache"
local sha256 = require "kong.plugins.utils.sha256"
local kong_jwt = require ("kong.plugins.jwt.jwt_parser")

-- Constants
local trace_obj = profiling.trace_obj

local GRABID = {
  USERID = "X-Grab-ID-UserID",
  SERVICEID = "X-Grab-ID-ServiceID",
  SERVICEUSERID = "X-Grab-ID-ServiceUserID",
  CONTEXTID = "X-GRAB-ID-OAUTH2-CTX-ID",
  PARTNERID = "X-Grab-Id-Oauth2-Partner-Id",
  FORWARD_PREFIX = "X-GID-AUX-"
}

local function log_token(token)
  local logToken = token
  local j2 = kong_jwt:new(token)
  local clientID = "clientID not found"
  local partnerID = "partnerID not found"
  if j2 ~= nil then
    partnerID = j2.claims["pid"] or partnerID
    clientID = j2.claims["aud"] or clientID
  end
  if logToken:len() <= 10 then
    logToken = ""
  else
    logToken = logToken:sub(1, 10) .. "..."
  end
  logger.err("Verify OAuth2 token (POST::/v1/oauth2/verify) error: ", error, ", token: ", logToken, ", partnerID: ", partnerID, ", clientID: ", clientID)
end

local redis_prefix = 'grabid:'

local function is_nil_cache(data)
  return data == nil or data == ngx.null
end

local function set_header(key, value)
  ngx.req.set_header(key, value)
end

-- Return: isSuccess, errCode
local function set_consumer(load_consumer, partner_id)
  local cache_error = 401
  local cache_callback = function(status)
    cache_error = status
  end
  local consumer_cache_key = kong.db.consumers:cache_key(partner_id)

  local consumer, err = singletons.cache:get(consumer_cache_key, nil, load_consumer, partner_id, cache_callback)
  if err then
    return false, cache_error
  end
  if not consumer then
    logger.err("Can't find consumer with partnerID ", partner_id)
    return false, 401
  end

  local const = constants.HEADERS
  local new_headers = {
    [const.CONSUMER_ID] = consumer.id,
    [const.CONSUMER_CUSTOM_ID] = tostring(consumer.custom_id),
    [const.CONSUMER_USERNAME] = consumer.username
  }

  kong.ctx.shared.authenticated_consumer = consumer -- forward compatibility
  ngx.ctx.authenticated_consumer = consumer -- backward compatibility
  kong.service.request.set_headers(new_headers)
  return true, nil
end

local function set_partner_info(client_id, partner_id)
  kong.ctx.shared.partner = { id = partner_id, client_id = client_id }
end

local function WriteGrabIDHeader(content, headers)
  if content["userID"] then
    set_header(GRABID.USERID, content["userID"])
  end

  if content["serviceID"] then
    set_header(GRABID.SERVICEID, content["serviceID"])
  end

  if content["serviceUserID"] then
    set_header(GRABID.SERVICEUSERID, content["serviceUserID"])
  end

  if content["oauth2ContextID"] then
    set_header(GRABID.CONTEXTID, content["oauth2ContextID"])
  end

  if content["partnerID"] then
    set_header(GRABID.PARTNERID, content["partnerID"])
  end

  -- Add all header values
  for k, v in pairs(headers) do
    set_header(k, v)
  end

  kong.ctx.shared.session_user = {
    ["$ctx.session.UserID"] = content["userID"] or "",
    ["$ctx.session.ServiceID"] = content["serviceID"] or "",
    ["$ctx.session.ServiceUserID"] = content["serviceUserID"] or "",
    ["$ctx.session.RegisteredOnService"] = content["registeredOnService"] or "",
    ["$ctx.session.Oauth2ContextID"] = content["oauth2ContextID"] or "",
    ["$ctx.session.ClientID"] = content["clientID"] or "",
    ["$ctx.session.PartnerID"] = content["partnerID"] or "",
    ["$ctx.session.PartnerUserID"] = content["partnerUserID"] or "",
    ["$ctx.session.TokenID"] = content["tokenID"] or ""
  }
end

local _GrabFilter = {}

local function wrap_timer_fn(premature, fn, ...)
  if premature then
    return
  end
  return fn(...)
end

-- Returns: bool
local function should_get_cache_error(error)
  return (tonumber(error) ~= nil and error >= 500) or (error == "closed") or (error == "timeout")
end

--@function check a token is still valid (conf_ttl = 0 => check token expiry)
-- Returns: bool, error
local function is_not_expired_cache(token, conf_ttl)
  local current_time = ngx.time()
  local parsed_jwt = kong_jwt:new(token)
  if not parsed_jwt then
    return false, "invalid token"
  end
  local token_expiry = parsed_jwt.claims.exp or 0
  if (tonumber(conf_ttl) or 0) > 0 then
    token_expiry = parsed_jwt.claims.iat + tonumber(conf_ttl)
  end
  return token_expiry > current_time, nil
end

-- Return: isSuccess, errCode
local function verify_header(load_consumer, host, scope, method, token, _header, conf)
  local retries = conf.retries or 5
  if not token or #token <= 1 then
    logger.err("No token provided")
    return false, 401
  end
  -- check redis is configured (make sure this can work with existing configuration)
  local is_cache_configured = conf.redis_host ~= nil and #conf.redis_host > 0
  local token_hash = sha256.hash(token)
  local cache_key = redis_prefix .. method .. ":" .. scope .. ":" .. token_hash
  -- when fail_away flag is on, try to load cached data first.
  local cache_dat, cache_err
  local content, grab_id_headers
  local cache_checked = false
  local is_living_cache, token_err = is_not_expired_cache(token, conf.token_cache_ttl)
  if token_err then
    return false, 400
  end
  if is_cache_configured and conf.fail_away and is_living_cache then
    logger.debug('[grabid] load cache key: ', cache_key)
    cache_dat, cache_err = cache.load(conf, cache_key)
    cache_checked = true
    if cache_err then
      logger.err("failed to fetch cached data:", cache_err)
    elseif not is_nil_cache(cache_dat) then
      -- unpack data
      local unpacked = cjson.decode(cache_dat)
      content = unpacked.content
      grab_id_headers = unpacked.headers
    end
  end
  -- in case of empty `content` (fail_away is off or no cached data), call grab_id normally
  if not content then
    local _client = client:new(host, retries)
    local request_body = {
      ["oauth_token"] = token,
      ["path"] = scope,
      ["method"] = method,
      ["gateway_id"] = conf.gateway_id
    }
    local json_request_body = cjson.encode(request_body)
    local error
    content, error, grab_id_headers = _client:send(
      "/v1/oauth2/verify",
      "POST",
      json_request_body,
      _header,
      conf.alive_pool,
      conf.alive_requests,
      conf.timeout
    )
    if error then
      log_token(token)
      -- if cache wasn't hit before, try to fetch
      if is_cache_configured then
        if should_get_cache_error(error) and not cache_checked and is_living_cache then
          cache_dat, cache_err = cache.load(conf, cache_key)
          if cache_err then
            logger.err("[grabid] failed to get redis cache:", cache_err)
          elseif not is_nil_cache(cache_dat) then
            -- unpack data
            local unpacked = cjson.decode(cache_dat)
            content = unpacked.content
            grab_id_headers = unpacked.headers
          end
        elseif tonumber(error) ~= nil and tonumber(error) < 500 then
          -- invalidate cache
          ngx.timer.at(0, wrap_timer_fn, cache.del, conf, cache_key)
        end
      end

      if is_nil_cache(cache_dat) then
        return false, error
      end
    elseif is_cache_configured then
      -- call grabid successfully, write response to cache
      local payload = cjson.encode({content=content, headers=grab_id_headers})
      -- calculate expiry
      local expiry = conf.token_cache_ttl
      if tonumber(conf.token_cache_ttl) == nil or conf.token_cache_ttl <= 0 then
        local parsed_jwt = kong_jwt:new(token)
        if parsed_jwt ~= nil then
          expiry = parsed_jwt.claims.exp - ngx.time()
        end
      end
      ngx.timer.at(0, wrap_timer_fn, cache.set, conf, cache_key, payload, expiry)
    end
  end

  local partner_id = content["partnerID"]
  local client_id = content["clientID"]
  if not partner_id or not client_id then
    logger.err("No partner provided")
    return false, 401
  end

  -- Ensure we got headers from client
  if grab_id_headers == nil then
    grab_id_headers = {}
  end

  -- Send header values with prefix "X-GID-AUX-"
  local upstream_headers = {}
  for k, v in pairs(grab_id_headers) do
    if k:len() > GRABID.FORWARD_PREFIX:len() and GRABID.FORWARD_PREFIX == string.upper(string.sub(k, 1, GRABID.FORWARD_PREFIX:len())) then
      upstream_headers[k] = v
    end
  end

  WriteGrabIDHeader(content, upstream_headers)
  set_partner_info(client_id, partner_id)
  return set_consumer(load_consumer, partner_id)
end

-- Return: errCode, respMsg, respHeader
function _GrabFilter.execute(conf, load_consumer, token)
  local _method = ngx.req.get_method()
  local _headers = ngx.req.get_headers()

  local _header = {
    ["Cache-Control"] = "no-cache",
    ["Content-Type"] = "application/json",
    ["Authorization"] = "Token " .. conf.service_key .. " " .. conf.service_token
  }
  for _, pass in pairs(conf.pass_headers) do
    pass = string.upper(pass)
    for k, v in pairs(_headers) do
      k = string.upper(k)
      local pattern = string.gsub(pass, "-", "%%-")
      if string.match(k, pattern) then
        _header[k] = v
      end
    end
  end

  _header = pl_table.union(_header, get_correlation_id(_headers, conf))

  local _, err = verify_header(load_consumer, conf.host, kong.request.get_path(), _method, token, _header, conf)
  if err then
    local errCode = tonumber(err)

    -- 401
    if errCode == ngx.HTTP_UNAUTHORIZED then
      return ngx.HTTP_UNAUTHORIZED, nil, {
        ["WWW-Authenticate"] = [[Bearer error="invalid_token", error_description="invalid token"]]
      }
    end

    -- Internal error
    if errCode == nil then
      errCode = ngx.HTTP_INTERNAL_SERVER_ERROR
    end

    return errCode, nil, nil
  end

  return nil, nil, nil
end

trace_obj("oauth2", _GrabFilter, { "execute" })
return _GrabFilter
