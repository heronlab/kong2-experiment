-- Imports
local profiling = require("kong.plugins.commons.profiling")
local client = require("kong.plugins.grabid.client")
local utils = require("kong.tools.utils")
local http = require("kong.plugins.grabid.http")
local singletons = require("kong.singletons")
local logger = require("kong.plugins.commons.log")
local get_correlation_id = require("kong.plugins.grabid.correlation_id").get_correlation_id
local pl_table = require("pl.tablex")

-- Constants
local table_concat = table.concat
local trace_obj = profiling.trace_obj

local HMAC_HEADER = {
  X_GRAB_APP = "X-Grab-App",
  X_GRAB_DATE = "X-Grab-Date",
  X_GRAB_PATH = "X-Grab-Path",
  X_GRAB_VERB = "X-Grab-Verb",
  X_GRAB_CERT = "X-Grab-Signature",
  PartnerAuthStatusCodeHeader = "X-Grab-Status-Code",
  DateHeader = "Date",
  PartnerIDHeader = "X-Partner-Id",
  UserIDHeader = "X-Grab-ID-UserID",
  ServiceIDHeader = "X-Grab-ID-ServiceID",
  SignatureHeader = "X-Grab-Signature-HMAC-SHA256",
  AUTHORIZATION = "Authorization"
}

local _M = {
  VERSION = "0.1.0"
}
local function get_time()
  return os.date("!%a, %d %b %H:%M:%S GMT", os.time(os.date("!*t"))) -- Sun, 06 Nov 2016 08:49:37 GMT
end

local function set_header(key, value)
  ngx.req.set_header(key, value)
end

function _M:new()
  local data = {body = nil, method = nil, app_id = nil, signature = nil}
  self.__index = self

  return setmetatable(data, self)
end

function _M:init(header_auth, body)
  local _app
  local _signature
  local auth_parts = utils.split(header_auth, ":")
  _app = auth_parts[1]
  _signature = auth_parts[2]

  self.body = body
  self.method = ngx.req.get_method()
  self.app_id = _app
  self.signature = _signature
end

-- Return: errCode, respMsg, respHeader
function _M:verifyRequest(conf, load_consumer)
  local retries = conf.retries or 5
  local _client = client:new(conf.host, retries)
  kong.ctx.plugin.hmac_verified = false
  local _method = self.method
  local _headers = ngx.req.get_headers()
  local _partner_id = self.app_id
  local _signature = self.signature
  local _path = kong.request.get_path()
  if #kong.request.get_raw_query() > 1 then
    _path = _path .. "?" .. kong.request.get_raw_query()
  end

  --write log Date field in Header nil
  if _headers[HMAC_HEADER.DateHeader] == nil then
    logger.err("Missing required header fields 'Date' in HMAC request")
  end

  local _header = {
    ["Cache-Control"] = "no-cache",
    ["Authorization"] = "Token " .. conf.service_key .. " " .. conf.service_token,
    [HMAC_HEADER.X_GRAB_APP] = _partner_id,
    [HMAC_HEADER.X_GRAB_CERT] = _signature,
    [HMAC_HEADER.X_GRAB_DATE] = _headers[HMAC_HEADER.DateHeader],
    [HMAC_HEADER.X_GRAB_PATH] = _path,
    [HMAC_HEADER.X_GRAB_VERB] = _method,
    ["X-GRAB-GATEWAY-ID"] = conf.gateway_id,
    ["Content-Type"] = _headers["content-type"]
  }

  local _header = pl_table.union(_header, get_correlation_id(_headers, conf))

  local _, error =
  _client:send(
    "/v1/oauth2/partner/verify",
    "POST",
    self.body,
    _header,
    conf.alive_pool,
    conf.alive_requests,
    conf.timeout
  )
  if error then
    local logSignature = self.signature
    if logSignature == nil or logSignature:len() <= 10 then
      logSignature = ""
    else
      logSignature = logSignature:sub(1, 10) .. "..."
    end
    logger.err(
      "Verify HMAC token (POST::/v1/oauth2/partner/verify) error: ",
      error,
      ", partnerID: ",
      self.app_id,
      ", HMAC signature: ",
      logSignature
    )

    local errCode = tonumber(error)
    if errCode == nil then
      errCode = ngx.HTTP_INTERNAL_SERVER_ERROR
    end

    return errCode, nil, nil
  else
    kong.ctx.shared.session_user = {
      -- deprecated $ctx.session.UserID for HMAC & using $ctx.session.partnerID instead
      ["$ctx.session.UserID"] = _partner_id,
      ["$ctx.session.PartnerID"] = _partner_id,
      ["$ctx.session.ServiceUserID"] = nil
    }
  end
  local cache_error = 401
  local cache_callback = function(status)
    cache_error = status
  end
  local consumer_cache_key = kong.db.consumers:cache_key(_partner_id)
  local consumer, err = singletons.cache:get(consumer_cache_key, nil, load_consumer, _partner_id, cache_callback)
  if err then
    logger.err("Can't query against db ", err)
    return cache_error, nil, nil
  end

  kong.ctx.plugin.hmac_verified = true
  kong.ctx.shared.authenticated_consumer = consumer -- forward compatibility
  ngx.ctx.authenticated_consumer = consumer -- backward compatibility
  kong.ctx.shared.partner = {id = _partner_id, client_id = "HMAC"}

  return nil, nil, nil
end

function _M:signRequest(conf)
  local _signed_time = get_time()
  local _header = {
    [HMAC_HEADER.AUTHORIZATION] = "Token " .. conf.service_key .. " " .. conf.service_token,
    [HMAC_HEADER.X_GRAB_APP] = self.app_id,
    [HMAC_HEADER.X_GRAB_DATE] = _signed_time,
    [HMAC_HEADER.PartnerAuthStatusCodeHeader] = tonumber(ngx.status)
  }
  local ctx = ngx.ctx
  local chunk, eof = ngx.arg[1], ngx.arg[2]

  ctx.rt_body_chunks = ctx.rt_body_chunks or {}
  ctx.rt_body_chunk_number = ctx.rt_body_chunk_number or 1

  if eof then
    local body = table_concat(ctx.rt_body_chunks)
    ngx.arg[1] = body
    local http_client = http:new(conf.host)
    local res, _ = http_client:send("POST", "/v1/partner/auth/sign", body, _header)
    set_header(HMAC_HEADER.DateHeader, _signed_time)
    set_header(HMAC_HEADER.SignatureHeader, res["signature"])
  else
    ctx.rt_body_chunks[ctx.rt_body_chunk_number] = chunk
    ctx.rt_body_chunk_number = ctx.rt_body_chunk_number + 1
    ngx.arg[1] = nil
  end
end

trace_obj("hmac", _M, {"signRequest", "verifyRequest", "init"})
return _M
