local http = require("ssl.https")
local ltn12 = require("ltn12")

local _M = {}

local cjson = require "cjson"
local pcall = pcall
local function parse_json(body)
  local status, res = pcall(cjson.decode, body)
  if status then
    return res
  end
end

function _M:new(host)
  local setting = { host = "https://" .. host }
  self.__index = self
  return setmetatable(setting, self)
end

function _M:send(method, path, request_body, headers)
  local response_body = {}
  headers["Content-Length"] = #request_body
  local _, code, _ = http.request {
    url = self.host .. path,
    method = method,
    headers = headers,
    source = ltn12.source.string(request_body),
    sink = ltn12.sink.table(response_body),
  }
  if tonumber(code) ~= nil and tonumber(code) >= 400 then
    return nil, code
  end
  local content = parse_json(response_body[1])
  return content, nil
end

return _M
