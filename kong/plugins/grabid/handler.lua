--
-- Created by IntelliJ IDEA.
-- User: hoaian.nguyen
-- Date: 8/15/18
-- Time: 11:12 AM
-- To change this template use File | Settings | File Templates.
--

local BasePlugin = require "kong.plugins.base_plugin"
local filter = require "kong.plugins.grabid.oauth"
local hmac = require "kong.plugins.grabid.hmac"
local utils = require "kong.tools.utils"
local response = require "kong.plugins.utils.response"
local logger = require("kong.plugins.commons.log")
local monitor = require("kong.plugins.grabid.monitor")

local GrabIDHandler = BasePlugin:extend()
GrabIDHandler.PRIORITY = 1048
GrabIDHandler.VERSION = "0.1.2"
GrabIDHandler.hmac = nil

local function load_consumer(custom_id, cb_status)
  local generate_consumer = {
    id = custom_id,
    custom_id = custom_id,
    create_at = 0,
    username = "generate_" .. custom_id
  }
  return generate_consumer, nil
end

local load_security_strategy = {
  authorization_code = function(config, scheme_name)
    logger.info("Calling strategy: authorization_code")

    local header_auth = kong.request.get_header(scheme_name)

    local _token
    if header_auth then
      local auth_parts = utils.split(header_auth, " ")
      _token = auth_parts[2]
    end

    return filter.execute(config, load_consumer, _token)
  end,
  client_credentials = function(config, scheme_name)
    logger.info("Calling strategy: client_credentials")

    local header_auth = kong.request.get_header(scheme_name)

    local _token
    if header_auth then
      local auth_parts = utils.split(header_auth, " ")
      _token = auth_parts[2]
    end

    return filter.execute(config, load_consumer, _token)
  end,
  hmac = function(config, scheme_name)
    logger.info("Calling strategy: hmac")

    local header_auth = kong.request.get_header(scheme_name)

    if header_auth == nil then
      return kong.response.exit(ngx.HTTP_UNAUTHORIZED, response.newMessage(ngx.HTTP_UNAUTHORIZED))
    end
    local body = kong.request.get_raw_body()

    local hmac_client = hmac:new()
    hmac_client:init(header_auth, body)

    return hmac_client:verifyRequest(config, load_consumer)
  end,
  hmac_verify = function(config, scheme_name)
    logger.info("Calling strategy: hmac_verify")

    local header_auth = kong.request.get_header(scheme_name)

    local hmac_client = hmac:new()
    hmac_client:init(header_auth)
    hmac_client:signRequest(config, load_consumer)

    -- Return: code, msg, header
    return nil, nil, nil
  end
}

local function get_authentication()
  if kong.ctx.shared.x_security ~= nil then
    return kong.ctx.shared.x_security.security
  end
  return nil
end

local function get_scheme_name()
  if kong.ctx.shared.x_security ~= nil and kong.ctx.shared.x_security.scheme_name ~= nil then
    return kong.ctx.shared.x_security.scheme_name
  end
  return "Authorization"
end

local function isProbablyHMAC()
  local header_auth = kong.request.get_header(get_scheme_name())
  if header_auth == nil then
    return false
  end
  -- HMAC token has ":" character
  if string.find(header_auth, ":", 1, true) ~= nil then
    return true
  end

  return false
end

function GrabIDHandler:new()
  GrabIDHandler.super.new(self, "grabid")
end

function GrabIDHandler:access(config)
  GrabIDHandler.super.access(self)

  if kong.ctx.shared.is_fallback then
    return
  end

  -- Get auth type
  local auth = get_authentication()
  logger.info("Security type: ", auth)
  monitor.tag_request_auth(auth)

  if auth == "public" then
    -- Public
    return
  elseif auth == nil then
    -- Missing auth, error
    logger.err("missing authentication type")
    return kong.response.exit(ngx.HTTP_INTERNAL_SERVER_ERROR, response.newMessage(ngx.HTTP_INTERNAL_SERVER_ERROR), nil)
  end

  -- Missing auth token ?
  local auth_token = kong.request.get_header(get_scheme_name())
  if auth_token == nil or auth_token == "" then
    logger.err("Missing authorization header. Expected header ", get_scheme_name())
    return kong.response.exit(ngx.HTTP_UNAUTHORIZED, response.newMessage(ngx.HTTP_UNAUTHORIZED), nil)
  end

  local strategies = {}

  -- Priority HMAC
  if auth == "hmac+client_credentials" then
    -- Both "hmac" and "client_credentials"
    if isProbablyHMAC() then
      -- HMAC first
      table.insert(strategies, load_security_strategy.hmac)
      table.insert(strategies, load_security_strategy.client_credentials)
    else
      --  "client_credentials" first
      table.insert(strategies, load_security_strategy.client_credentials)
      table.insert(strategies, load_security_strategy.hmac)
    end
  else
    -- 1 auth type
    local strategy = load_security_strategy[auth]
    if strategy == nil then
      -- Not support auth type
      return
    end

    table.insert(strategies, strategy)
  end

  -- Call verify calling
  local respErrCode, respMsg, resHeaders
  for _, strategy in pairs(strategies) do
    -- Call strategy
    respErrCode, respMsg, resHeaders = strategy(config, get_scheme_name())
    if respErrCode == nil then
      -- Success
      return
    end
  end

  --  Handle error
  if respMsg == nil then
    respMsg = response.newMessage(respErrCode)
  end

  -- TODO: can't print respMsg if it's a table
  logger.err("Fail to verify request with auth=", auth, " errCode=", respErrCode, " respMsg=", tostring(respMsg))
  kong.response.exit(respErrCode, respMsg, resHeaders)
end

function GrabIDHandler:body_filter(config)
  GrabIDHandler.super.body_filter(self)

  if kong.ctx.shared.is_fallback then
    return
  end

  if kong.ctx.plugin.hmac_verified then
    load_security_strategy.hmac_verify(config, get_scheme_name())
  end
end

return GrabIDHandler
