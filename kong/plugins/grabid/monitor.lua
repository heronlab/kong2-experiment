-- Imports
local analytics = require("kong.plugins.commons.analytics")

--Create DD chart to capture 1xx, 2xx, 3xx, 4xx, 5xx status_code for GrabID
local GRABID_STATUS = {
  GRABID_STATUS_GROUP = "grabid_status_group",
  GRABID_STATUS = "grabid_status",
}

local AUTHENTICATION_TYPE = "auth_type"

local tag_request = function(status_code)
  local dd_reporter = analytics.get_datadog_reporter()
  if tonumber(status_code) ~= nil and (status_code >= 100 and status_code < 200) then
    dd_reporter.tag_request({ [GRABID_STATUS.GRABID_STATUS_GROUP] = "1xx" })

  elseif tonumber(status_code) ~= nil and (status_code >= 200 and status_code < 300) then
    dd_reporter.tag_request({ [GRABID_STATUS.GRABID_STATUS_GROUP] = "2xx" })

  elseif tonumber(status_code) ~= nil and (status_code >= 300 and status_code < 400) then
    dd_reporter.tag_request({ [GRABID_STATUS.GRABID_STATUS_GROUP] = "3xx" })

  elseif tonumber(status_code) ~= nil and (status_code >= 400 and status_code < 500) then
    dd_reporter.tag_request({ [GRABID_STATUS.GRABID_STATUS_GROUP] = "4xx" })

  elseif tonumber(status_code) ~= nil and (status_code >= 500) then
    dd_reporter.tag_request({ [GRABID_STATUS.GRABID_STATUS_GROUP] = "5xx" })
  end

  dd_reporter.tag_request({ [GRABID_STATUS.GRABID_STATUS] = status_code })

end

local tag_request_auth = function(auth_type)
  local dd_reporter = analytics.get_datadog_reporter()
  dd_reporter.tag_request({ [AUTHENTICATION_TYPE] = auth_type })
end

-- Exports
return {
  tag_request = tag_request,
  tag_request_auth = tag_request_auth
}
