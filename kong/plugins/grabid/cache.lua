local redis_util = require "kong.plugins.utils.redis_util"

--@function load cached key
--Returns: data, error
local function load(conf, key)
  local red, err, data
  red, err = redis_util.new_conn(conf)
  if err then return nil, err end
  data, err = red:get(key)
  if err then return nil, err end
  red:set_keepalive(10000, 100)
  return data, nil
end

--@function set redis data
--Returns: error
local function set(conf, key, value, expiry)
  local red, err = redis_util.new_conn(conf)
  if err then return err end
  red:set(key, value)
  if expiry > 0 then
    red:expire(key, expiry)
  end
  red:set_keepalive(10000, 100)
  return nil
end

--@function delete redis key
--Returns: error
local function del(conf, key)
  local red, err = redis_util.new_conn(conf)
  if err then return err end
  red:del(key)
  red:set_keepalive(10000, 100)
  return nil
end

return {
  load = load,
  set = set,
  del = del,
}
