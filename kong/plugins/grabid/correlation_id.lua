local function get_correlation_id(headers, conf)
  local correlation_id_header = conf.correlation_id_header or ""
  local correlation_id = headers[correlation_id_header] or ""

  if #correlation_id_header > 0 and #correlation_id > 0 then
    return {[conf.correlation_id_header] = correlation_id}
  end
  return {}
end

return {
  get_correlation_id = get_correlation_id
}
