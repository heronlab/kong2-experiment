# kong-plugin-grabid

This plugin does the following things:

- Call GrabID to verify token.
- Find consumer with custom_id that matches partnerID and set consumer info in to `kong.ctx.shared.authenticated_consumer`
- Set partner info into `kong.ctx.shared.partner`.
