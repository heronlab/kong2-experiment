package.loaded["kong.plugins.commons.profiling"] = require("kong.spec.utils.profiling-mock")
package.loaded["kong.plugins.commons.opentracing.opentracing_bridge_tracer"] =
  require("kong.spec.utils.opentracing-bridge-tracer-mock")

package.loaded.resty = nil
package.loaded.kong = nil
package.loaded.ssl = nil
package.loaded.cjson = nil
local pl_table = require("pl.tablex")

local kong_log_mock = require("kong.spec.utils.kong-log-mock")
package.loaded["kong.plugins.commons.log"] = kong_log_mock

local datadog_reporter_mock = {}
package.loaded["kong.plugins.commons.analytics"] = {
  get_datadog_reporter = function()
    return datadog_reporter_mock
  end
}

local GRABID_STATUS = {
  GRABID_STATUS_GROUP = "grabid_status_group",
  GRABID_STATUS = "grabid_status",
}

local spy_keep_alive
local httpc

-- Ended Mock Http Client

local CONFIG_TIMEOUT, CONFIG_POOL, CONFIG_ALIVE_REQUESTS = 500, 440, 220
local mock_config = {
  host = "https://grabid.com",
  header_name = "Authorization",
  service_key = "OATUTH_MANAGER",
  service_token = "TOKEN_IS",
  ignore_services_list = {"ignore-service-id-1", "ignore-service-id-2"},
  ignore_routes_list = {"/ignore"},
  timeout = CONFIG_TIMEOUT,
  alive_pool = CONFIG_POOL,
  alive_requests = CONFIG_ALIVE_REQUESTS,
  port = 443,
  gateway_id = "50775e244d674d3c9e8381f63c93d0ce",
  pass_headers = {"X-GID-AUX", "User-Agent"}
}

package.loaded["kong.request"] = {
  get_path = function(...)
    return "/authenticated"
  end,
  get_raw_query = function()
    return "x=debug"
  end
}

-- MOCK Kong Cache
local kong_cache = {
  get = function(...)
    local arg = {...}
    local loader = arg[4]
    local cache_calback = arg[6]
    return loader(arg[5], cache_calback)
  end
}

package.loaded["kong.singletons"] = {
  cache = kong_cache
}
-- END Mock Kong Cache
package.loaded["resty.http"] = {
  new = function()
    return httpc
  end
}
package.loaded["ssl.https"] = {
  request = function(...)
    return '{"signature":"xxx"}', 200
  end
}

local mock_cjson_response
package.loaded["cjson"] = {
  decode = function(...)
    return mock_cjson_response(...)
  end
}
local spy_on_set_header
local spy_on_response
local plugin_handler = require "kong.plugins.grabid.handler"

local nginx_status = "200"
describe(
  "UNITTEST - Handler",
  function()
    local old_ngx = _G.ngx
    local old_kong = _G.kong

    local stubbed_kong
    local stubbed_ngx

    local grab_handler
    local consumer_mock = {
      select_by_custom_id = function()
        return {}
      end,
      cache_key = function()
        return "cache-key-kong"
      end
    }

    local unauthorized_error_message = {
      message = "Unauthorized"
    }

    local headers = {
      ["Authorization"] = "appId:Signature",
      ["content-type"] = "application/json",
      ["Date"] = "Sun, 06 Nov 2016 08:49:37 GMT"
    }

    local expected_headers_to_grab_id = {
      ["Authorization"] = "Token OATUTH_MANAGER TOKEN_IS",
      ["Cache-Control"] = "no-cache",
      ["X-Grab-App"] = "appId",
      ["X-Grab-Date"] = "Sun, 06 Nov 2016 08:49:37 GMT",
      ["X-Grab-Path"] = "/authenticated?x=debug",
      ["X-Grab-Verb"] = "GET",
      ["X-Grab-Signature"] = "Signature",
      ["X-GRAB-GATEWAY-ID"] = "50775e244d674d3c9e8381f63c93d0ce",
      ["Content-Type"] = "application/json"
    }

    before_each(
      function()
        local response_200

        spy_keep_alive =
          spy.new(
          function(...)
          end
        )
        spy.on(consumer_mock, "select_by_custom_id")
        spy_on_set_header =
          spy.new(
          function(...)
          end
        )
        spy_on_response =
          spy.new(
          function(...)
          end
        )
        stubbed_ngx = {
          arg = {"upstream-content", true},
          req = {
            get_method = function(...)
              return "GET"
            end,
            get_headers = function()
              return pl_table.copy(headers)
            end,
            set_header = function(...)
              spy_on_set_header(...)
            end
          },
          ctx = {
            route = {
              host = nil,
              id = "5adb6b69-304b-4e5b-9777-0b72f5b3f035",
              methods = {"GET", "POST", "PUT"},
              paths = {"/authenticated"},
              service = {id = "92cb26b2-3663-4e30-b918-6bafd783ab39"}
            },
            req_uri = "/authenticated",
            service = {
              name = "grabpay"
            }
          },
          status = nginx_status
        }
        stubbed_kong = {
          db = {
            consumers = consumer_mock
          },
          request = {
            get_path = function(...)
              return "/authenticated"
            end,
            get_raw_query = function()
              return "x=debug"
            end,
            get_raw_body = function()
              return '{"name": "request"}'
            end,
            get_header = function(key)
              local headers = {["Authorization"] = "appId:Signature"}
              return headers[key]
            end
          },
          response = {
            exit = function(...)
              spy_on_response(...)
            end
          },
          ctx = {
            shared = {
              x_security = {
                security = "hmac"
              }
            },
            plugin = {}
          }
        }
        mock_cjson_response = function()
          return {valid = true}
        end
        _G.ngx = setmetatable(stubbed_ngx, {__index = old_ngx})

        _G.kong = setmetatable(stubbed_kong, {__index = old_kong})

        grab_handler = plugin_handler()

        response_200 = {
          status = 200,
          read_body = function(...)
            return [[]]
          end
        }
        httpc = {
          keepalive = true,
          set_timeout = function(...)
          end,
          connect = function(...)
          end,
          ssl_handshake = spy.new(
            function(...)
              return true, nil
            end
          ),
          request = spy.new(
            function()
              return response_200, nil
            end
          ),
          set_keepalive = function(...)
            spy_keep_alive(...)
            return 1, nil
          end
        }
        stub(datadog_reporter_mock, "tag_request")
      end
    )

    describe(
      "Unit: Test Client",
      function()
        local match = require("luassert.match")
        local _ = match._
        it(
          "fallback requests",
          function()
            stubbed_kong.ctx.shared.is_fallback = true

            grab_handler:access(mock_config)

            assert.spy(httpc.request).was.called(0)
            assert.spy(httpc.ssl_handshake).was.called(0)
          end
        )

        it(
          "should return 401 if missing authorization header",
          function()
            kong.request.get_header =
              spy.new(
              function(headerName)
                if headerName == "Authorization" then
                  return nil
                end
              end
            )

            grab_handler:access(mock_config)

            assert.spy(spy_on_response).was_called_with(401, unauthorized_error_message, nil)
            assert.spy(httpc.request).was.called(0)
          end
        )
        it(
          "GRABID verify request correctly and return empty response",
          function()
            local consumer = {id = "consumer_uuid", custom_id = "custom_id", username = "user_name"}
            consumer_mock.select_by_custom_id =
              spy.new(
              function()
                return consumer
              end
            )
            grab_handler:access(mock_config)

            -- deprecated $ctx.session.UserID for HMAC & using $ctx.session.partnerID instead
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.UserID"], "appId")
            assert.spy(httpc.request).was.called(1)
            assert.spy(httpc.request).was_called_with(
              match.is_not_nil(),
              {
                path = "/v1/oauth2/partner/verify",
                method = "POST",
                body = '{"name": "request"}',
                headers = expected_headers_to_grab_id,
                keepalive_timeout = CONFIG_TIMEOUT,
                keepalive_pool = CONFIG_POOL,
                keepalive_requests = CONFIG_ALIVE_REQUESTS
              }
            )
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.PartnerID"], "appId")
            assert.spy(spy_keep_alive).was.called(1)
            assert.are.same(kong.ctx.plugin.hmac_verified, true)
          end
        )
        it(
          "Success HMAC verify with security = 'hmac+client_credentials'",
          function()
            kong.ctx.shared.x_security.security = "hmac+client_credentials"

            local consumer = {id = "consumer_uuid", custom_id = "custom_id", username = "user_name"}
            consumer_mock.select_by_custom_id =
              spy.new(
              function()
                return consumer
              end
            )
            grab_handler:access(mock_config)

            -- deprecated $ctx.session.UserID for HMAC & using $ctx.session.partnerID instead
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.UserID"], "appId")
            assert.spy(httpc.request).was.called(1)
            assert.spy(httpc.request).was_called_with(
              match.is_not_nil(),
              {
                path = "/v1/oauth2/partner/verify",
                method = "POST",
                body = '{"name": "request"}',
                headers = expected_headers_to_grab_id,
                keepalive_timeout = CONFIG_TIMEOUT,
                keepalive_pool = CONFIG_POOL,
                keepalive_requests = CONFIG_ALIVE_REQUESTS
              }
            )
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.PartnerID"], "appId")
            assert.spy(spy_keep_alive).was.called(1)
            assert.are.same(kong.ctx.plugin.hmac_verified, true)
            assert.spy(spy_on_response).was.was_not_called_with(401, unauthorized_error_message, nil)
          end
        )

        it(
          "GrabID verified failed",
          function()
            kong.ctx.plugin.hmac_verified = false
            httpc.request =
              spy.new(
              function()
                return nil, 400
              end
            )
            grab_handler:access(mock_config)
            assert.spy(spy_on_response).was.called(1)
            assert.spy(spy_on_response).was.called_with(401, unauthorized_error_message, nil)
            assert.spy(spy_keep_alive).was.called(1)
            assert.are.same(kong.ctx.plugin.hmac_verified, false)
          end
        )

        it(
          "should send correlation id header if configured",
          function()
            local config = pl_table.union(mock_config, {correlation_id_header = "X-Request-ID2"})
            local correlation_header = "123-456"

            ngx.req.get_headers = function(...)
              return pl_table.union(headers, {[config.correlation_id_header] = correlation_header})
            end

            grab_handler:access(config)

            local expected_headers =
              pl_table.union(expected_headers_to_grab_id, {[config.correlation_id_header] = correlation_header})
            assert.spy(httpc.request).was_called_with(
              match.is_not_nil(),
              {
                path = "/v1/oauth2/partner/verify",
                method = "POST",
                body = '{"name": "request"}',
                headers = expected_headers,
                keepalive_timeout = CONFIG_TIMEOUT,
                keepalive_pool = CONFIG_POOL,
                keepalive_requests = CONFIG_ALIVE_REQUESTS
              }
            )
          end
        )

        it(
          "should not send correlation id header if correlation id header config is empty",
          function()
            local config = pl_table.union(mock_config, {correlation_id_header = ""})
            local correlation_header = "123-456"

            ngx.req.get_headers = function(...)
              return pl_table.union(headers, {[config.correlation_id_header] = correlation_header})
            end

            grab_handler:access(config)

            assert.spy(httpc.request).was_called_with(
              match.is_not_nil(),
              {
                path = "/v1/oauth2/partner/verify",
                method = "POST",
                body = '{"name": "request"}',
                headers = expected_headers_to_grab_id,
                keepalive_timeout = CONFIG_TIMEOUT,
                keepalive_pool = CONFIG_POOL,
                keepalive_requests = CONFIG_ALIVE_REQUESTS
              }
            )
          end
        )

        it(
          "should not send correlation id header if empty correlation id",
          function()
            local config = pl_table.union(mock_config, {correlation_id_header = "X-Request-ID2"})
            ngx.req.get_headers = function(...)
              return pl_table.union(headers, {[config.correlation_id_header] = ""})
            end
            grab_handler:access(config)

            assert.spy(httpc.request).was_called_with(
              match.is_not_nil(),
              {
                path = "/v1/oauth2/partner/verify",
                method = "POST",
                body = '{"name": "request"}',
                headers = expected_headers_to_grab_id,
                keepalive_timeout = CONFIG_TIMEOUT,
                keepalive_pool = CONFIG_POOL,
                keepalive_requests = CONFIG_ALIVE_REQUESTS
              }
            )
          end
        )

        it(
          "should not send correlation id header if nil correlation id",
          function()
            local config = pl_table.union(mock_config, {correlation_id_header = "X-Request-ID2"})
            grab_handler:access(config)

            assert.spy(httpc.request).was_called_with(
              match.is_not_nil(),
              {
                path = "/v1/oauth2/partner/verify",
                method = "POST",
                body = '{"name": "request"}',
                headers = expected_headers_to_grab_id,
                keepalive_timeout = CONFIG_TIMEOUT,
                keepalive_pool = CONFIG_POOL,
                keepalive_requests = CONFIG_ALIVE_REQUESTS
              }
            )
          end
        )

        it(
          "GRABID signed request",
          function()
            local mock_config = {
              host = "https://grabid.com",
              header_name = "Authorization",
              service_key = "OATUTH_MANAGER",
              service_token = "TOKEN_IS",
              ignore_services_list = {"ignore-service-id-1", "ignore-service-id-2"},
              ignore_routes_list = {"/authenticated"},
              gateway_id = "50775e244d674d3c9e8381f63c93d0ce"
            }
            kong.ctx.plugin.hmac_verified = true
            mock_cjson_response = function(...)
              return {["signature"] = "MOCK_SIGNATURE"}
            end
            grab_handler:body_filter(mock_config)

            assert.spy(spy_on_set_header).was_called_with("X-Grab-Signature-HMAC-SHA256", "MOCK_SIGNATURE")
          end
        )

        it(
          "fallback request, should not sign it",
          function()
            stubbed_kong.ctx.shared.is_fallback = true
            local mock_config = {
              host = "https://grabid.com",
              header_name = "Authorization",
              service_key = "OATUTH_MANAGER",
              service_token = "TOKEN_IS",
              ignore_services_list = {"ignore-service-id-1", "ignore-service-id-2"},
              ignore_routes_list = {"/authenticated"},
              gateway_id = "50775e244d674d3c9e8381f63c93d0ce"
            }
            kong.ctx.plugin.hmac_verified = true
            mock_cjson_response = function(...)
              return {["signature"] = "MOCK_SIGNATURE"}
            end
            grab_handler:body_filter(mock_config)

            assert.spy(spy_on_set_header).was_not_called_with("X-Grab-Signature-HMAC-SHA256", "MOCK_SIGNATURE")
          end
        )

        it(
          "shouldn't add header values with prefix 'X-GID-AUX-' from GrabID when security = 'hmac'",
          function()
            kong.ctx.shared.x_security.security = "hmac"

            httpc.request =
            spy.new(
              function(...)
                local response_200 = {
                  status = 200,
                  read_body = function(...)
                    return [[]]
                  end,
                  ["headers"] = {
                    ["X-GID-AUX-TEST"] = "valid",
                    ["X-GID-INVALID-TEST"] = "invalid"
                  }
                }
                return response_200, nil
              end
            )

            grab_handler:access(mock_config)
            assert.spy(spy_on_set_header).was_not.called_with("X-GID-AUX-TEST", "valid")
            assert.spy(spy_on_set_header).was_not.called_with("X-GID-INVALID-TEST", "invalid")
          end
        )

        it(
          "should verify GrabID timeout & send grabid_status_group:5xx, grabid_status:504 to datadog",
          function()
            kong.ctx.plugin.hmac_verified = false
            httpc.request =
            spy.new(
              function()
                local response_504 = {
                  status = 504,
                  read_body = function(...)
                    return [[]]
                  end
                }
                return response_504, 504
              end
            )
            grab_handler:access(mock_config)
            assert.spy(datadog_reporter_mock.tag_request).was.called_with({[GRABID_STATUS.GRABID_STATUS_GROUP] = "5xx"})
            assert.spy(datadog_reporter_mock.tag_request).was.called_with({[GRABID_STATUS.GRABID_STATUS]= 504})
          end
        )

        it(
          "should verify GrabID ssl timeout & send grabid_status_group:5xx, grabid_status:504 to datadog",
          function()
            kong.ctx.plugin.hmac_verified = false
            httpc.ssl_handshake =
            spy.new(
              function()
                local ssl = {
                  status = 200
                }
                return ssl, "timeout"
              end
            )
            grab_handler:access(mock_config)
            assert.spy(datadog_reporter_mock.tag_request).was.called_with({[GRABID_STATUS.GRABID_STATUS_GROUP] = "5xx"})
            assert.spy(datadog_reporter_mock.tag_request).was.called_with({[GRABID_STATUS.GRABID_STATUS]= 504})
          end
        )

        it(
          "should verify GrabID ssl connection timeout & send grabid_status_group:5xx, grabid_status:599 to datadog",
          function()
            kong.ctx.plugin.hmac_verified = false
            httpc.ssl_handshake =
            spy.new(
              function()
                return nil, nil
              end
            )
            grab_handler:access(mock_config)
            assert.spy(datadog_reporter_mock.tag_request).was.called_with({[GRABID_STATUS.GRABID_STATUS_GROUP] = "5xx"})
            assert.spy(datadog_reporter_mock.tag_request).was.called_with({[GRABID_STATUS.GRABID_STATUS]= 599})
          end
        )

        it(
          "should verify GrabID unauthorized 4xx & send grabid_status_group:4xx, grabid_status:401 to datadog",
          function()
            kong.ctx.plugin.hmac_verified = false
            httpc.request =
            spy.new(
              function()
                local response_401 = {
                  status = 401,
                  read_body = function(...)
                    return [[]]
                  end
                }
                return response_401, 401
              end
            )
            grab_handler:access(mock_config)
            assert.spy(datadog_reporter_mock.tag_request).was.called_with({[GRABID_STATUS.GRABID_STATUS_GROUP] = "4xx"})
            assert.spy(datadog_reporter_mock.tag_request).was.called_with({[GRABID_STATUS.GRABID_STATUS]= 401})
          end
        )

        it(
          "should verify GrabID redirection 3xx & send grabid_status_group:3xx, grabid_status:300 to datadog",
          function()
            kong.ctx.plugin.hmac_verified = false
            httpc.request =
            spy.new(
              function()
                local response_300 = {
                  status = 300,
                  read_body = function(...)
                    return [[]]
                  end
                }
                return response_300, 300
              end
            )
            grab_handler:access(mock_config)
            assert.spy(datadog_reporter_mock.tag_request).was.called_with({[GRABID_STATUS.GRABID_STATUS_GROUP] = "3xx"})
            assert.spy(datadog_reporter_mock.tag_request).was.called_with({[GRABID_STATUS.GRABID_STATUS]= 300})
          end
        )

        it(
          "should verify GrabID success 2xx & send send grabid_status_group:2xx, grabid_status:200 to datadog",
          function()
            kong.ctx.plugin.hmac_verified = false
            httpc.request =
            spy.new(
              function()
                local response_200 = {
                  status = 200,
                  read_body = function(...)
                    return [[]]
                  end
                }
                return response_200, 200
              end
            )
            grab_handler:access(mock_config)
            assert.spy(datadog_reporter_mock.tag_request).was.called_with({[GRABID_STATUS.GRABID_STATUS_GROUP] = "2xx"})
            assert.spy(datadog_reporter_mock.tag_request).was.called_with({[GRABID_STATUS.GRABID_STATUS]= 200})
          end
        )

        it(
          "expect send auth_type:hmac to datadog",
          function()
            httpc.request = spy.new(
              function()
                local response_200 = {
                  status = 200,
                  read_body = function(...)
                    return [[]]
                  end
                }
                return response_200, 200
              end
            )
            grab_handler:access(mock_config)

            assert.spy(datadog_reporter_mock.tag_request).was.called_with({["auth_type"]= "hmac"})
          end
        )

      end
    )
  end
)
