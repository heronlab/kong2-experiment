local match = require("luassert.match")
local pl_table = require("pl.tablex")
local tracer = require("kong.spec.utils.tracer-mock")

package.loaded["kong.plugins.commons.profiling"] = require("kong.spec.utils.profiling-mock")

local tracer_mock = {}
package.loaded["kong.plugins.commons.opentracing.opentracing_bridge_tracer"] = {
  new_from_global = function()
    return tracer_mock
  end
}
local opentracing_bridge_tracer = require("kong.plugins.commons.opentracing.opentracing_bridge_tracer")

local kong_log_mock = require("kong.spec.utils.kong-log-mock")
package.loaded["kong.plugins.commons.log"] = kong_log_mock

local httpc
local spy_on_keep_alive

package.loaded["kong.request"] = {
  get_path = function(...)
    return "/authenticated"
  end,
  get_raw_query = function()
    return "x=debug"
  end
}

package.loaded["resty.http"] = {
  new = function()
    return httpc
  end
}

local client = require "kong.plugins.grabid.client"

describe(
  "UNITTEST - Handler",
  function()
    local old_kong = _G.kong

    local stubbed_kong

    local _client

    local user_id = "40cf2a40-e64a-408a-adea-59f92a410905"

    local response_200 = {
      status = 200,
      body = '{"message": "OK"}',
      read_body = function(...)
        return [[
            {"userID":"]] ..
          user_id ..
            [[",
             "serviceID":"PASSENGER",
             "serviceUserID":1,
             "registeredOnService":true,
             "oauth2ContextID":"4a6c400b613c439ebca58104aa484039",
             "clientID": "clientA",
             "partnerID": "partnerA"
            }
            ]]
      end
    }

    local parent_context = {id = "1"}

    before_each(
      function()
        spy_on_keep_alive =
          spy.new(
          function(...)
          end
        )
        stubbed_kong = {
          ctx = {
            shared = {
              x_opentracing = {
                binary_context = {
                  prop = 1
                }
              }
            }
          }
        }
        _G.kong = setmetatable(stubbed_kong, {__index = old_kong})
        _client = client:new("grab-id.my-teksi.com", 5)
        package.loaded["cjson"] = {
          decode = function(...)
            return {["signature"] = "MOCK_SIGNATURE"}
          end
        }

        tracer_mock = pl_table.copy(tracer)

        httpc = {
          keepalive = true,
          set_timeout = function(...)
          end,
          connect = function(...)
          end,
          set_keepalive = function(...)
            spy_on_keep_alive(...)
            return 1, nil
          end,
          ssl_handshake = function()
            return true, nil
          end,
          request = spy.new(
            function()
              return response_200, nil
            end
          )
        }

        tracer_mock.http_headers_inject =
          spy.new(
          function(_, _, carrier)
            carrier["x-opentracing-id"] = "1"
          end
        )

        tracer_mock.binary_extract =
          spy.new(
          function(...)
            return parent_context
          end
        )
        opentracing_bridge_tracer.not_found = nil
      end
    )

    describe(
      "Unit: Test Client",
      function()
        it(
          "Able to receive request reflect mock",
          function()
            local res, err = _client:send("/v1", "POST", [[{"null":"body"}]], {}, 1, 1, 1)
            assert.are.same(user_id, res.userID)
            assert.are.same(nil, err)
            assert.spy(spy_on_keep_alive).was.called(1)
          end
        )
        it(
          "Retried 5 times and receive timeout error",
          function()
            httpc.request =
              spy.new(
              function()
                return nil, "timeout"
              end
            )
            local res, err = _client:send("/v1", "POST", [[{"null":"body"}]], {}, 1, 1, 1)
            assert.are.same("timeout", err)
            assert.are.same(nil, res)
            assert.spy(httpc.request).was.called(5)
            assert.spy(spy_on_keep_alive).was.called(5)
          end
        )
        it(
          "Retried 1 time and get 400 from GrabID and receive 401 error",
          function()
            httpc.request =
              spy.new(
              function()
                return nil, 400
              end
            )
            local res, err = _client:send("/v1", "POST", [[{"null":"body"}]], {}, 1, 1, 1)
            assert.are.same(401, err)
            assert.are.same(nil, res)
            assert.spy(httpc.request).was.called(1)
            assert.spy(spy_on_keep_alive).was.called(1)
          end
        )
        it(
          "Retried 1 time and receive 500 error",
          function()
            httpc.request =
              spy.new(
              function()
                return nil, 500
              end
            )
            local res, err = _client:send("/v1", "POST", [[{"null":"body"}]], {}, 1, 1, 1)
            assert.are.same(500, err)
            assert.are.same(nil, res)
            assert.spy(httpc.request).was.called(1)
            assert.spy(spy_on_keep_alive).was.called(1)
          end
        )
        it(
          "Retried 5 time and ssh_handshake timeout error, then keep_alive should not call anytime",
          function()
            httpc.ssl_handshake =
              spy.new(
              function()
                return false, "timeout"
              end
            )
            local res, err = _client:send("/v1", "POST", [[{"null":"body"}]], {}, 1, 1, 1)
            assert.are.same("timeout", err)
            assert.are.same(nil, res)
            assert.spy(httpc.ssl_handshake).was.called(5)
            assert.spy(spy_on_keep_alive).was.called(0)
          end
        )
        it(
          "should inject http headers into requests sent to grabid",
          function()
            -- Set up mocks
            local request = {
              path = "/v1",
              method = "POST",
              body = [[{"ab":"body"}]],
              headers = {["content-type"] = "application/json"},
              keepalive_timeout = 1,
              keepalive_pool = 2,
              keepalive_requests = 3
            }

            -- execute
            local _, err =
              _client:send(
              request.path,
              request.method,
              request.body,
              {["content-type"] = "application/json"},
              request.keepalive_pool,
              request.keepalive_requests,
              request.keepalive_timeout
            )

            -- assert
            assert.is_nil(err)
            assert.spy(tracer_mock.binary_extract).was_called_with(
              match.is_not_nil(),
              kong.ctx.shared.x_opentracing.binary_context
            )
            assert.spy(tracer_mock.http_headers_inject).was_called_with(
              match.is_not_nil(),
              parent_context,
              match.is_not_nil()
            )
            local expected_headers = {
              ["content-type"] = "application/json",
              ["x-opentracing-id"] = "1"
            }
            assert.spy(httpc.request).was_called_with(
              match.is_not_nil(),
              {
                path = request.path,
                method = request.method,
                body = request.body,
                keepalive_timeout = request.keepalive_timeout,
                keepalive_pool = request.keepalive_pool,
                keepalive_requests = request.keepalive_requests,
                headers = expected_headers
              }
            )
          end
        )
        it(
          "should not inject opentracing header if opentracing is disabled",
          function(...)
            kong.ctx.shared.x_opentracing.binary_context = nil
            local res, err = _client:send("/v1", "POST", [[{"null":"body"}]], {}, 1, 1, 1)
            assert.are.same(user_id, res.userID)
            assert.are.same(nil, err)
            assert.spy(spy_on_keep_alive).was.called(1)

            assert.spy(tracer_mock.binary_extract).was_not_called()
            assert.spy(tracer_mock.http_headers_inject).was_not_called()
          end
        )

        it(
          "should not inject opentracing header if x-opentracing is not installed",
          function(...)
            kong.ctx.shared.x_opentracing = nil
            local res, err = _client:send("/v1", "POST", [[{"null":"body"}]], {}, 1, 1, 1)
            assert.are.same(user_id, res.userID)
            assert.are.same(nil, err)
            assert.spy(spy_on_keep_alive).was.called(1)

            assert.spy(tracer_mock.binary_extract).was_not_called()
            assert.spy(tracer_mock.http_headers_inject).was_not_called()
          end
        )

        it(
          "should not inject opentracing header if opentracing_bridge_tracer is not found",
          function(...)
            opentracing_bridge_tracer.not_found = true
            local res, err = _client:send("/v1", "POST", [[{"null":"body"}]], {}, 1, 1, 1)
            assert.are.same(user_id, res.userID)
            assert.are.same(nil, err)
            assert.spy(spy_on_keep_alive).was.called(1)

            assert.spy(tracer_mock.binary_extract).was_not_called()
            assert.spy(tracer_mock.http_headers_inject).was_not_called()
          end
        )
      end
    )
  end
)
