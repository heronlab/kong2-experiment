package.loaded["kong.plugins.commons.opentracing.opentracing_bridge_tracer"] = require("kong.spec.utils.opentracing-bridge-tracer-mock")
package.loaded["kong.plugins.commons.profiling"] = {
  trace_obj = function(label, obj, methods)
    return obj
  end
}

local mock_redis
local mock_ngx_time_value = 1576124451
local mock_token_payload = {
  iat = mock_ngx_time_value,
  exp = mock_ngx_time_value + 1000,
  pid = "pid",
  aud = "aud",
}
local mock_redis_no_cache = function ()
  mock_redis.get = function (...) return ngx.null, nil end
end

package.loaded["kong.plugins.utils.redis_util"] = {
  new_conn = function (...)
    return mock_redis
  end
}

local mock_sha256_hash_value = 'hashed_token'
package.loaded["kong.plugins.utils.sha256"] = {
  hash = function ()
    return mock_sha256_hash_value
  end
}

local cjson = require "cjson"
local constants = require "kong.constants"
local match = require("luassert.match")
local pl_table = require("pl.tablex")

package.loaded["kong.plugins.jwt.jwt_parser"] = {
  new = function()
    return { claims = mock_token_payload }
  end
}

local kong_log_mock = require("kong.spec.utils.kong-log-mock")
package.loaded["kong.plugins.commons.log"] = kong_log_mock

local spy_keep_alive

-- Ended Mock Http Client
local CONFIG_TIMEOUT, CONFIG_POOL, CONFIG_ALIVE_REQUESTS = 500, 440, 220
local mock_config = {
  host = "https://grabid.com",
  header_name = "Authorization",
  service_key = "OATUTH_MANAGER",
  service_token = "TOKEN_IS",
  ignore_services_list = { "ignore-service-id-1", "ignore-service-id-2" },
  ignore_routes_list = { "/ignore" },
  timeout = CONFIG_TIMEOUT,
  alive_pool = CONFIG_POOL,
  alive_requests = CONFIG_ALIVE_REQUESTS,
  port = 443,
  gateway_id = "50775e244d674d3c9e8381f63c93d0ce",
  pass_headers = {"X-GID-AUX", "User-Agent"},
  fail_away = false,
  redis_host = "redis",
  redis_port = 6379,
}

package.loaded["kong.request"] = {
  get_path = function(...)
    return "/authenticated"
  end,
  get_raw_query = function()
    return "x=debug"
  end
}

local httpc
package.loaded["resty.http"] = {
  new = function()
    return httpc
  end
}

-- MOCK Kong Cache
local kong_cache = {
  get = function(...)
    local arg = { ... }
    local loader = arg[4]
    local cache_calback = arg[6]
    return loader(arg[5], cache_calback)
  end
}

package.loaded["kong.singletons"] = {
  cache = kong_cache
}
-- END Mock Kong Cache

local datadog_reporter_mock = {}
package.loaded["kong.plugins.commons.analytics"] = {
  get_datadog_reporter = function()
    return datadog_reporter_mock
  end
}

local spy_on_set_header
local spy_on_response

local plugin_handler = require "kong.plugins.grabid.handler"

local get_response_200 = function()
  return {
    status = 200,
    body = '{"message": "OK"}',
    read_body = function(...)
      return [[
            {"userID":"40cf2a40-e64a-408a-adea-59f92a410905",
             "serviceID":"PASSENGER",
             "serviceUserID":1,
             "registeredOnService":true,
             "oauth2ContextID":"4a6c400b613c439ebca58104aa484039",
             "clientID": "clientA",
             "partnerID": "partnerA",
             "partnerUserID": "partnerUserIDA",
             "tokenID": "Iqh0Bf8JSP2TwzLUZ3F8kA"
            }
            ]]
    end
  }
end

describe(
  "UNITTEST - Handler",
  function()
    local old_ngx = _G.ngx
    local old_kong = _G.kong

    local stubbed_kong
    local stubbed_ngx
    local spy_on_get_header

    local grab_handler

    local consumer_mock = {
      select_by_custom_id = function()
        return {}
      end,
      cache_key = function()
        return "cache-key-kong"
      end
    }

    local internal_error_message = {
      message = "Internal Server Error"
    }

    local unauthorized_error_message = {
      message = "Forbidden"
    }

    local headers = {
      ["Authorization"] = "Bearer CLIENT_TOKEN",
      ["X-GID-AUX-POP"] = "pop",
      ["X-GID-AUX-X"] = "x-pop",
      ["User-Agent"] = "samsung-S8"
    }

    local expected_headers_to_grabid = {
      ["Cache-Control"] = "no-cache",
      ["Content-Type"] = "application/json",
      ["Authorization"] = "Token OATUTH_MANAGER TOKEN_IS",
      ["X-GID-AUX-POP"] = "pop",
      ["X-GID-AUX-X"] = "x-pop",
      ["USER-AGENT"] = "samsung-S8"
    }
    before_each(
      function()
        httpc = {
          keepalive = true,
          set_timeout = function(...)
          end,
          connect = function(...)
          end,
          ssl_handshake = function(...)
            return true, nil
          end,
          set_keepalive = function(...)
            spy_keep_alive(...)
            return 1, nil
          end,
          request = spy.new(
            function(...)
              return get_response_200(), nil
            end
          )
        }

        spy.on(consumer_mock, "select_by_custom_id")
        spy_keep_alive = spy.new(
          function(...)
          end
        )
        spy_on_set_header = spy.new(
          function(...)
          end
        )
        spy_on_get_header = spy.new(
          function(...)
          end
        )
        spy_on_response = spy.new(
          function(...)
          end
        )
        stubbed_ngx = {
          req = {
            get_method = function(...)
              return "GET"
            end,
            get_headers = function()
              return pl_table.copy(headers)
            end,
            set_header = function(...)
              spy_on_set_header(...)
            end
          },
          ctx = {
            route = {
              host = nil,
              id = "5adb6b69-304b-4e5b-9777-0b72f5b3f035",
              methods = { "GET", "POST", "PUT" },
              paths = { "/authenticated" },
              service = { id = "92cb26b2-3663-4e30-b918-6bafd783ab39" }
            },
            req_uri = "/authenticated"
          },
          timer = {
            at = function (t, f, ...)
              return f(false, ...)
            end
          },
          time = function()
            return mock_ngx_time_value
          end,
        }
        stubbed_kong = {
          request = {
            get_path = function(...)
              return "/authenticated"
            end,
            get_raw_query = function()
              return "x=debug"
            end,
            get_raw_body = function(...)
              return '{"raw": "body"}'
            end,
            get_header = function(key)
              local headers = {
                ["Authorization"] = "Bearer CLIENT_TOKEN",
                ["Another_Auth_Key"] = "Bearer CLIENT_TOKEN2"
              }
              spy_on_get_header(key)
              return headers[key]
            end
          },
          response = {
            exit = function(...)
              spy_on_response(...)
            end
          },
          ctx = {
            shared = {
              x_security = {
                security = "authorization_code",
                scheme_name = nil,
                x_provider = nil
              }
            }
          },
          db = {
            consumers = consumer_mock
          },
          service = {
            request = {
              set_headers = spy.new(
                function()
                end
              )
            }
          }
        }
        mock_redis = {
          get = spy.new(function (o, key)
            return '{"content":{"oauth2ContextID":"4a6c400b613c439ebca58104aa484039","serviceID":"PASSENGER","partnerID":"partnerA","userID":"40cf2a40-e64a-408a-adea-59f92a410905","clientID":"clientA","registeredOnService":true,"partnerUserID":"partnerUserIDA","tokenID":"Iqh0Bf8JSP2TwzLUZ3F8kA","serviceUserID":1}}', nil
          end),
          set = spy.new(function (o, key, value) end),
          del = spy.new(function (o, ...) end),
          set_keepalive = function () end,
          expire = spy.new(function (o, key, expiry) end),
        }
        _G.ngx = setmetatable(stubbed_ngx, {__index = old_ngx})
        _G.kong = setmetatable(stubbed_kong, {__index = old_kong})
        stub(stubbed_ngx, "exit")
        grab_handler = plugin_handler()
        stub(datadog_reporter_mock, "tag_request")
      end
    )
    describe(
      "Unit: Test Client",
      function(...)
        it(
          "In case kong.ctx.shared.x_security.security is authorization_code GrabID responses is 200, request is passed through",
          function()
            httpc.request = spy.new(
              function(...)
                return get_response_200(), nil
              end
            )

            kong.ctx.shared.x_security.security = "authorization_code"
            grab_handler:access(mock_config)
            assert.spy(httpc.request).was.called(1)
            local body = {
              ["oauth_token"] = "CLIENT_TOKEN",
              ["path"] = "/authenticated",
              ["method"] = "GET",
              ["gateway_id"] = mock_config.gateway_id
            }
            assert.spy(httpc.request).was_called_with(
              match.is_not_nil(),
              {
                path = "/v1/oauth2/verify",
                method = "POST",
                body = cjson.encode(body),
                headers = expected_headers_to_grabid,
                keepalive_timeout = CONFIG_TIMEOUT,
                keepalive_pool = CONFIG_POOL,
                keepalive_requests = CONFIG_ALIVE_REQUESTS
              }
            )
            assert.spy(spy_on_get_header).was.called_with("Authorization")
            assert.spy(spy_on_set_header).was.called_with("X-Grab-ID-UserID", "40cf2a40-e64a-408a-adea-59f92a410905")
            assert.spy(spy_on_set_header).was.called_with("X-Grab-ID-ServiceID", "PASSENGER")
            assert.spy(spy_on_set_header).was.called_with("X-Grab-ID-ServiceUserID", 1)
            assert.spy(spy_on_set_header).was.called_with("X-GRAB-ID-OAUTH2-CTX-ID", "4a6c400b613c439ebca58104aa484039")
            assert.spy(spy_on_set_header).was.called_with("X-GRAB-ID-OAUTH2-CTX-ID", "4a6c400b613c439ebca58104aa484039")
            assert.spy(spy_on_set_header).was.called_with("X-Grab-Id-Oauth2-Partner-Id", "partnerA")
            assert.spy(spy_keep_alive).was.called(1)
            -- should set cache
            local cache_key = "grabid:GET:/authenticated:hashed_token"
            local expected_payload = '{"content":{"oauth2ContextID":"4a6c400b613c439ebca58104aa484039","serviceID":"PASSENGER","partnerID":"partnerA","userID":"40cf2a40-e64a-408a-adea-59f92a410905","clientID":"clientA","registeredOnService":true,"partnerUserID":"partnerUserIDA","tokenID":"Iqh0Bf8JSP2TwzLUZ3F8kA","serviceUserID":1}}'
            assert.spy(mock_redis.set).was.called_with(mock_redis, cache_key, expected_payload)

            assert.are.same(kong.ctx.shared.session_user["$ctx.session.UserID"], "40cf2a40-e64a-408a-adea-59f92a410905")
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.ServiceID"], "PASSENGER")
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.ServiceUserID"], 1)
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.RegisteredOnService"], true)
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.Oauth2ContextID"], "4a6c400b613c439ebca58104aa484039")
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.ClientID"], "clientA")
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.PartnerID"], "partnerA")
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.PartnerUserID"], "partnerUserIDA")
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.TokenID"], "Iqh0Bf8JSP2TwzLUZ3F8kA")
          end
        )
        it(
          "In case kong.ctx.shared.x_security.security is authorization_code GrabID responses is 200, request is passed through",
          function()
            httpc.request = spy.new(
              function(...)
                return get_response_200(), nil
              end
            )

            kong.ctx.shared.x_security.security = "authorization_code"
            kong.ctx.shared.x_security.scheme_name = "Another_Auth_Key"
            grab_handler:access(mock_config)
            assert.spy(httpc.request).was.called(1)
            local body = {
              ["oauth_token"] = "CLIENT_TOKEN2",
              ["path"] = "/authenticated",
              ["method"] = "GET",
              ["gateway_id"] = mock_config.gateway_id
            }
            assert.spy(httpc.request).was_called_with(
              match.is_not_nil(),
              {
                path = "/v1/oauth2/verify",
                method = "POST",
                body = cjson.encode(body),
                headers = expected_headers_to_grabid,
                keepalive_timeout = CONFIG_TIMEOUT,
                keepalive_pool = CONFIG_POOL,
                keepalive_requests = CONFIG_ALIVE_REQUESTS
              }
            )
            assert.spy(spy_on_get_header).was.called_with("Another_Auth_Key")
            assert.spy(spy_on_set_header).was.called_with("X-Grab-ID-UserID", "40cf2a40-e64a-408a-adea-59f92a410905")
            assert.spy(spy_on_set_header).was.called_with("X-Grab-ID-ServiceID", "PASSENGER")
            assert.spy(spy_on_set_header).was.called_with("X-Grab-ID-ServiceUserID", 1)
            assert.spy(spy_on_set_header).was.called_with("X-GRAB-ID-OAUTH2-CTX-ID", "4a6c400b613c439ebca58104aa484039")
            assert.spy(spy_on_set_header).was.called_with("X-GRAB-ID-OAUTH2-CTX-ID", "4a6c400b613c439ebca58104aa484039")
            assert.spy(spy_on_set_header).was.called_with("X-Grab-Id-Oauth2-Partner-Id", "partnerA")
            assert.spy(spy_keep_alive).was.called(1)
            -- should set cache
            local cache_key = "grabid:GET:/authenticated:hashed_token"
            local expected_payload = '{"content":{"oauth2ContextID":"4a6c400b613c439ebca58104aa484039","serviceID":"PASSENGER","partnerID":"partnerA","userID":"40cf2a40-e64a-408a-adea-59f92a410905","clientID":"clientA","registeredOnService":true,"partnerUserID":"partnerUserIDA","tokenID":"Iqh0Bf8JSP2TwzLUZ3F8kA","serviceUserID":1}}'
            assert.spy(mock_redis.set).was.called_with(mock_redis, cache_key, expected_payload)

            assert.are.same(kong.ctx.shared.session_user["$ctx.session.UserID"], "40cf2a40-e64a-408a-adea-59f92a410905")
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.ServiceID"], "PASSENGER")
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.ServiceUserID"], 1)
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.RegisteredOnService"], true)
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.Oauth2ContextID"], "4a6c400b613c439ebca58104aa484039")
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.ClientID"], "clientA")
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.PartnerID"], "partnerA")
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.PartnerUserID"], "partnerUserIDA")
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.TokenID"], "Iqh0Bf8JSP2TwzLUZ3F8kA")
          end
        )

        it(
          "In case GrabID responses is 200, should set consumer to context and header and partner to context",
          function()
            httpc.request = spy.new(
              function(...)
                return get_response_200(), nil
              end
            )

            local consumer = { id = "partnerA", custom_id = "partnerA", username = "generate_partnerA", create_at = 0 }

            grab_handler:access(mock_config)

            assert.are.same(consumer, ngx.ctx.authenticated_consumer)
            assert.are.same(consumer, kong.ctx.shared.authenticated_consumer)
            local const = constants.HEADERS
            assert.spy(kong.service.request.set_headers).was.called_with(
              {
                [const.CONSUMER_ID] = consumer.id,
                [const.CONSUMER_CUSTOM_ID] = tostring(consumer.custom_id),
                [const.CONSUMER_USERNAME] = consumer.username
              }
            )

            assert.spy(spy_keep_alive).was.called(1)
            assert.are.same({ id = "partnerA", client_id = "clientA" }, kong.ctx.shared.partner)
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.UserID"], "40cf2a40-e64a-408a-adea-59f92a410905")
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.ServiceID"], "PASSENGER")
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.ServiceUserID"], 1)
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.RegisteredOnService"], true)
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.Oauth2ContextID"], "4a6c400b613c439ebca58104aa484039")
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.ClientID"], "clientA")
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.PartnerID"], "partnerA")
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.PartnerUserID"], "partnerUserIDA")
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.TokenID"], "Iqh0Bf8JSP2TwzLUZ3F8kA")
          end
        )

        it(
          "In case request timeout, should return 500",
          function()
            httpc.request = spy.new(
              function()
                return nil, "timeout"
              end
            )
            mock_redis_no_cache()

            grab_handler:access(mock_config)

            assert.are.equals(nil, ngx.ctx.authenticated_consumer)
            assert.are.equals(nil, kong.ctx.shared.authenticated_consumer)
            assert.spy(kong.service.request.set_headers).was_not_called()
            assert.spy(spy_on_response).was_called_with(500, internal_error_message, nil)

            -- no cache call
            assert.spy(mock_redis.set).was_not_called()
          end
        )

        it(
          "In case can't do ssl handshake, should return 500",
          function()
            httpc.ssl_handshake = spy.new(
              function(...)
                return false, nil
              end
            )
            mock_redis_no_cache()

            grab_handler:access(mock_config)

            assert.are.equals(nil, ngx.ctx.authenticated_consumer)
            assert.are.equals(nil, kong.ctx.shared.authenticated_consumer)
            assert.spy(kong.service.request.set_headers).was_not_called()
            assert.spy(spy_on_response).was_called_with(500, internal_error_message, nil)
            -- no cache call
            assert.spy(mock_redis.set).was_not_called()
          end
        )

        it(
          "In case there is no partnerID returned from GrabID, should return 401",
          function()
            local response = {
              status = 200,
              body = '{"message": "OK"}',
              read_body = function(...)
                return [[
                    {"userID":"40cf2a40-e64a-408a-adea-59f92a410905",
                     "serviceID":"PASSENGER",
                     "serviceUserID":1,
                     "clientID": "clientA",
                     "registeredOnService":true,
                     "oauth2ContextID":"4a6c400b613c439ebca58104aa484039",
                     "partnerUserID": "partnerUserIDA",
                     "tokenID": "Iqh0Bf8JSP2TwzLUZ3F8kA"
                    }
                    ]]
              end
            }
            httpc.request = spy.new(
              function(...)
                return response, nil
              end
            )

            grab_handler:access(mock_config)

            assert.are.equals(nil, ngx.ctx.authenticated_consumer)
            assert.are.equals(nil, kong.ctx.shared.authenticated_consumer)
            assert.spy(kong.service.request.set_headers).was_not_called()
            assert.spy(spy_on_response).was_called_with(
              401,
              { message = "Unauthorized" },
              { ["WWW-Authenticate"] = [[Bearer error="invalid_token", error_description="invalid token"]] }
            )
            assert.spy(spy_keep_alive).was.called(1)
            assert.spy(mock_redis.set).was.called(1)
          end
        )

        it(
          "In case there is no clientID returned from GrabID, should return 401",
          function()
            local response = {
              status = 200,
              body = '{"message": "OK"}',
              read_body = function(...)
                return [[
                    {"userID":"40cf2a40-e64a-408a-adea-59f92a410905",
                     "serviceID":"PASSENGER",
                     "serviceUserID":1,
                     "partnerID": "partnerA",
                     "registeredOnService":true,
                     "oauth2ContextID":"4a6c400b613c439ebca58104aa484039",
                     "partnerUserID": "partnerUserIDA",
                     "tokenID": "Iqh0Bf8JSP2TwzLUZ3F8kA"
                    }
                    ]]
              end
            }
            httpc.request = spy.new(
              function(...)
                return response, nil
              end
            )

            consumer_mock.select_by_custom_id = spy.new(
              function()
                return nil, nil
              end
            )

            grab_handler:access(mock_config)

            assert.spy(spy_on_response).was_called_with(
              401,
              { message = "Unauthorized" },
              { ["WWW-Authenticate"] = [[Bearer error="invalid_token", error_description="invalid token"]] }
            )
            assert.spy(spy_keep_alive).was.called(1)
            assert.spy(mock_redis.set).was.called(1)
          end
        )

        local cases = {
          ["GrabID Response 401"] = {
            status = 401,
            body = '{"message": "NO"}',
            read_body = function()
              return [[{"status": 401}]]
            end
          },
          ["GrabID Response 422"] = {
            status = 422,
            body = '{"message": "NO"}',
            read_body = function()
              return [[{"status": 422}]]
            end
          }
        }
        for case, _res in pairs(cases) do
          it(
            case .. ", we should send 401 status code",
            function()
              httpc.request = spy.new(
                function()
                  return _res, nil
                end
              )

              grab_handler:access(mock_config)
              assert.spy(spy_keep_alive).was.called(1)
              assert.spy(mock_redis.set).was_not_called()
              assert.spy(spy_on_response).was_called_with(
                401,
                { message = "Unauthorized" },
                { ["WWW-Authenticate"] = [[Bearer error="invalid_token", error_description="invalid token"]] }
              )
            end
          )
        end

        local cases = {
          ["GrabID Response 403"] = {
            status = 403,
            body = '{"message": "NO"}',
            read_body = function()
              return [[{"status": 401}]]
            end
          }
        }
        for case, _res in pairs(cases) do
          it(
            case .. ", we should send 403 status code",
            function()
              httpc.request = function(...)
                return _res, nil
              end
              mock_redis_no_cache()

              grab_handler:access(mock_config)
              assert.spy(spy_keep_alive).was.called(1)
              assert.spy(mock_redis.set).was_not_called()
              assert.spy(spy_on_response).was_called_with(403, unauthorized_error_message, nil)
            end
          )
        end

        local cases = {
          ["GrabID Response 500"] = {
            status = 500,
            body = '{"message": "NO"}',
            read_body = function()
              return [[{"status": 500}]]
            end
          },
          ["GrabID Response 504"] = {
            status = 504,
            body = '{"message": "NO"}',
            read_body = function()
              return [[{"status": 500}]]
            end
          },
          ["GrabID Response 404"] = {
            status = 404,
            body = '{"message": "NO"}',
            read_body = function()
              return [[{"status": 500}]]
            end
          },
          ["GrabID Response 502"] = {
            status = 503,
            body = '{"message": "NO"}',
            read_body = function()
              return [[{"status": 500}]]
            end
          }
        }
        for case, _res in pairs(cases) do
          it(
            case .. ", we should send 500 status code",
            function()
              httpc.request = function(...)
                return _res, nil
              end
              mock_redis_no_cache()

              grab_handler:access(mock_config)
              assert.spy(spy_keep_alive).was.called(1)
              assert.spy(mock_redis.set).was_not_called()
              assert.spy(spy_on_response).was_called_with(500, internal_error_message, nil)
            end
          )
        end

        it(
          "should pass correlation id to grabid if configured",
          function()
            kong.ctx.shared.x_security.security = "authorization_code"
            kong.ctx.shared.x_security.scheme_name = "Another_Auth_Key"

            local config = pl_table.union(mock_config, { correlation_id_header = "X-Request-ID2" })
            local correlation_id = "id1"
            ngx.req.get_headers = function(...)
              return pl_table.union(headers, { [config.correlation_id_header] = correlation_id })
            end

            grab_handler:access(config)

            local body = {
              ["oauth_token"] = "CLIENT_TOKEN2",
              ["path"] = "/authenticated",
              ["method"] = "GET",
              ["gateway_id"] = mock_config.gateway_id
            }
            assert.spy(httpc.request).was_called_with(
              match.is_not_nil(),
              {
                path = "/v1/oauth2/verify",
                method = "POST",
                body = cjson.encode(body),
                headers = pl_table.union(
                  expected_headers_to_grabid,
                  {
                    [config.correlation_id_header] = correlation_id
                  }
                ),
                keepalive_timeout = CONFIG_TIMEOUT,
                keepalive_pool = CONFIG_POOL,
                keepalive_requests = CONFIG_ALIVE_REQUESTS
              }
            )
          end
        )

        it(
          "should not pass correlation id to grabid if empty correlation id header",
          function()
            kong.ctx.shared.x_security.security = "authorization_code"
            kong.ctx.shared.x_security.scheme_name = "Another_Auth_Key"

            local config = pl_table.union(mock_config, { correlation_id_header = "" })
            local correlation_id = "id1"
            ngx.req.get_headers = function(...)
              return pl_table.union(headers, { [config.correlation_id_header] = correlation_id })
            end

            grab_handler:access(config)

            local body = {
              ["oauth_token"] = "CLIENT_TOKEN2",
              ["path"] = "/authenticated",
              ["method"] = "GET",
              ["gateway_id"] = mock_config.gateway_id
            }
            assert.spy(httpc.request).was_called_with(
              match.is_not_nil(),
              {
                path = "/v1/oauth2/verify",
                method = "POST",
                body = cjson.encode(body),
                headers = expected_headers_to_grabid,
                keepalive_timeout = CONFIG_TIMEOUT,
                keepalive_pool = CONFIG_POOL,
                keepalive_requests = CONFIG_ALIVE_REQUESTS
              }
            )
          end
        )

        it(
          "should not pass correlation id to grabid if nil correlation id",
          function()
            kong.ctx.shared.x_security.security = "authorization_code"
            kong.ctx.shared.x_security.scheme_name = "Another_Auth_Key"

            local config = pl_table.union(mock_config, { correlation_id_header = "X-Request-ID" })
            grab_handler:access(config)

            local body = {
              ["oauth_token"] = "CLIENT_TOKEN2",
              ["path"] = "/authenticated",
              ["method"] = "GET",
              ["gateway_id"] = mock_config.gateway_id
            }
            assert.spy(httpc.request).was_called_with(
              match.is_not_nil(),
              {
                path = "/v1/oauth2/verify",
                method = "POST",
                body = cjson.encode(body),
                headers = expected_headers_to_grabid,
                keepalive_timeout = CONFIG_TIMEOUT,
                keepalive_pool = CONFIG_POOL,
                keepalive_requests = CONFIG_ALIVE_REQUESTS
              }
            )
          end
        )

        it(
          "should not pass correlation id to grabid if empty correlation id",
          function()
            kong.ctx.shared.x_security.security = "authorization_code"
            kong.ctx.shared.x_security.scheme_name = "Another_Auth_Key"
            local config = pl_table.union(mock_config, { correlation_id_header = "X-Request-ID" })
            ngx.req.get_headers = function(...)
              return pl_table.union(headers, { [config.correlation_id_header] = "" })
            end

            grab_handler:access(config)

            local body = {
              ["oauth_token"] = "CLIENT_TOKEN2",
              ["path"] = "/authenticated",
              ["method"] = "GET",
              ["gateway_id"] = mock_config.gateway_id
            }
            assert.spy(httpc.request).was_called_with(
              match.is_not_nil(),
              {
                path = "/v1/oauth2/verify",
                method = "POST",
                body = cjson.encode(body),
                headers = expected_headers_to_grabid,
                keepalive_timeout = CONFIG_TIMEOUT,
                keepalive_pool = CONFIG_POOL,
                keepalive_requests = CONFIG_ALIVE_REQUESTS
              }
            )
          end
        )

        it(
          "should add header values with prefix 'X-GID-AUX-' from GrabID when security = 'authorization_code'",
          function()
            kong.ctx.shared.x_security.security = "client_credentials"

            httpc.request = spy.new(
              function(...)
                local response_200 = get_response_200()
                response_200["headers"] = {
                  ["X-GID-AUX-TEST"] = "valid",
                  ["X-GID-INVALID-TEST"] = "invalid"
                }

                return response_200, nil
              end
            )

            grab_handler:access(mock_config)
            assert.spy(spy_on_set_header).was.called_with("X-GID-AUX-TEST", "valid")
            assert.spy(spy_on_set_header).was_not.called_with("X-GID-INVALID-TEST", "invalid")
          end
        )

        it(
          "expect send auth_type:hmac+client_credentials to datadog",
          function()
            kong.ctx.shared.x_security.security = "authorization_code"

            httpc.request = spy.new(
              function(...)
                local response_200 = get_response_200()
                return response_200, nil
              end
            )

            grab_handler:access(mock_config)

            assert.spy(datadog_reporter_mock.tag_request).was.called_with({ ["auth_type"] = "authorization_code" })
          end
        )

        it(
          "should call GrabID when fail_away=true and no cache available",
          function()
            kong.ctx.shared.x_security.security = "authorization_code"
            mock_config.fail_away = true
            mock_redis.get = spy.new(function (o, key)
              return nil, "faked error"
            end)
            package.loaded["kong.plugins.utils.redis_util"] = {
              new_conn = function (...)
                return mock_redis
              end
            }
            httpc.request =
              spy.new(
              function(...)
                local response_200 = get_response_200()
                return response_200, nil
              end
            )

            grab_handler:access(mock_config)
            assert.spy(mock_redis.get).was.called_with(mock_redis, "grabid:GET:/authenticated:hashed_token")
            assert.spy(httpc.request).was.called(1)
            local body = {
              ["oauth_token"] = "CLIENT_TOKEN",
              ["path"] = "/authenticated",
              ["method"] = "GET",
              ["gateway_id"] = mock_config.gateway_id
            }
            assert.spy(httpc.request).was_called_with(
              match.is_not_nil(),
              {
                path = "/v1/oauth2/verify",
                method = "POST",
                body = cjson.encode(body),
                headers = expected_headers_to_grabid,
                keepalive_timeout = CONFIG_TIMEOUT,
                keepalive_pool = CONFIG_POOL,
                keepalive_requests = CONFIG_ALIVE_REQUESTS
              }
            )
          end
        )

        it(
          "should get cache first when fail_away is true",
          function()
            mock_config.fail_away = true
            httpc.request = spy.new(function(...) end)
            grab_handler:access(mock_config)
            assert.spy(mock_redis.get).was.called_with(mock_redis, "grabid:GET:/authenticated:hashed_token")
            assert.spy(httpc.request).was_not_called()
            assert.spy(mock_redis.set).was_not_called()
          end
        )

        it(
          "should call grabid normally when fail_away is true but no cached",
          function()
            mock_config.fail_away = true
            mock_redis.get = spy.new(function (o, key)
              return ngx.null, nil
            end)
            httpc.request = spy.new(function(...) return nil, 401, nil end)
            grab_handler:access(mock_config)
            assert.spy(mock_redis.get).was.called_with(mock_redis, "grabid:GET:/authenticated:hashed_token")
            assert.spy(httpc.request).was.called(1)
            assert.spy(mock_redis.set).was_not_called()
          end
        )

        it(
          "should call grabid normally when fail_away is true but cache is expired",
          function()
            mock_config.fail_away = true
            mock_config.token_cache_ttl = 0
            mock_token_payload = { exp = ngx.time() - 10 }
            httpc.request = spy.new(function(...) return nil, 401, nil end)
            grab_handler:access(mock_config)
            assert.spy(mock_redis.get).was_not_called()
            assert.spy(httpc.request).was.called(1)
          end
        )

        -- tests for cache expiry
        local expiry_cases = {
          ["uses config token_cache_ttl"] = {
            token_cache_ttl = 60,
            token_expiry = mock_ngx_time_value + 1000,
            expected_expiry = 60,
          },
          ["uses token expiry"] = {
            token_cache_ttl = 0,
            token_expiry = mock_ngx_time_value + 1000,
            expected_expiry = 1000,
          }
        }
        for case_name, data in pairs(expiry_cases) do
          it(
            "should set right cache expiry (" .. case_name .. ")",
            function()
               local current_ts = mock_ngx_time_value
               mock_config.token_cache_ttl = data.token_cache_ttl
               mock_token_payload = { iat = current_ts, exp = data.token_expiry }
               httpc.request = spy.new(function(...) return get_response_200(), nil end)
               mock_redis_no_cache()
               grab_handler:access(mock_config)
               assert.spy(mock_redis.expire).was.called_with(mock_redis, match.is_string(), data.expected_expiry)
            end
          )
        end

        local not_fetch_expired_cache_cases = {
          ["token expired and not configured token_cache_ttl"] = {
            token_expiry = mock_ngx_time_value,
            token_iat = mock_ngx_time_value - 1000,
            token_cache_ttl = 0,
          },
          ["configured token_cache_ttl"] = {
            token_expiry = mock_ngx_time_value + 1000,
            token_iat = mock_ngx_time_value - 60,
            token_cache_ttl = 60, -- must be <= 60 to be expired
          }
        }
        for case_name, data in pairs(not_fetch_expired_cache_cases) do
          it(
            "should not fetch expired cache (" .. case_name .. ")",
            function()
              mock_token_payload = { exp = data.token_expiry, iat = data.token_iat }
              mock_config.token_cache_ttl = data.token_cache_ttl
              grab_handler:access(mock_config)
              assert.spy(mock_redis.get).was_not_called()
            end
          )
        end

        it(
          "should not use redis if redis_host hasn't configured yet",
          function()
            mock_config.redis_host = ''
            httpc.request = spy.new(function(...) return get_response_200(), nil end)
            grab_handler:access(mock_config)
            assert.spy(mock_redis.get).was_not_called()
            assert.spy(mock_redis.set).was_not_called()
          end
        )
      end
    )
  end
)
