package.loaded["kong.plugins.commons.profiling"] = require("kong.spec.utils.profiling-mock")
package.loaded["kong.plugins.commons.opentracing.opentracing_bridge_tracer"] =
  require("kong.spec.utils.opentracing-bridge-tracer-mock")

local cjson = require "cjson"
local kong_log_mock = require("kong.spec.utils.kong-log-mock")
package.loaded["kong.plugins.commons.log"] = kong_log_mock

-- Mock Http Client
local http = {
  _VERSION = "0.12"
}

local spy_keep_alive
local mt = {__index = http}
-- Ended Mock Http Client

local mock_redis

package.loaded["kong.plugins.utils.redis_util"] = {
  new_conn = function (...)
    return mock_redis
  end
}

local mock_token_payload = {iat = ngx.time() - 10, exp = ngx.time() + 1000 }
package.loaded["kong.plugins.jwt.jwt_parser"] = {
  new = function()
    return { claims = mock_token_payload }
  end
}

local CONFIG_TIMEOUT, CONFIG_POOL, CONFIG_ALIVE_REQUESTS = 500, 440, 220
local mock_config = {
  host = "https://grabid.com",
  header_name = "Authorization",
  service_key = "OATUTH_MANAGER",
  service_token = "TOKEN_IS",
  timeout = CONFIG_TIMEOUT,
  alive_pool = CONFIG_POOL,
  alive_requests = CONFIG_ALIVE_REQUESTS,
  port = 443,
  gateway_id = "50775e244d674d3c9e8381f63c93d0ce",
  pass_headers = {"X-GID-AUX", "User-Agent"}
}
package.loaded["kong.request"] = {
  get_path = function(...)
    return "/authenticated"
  end,
  get_raw_query = function()
    return "x=debug"
  end
}
-- MOCK Kong Cache
local kong_cache = {
  get = function(...)
    local arg = {...}
    local loader = arg[4]
    local cache_callback = arg[6]
    return loader(arg[5], cache_callback)
  end
}

package.loaded["kong.singletons"] = {
  cache = kong_cache
}
-- END Mock Kong Cache

local datadog_reporter_mock = {}
package.loaded["kong.plugins.commons.analytics"] = {
  get_datadog_reporter = function()
    return datadog_reporter_mock
  end
}

local httpc
local spy_on_set_header
local spy_on_response

package.loaded["resty.http"] = {
  new = function()
    local sock = {}
    httpc =
      setmetatable(
      {
        sock = sock,
        keepalive = true,
        set_timeout = function(...)
        end,
        connect = function(...)
        end,
        ssl_handshake = function(...)
          return true, nil
        end,
        set_keepalive = function(...)
          spy_keep_alive(...)
          return 1, nil
        end
      },
      mt
    )
    return httpc
  end
}
package.loaded["ssl.https"] = {
  request = function(...)
    return '{"signature":"xxx"}', 200
  end
}

local plugin_handler = require "kong.plugins.grabid.handler"
local nginx_status = "200"

local get_response_200 = function()
  return {
    status = 200,
    body = '{"message": "OK"}',
    read_body = function(...)
      return [[
            {"userID":"40cf2a40-e64a-408a-adea-59f92a410905",
             "serviceID":"PASSENGER",
             "serviceUserID":1,
             "registeredOnService":true,
             "oauth2ContextID":"4a6c400b613c439ebca58104aa484039",
             "clientID": "clientA",
             "partnerID": "partnerA",
             "partnerUserID": "partnerUserIDA",
             "tokenID": "Iqh0Bf8JSP2TwzLUZ3F8kA"
            }
            ]]
    end
  }
end

describe(
  "UNITTEST - Handler",
  function()
    local old_ngx = _G.ngx
    local old_kong = _G.kong
    local spy_on

    local snapshot

    local stubbed_kong
    local stubbed_ngx

    local grab_handler

    local consumer_mock = {
      select_by_custom_id = function()
        return {}
      end,
      cache_key = function()
        return "cache-key-kong"
      end
    }

    local unauthorized_error_message = {
      message = "Unauthorized"
    }

    before_each(
      function()
        snapshot = assert:snapshot()
        spy_keep_alive =
          spy.new(
          function(...)
          end
        )
        spy.on(consumer_mock, "select_by_custom_id")
        spy_on =
          spy.new(
          function(...)
          end
        )
        spy_on_set_header =
          spy.new(
          function(...)
          end
        )
        spy_on_response =
          spy.new(
          function(...)
          end
        )
        stubbed_ngx = {
          log = function(...)
            return old_ngx.log(...)
          end,
          print = function(...)
            return old_ngx.print(...)
          end,
          exit = function(...)
            return old_ngx.exist(...)
          end,
          req = {
            get_method = function(...)
              return "GET"
            end,
            get_headers = function()
              return {
                ["Authorization"] = "Bearer CLIENT_TOKEN",
                ["content-type"] = "application/json",
                ["Date"] = "Sun, 06 Nov 2016 08:49:37 GMT"
              }
            end,
            set_header = function(...)
              spy_on_set_header(...)
            end,
          },
          ctx = {
            route = {
              host = nil,
              id = "5adb6b69-304b-4e5b-9777-0b72f5b3f035",
              methods = {"GET", "POST", "PUT"},
              paths = {"/authenticated"},
              service = {id = "92cb26b2-3663-4e30-b918-6bafd783ab39"}
            },
            req_uri = "/authenticated",
            service = {
              name = "grabpay"
            }
          },
          status = nginx_status,
          timer = {
            at = function (t, f, ...)
              return f(false, ...)
            end
          }
        }
        stubbed_kong = {
          db = {
            consumers = consumer_mock
          },
          request = {
            get_path = function(...)
              return "/authenticated"
            end,
            get_raw_query = function()
              return "x=debug"
            end,
            get_raw_body = function()
              return '{"name": "request"}'
            end,
            get_header = function(key)
              local headers = {["Authorization"] = "Bearer CLIENT_TOKEN"}
              return headers[key]
            end
          },
          response = {
            exit = function(...)
              spy_on_response(...)
            end
          },
          ctx = {
            shared = {
              x_security = {
                security = nil,
                scheme_name = nil,
                x_provider = nil
              }
            },
            plugin = {}
          },
          service = {
            request = {
              set_headers = spy.new(
                function()
                end
              )
            }
          },

        }
        mock_redis = {
          get = spy.new(function (o, key) end),
          set = spy.new(function (o, key, value) end),
          del = spy.new(function (o, ...) end),
          set_keepalive = function () end,
          expire = spy.new(function (o, ...) end),
        }
        _G.ngx = setmetatable(stubbed_ngx, {__index = old_ngx})
        _G.kong = setmetatable(stubbed_kong, {__index = old_kong})

        grab_handler = plugin_handler()
        stub(datadog_reporter_mock, "tag_request")
      end
    )

    after_each(
      function()
        snapshot:revert()
        _G.ngx = old_ngx
        _G.kong = old_kong
      end
    )

    describe(
      "Unit: Test Client",
      function()
        it(
          "If kong.ctx.shared.x_security=client_credentials, then we should http request GRABID",
          function()
            function http:request(...)
              spy_on(...)
              return get_response_200(), nil
            end

            kong.ctx.shared.x_security.security = "client_credentials"
            grab_handler:access(mock_config)

            assert.spy(spy_on).was.called(1)
            local body = {
              ["oauth_token"] = "CLIENT_TOKEN",
              ["path"] = "/authenticated",
              ["method"] = "GET",
              ["gateway_id"] = mock_config.gateway_id
            }
            assert.spy(spy_on).was_called_with {
              path = "/v1/oauth2/verify",
              method = "POST",
              body = cjson.encode(body),
              headers = {
                ["Content-Type"] = "application/json",
                ["Cache-Control"] = "no-cache",
                ["Authorization"] = "Token OATUTH_MANAGER TOKEN_IS"
              },
              keepalive_timeout = CONFIG_TIMEOUT,
              keepalive_pool = CONFIG_POOL,
              keepalive_requests = CONFIG_ALIVE_REQUESTS
            }
            assert.spy(spy_keep_alive).was.called(1)
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.UserID"], "40cf2a40-e64a-408a-adea-59f92a410905")
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.ServiceID"], "PASSENGER")
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.ServiceUserID"], 1)
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.RegisteredOnService"], true)
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.Oauth2ContextID"], "4a6c400b613c439ebca58104aa484039")
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.ClientID"], "clientA")
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.PartnerID"], "partnerA")
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.PartnerUserID"], "partnerUserIDA")
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.TokenID"], "Iqh0Bf8JSP2TwzLUZ3F8kA")
          end
        )

        it(
          "Success 'client_credentials' verify with security = 'hmac+client_credentials'",
          function()
            kong.ctx.shared.x_security.security = "hmac+client_credentials"

            function http:request(...)
              spy_on(...)
              return get_response_200(), nil
            end

            grab_handler:access(mock_config)

            assert.spy(spy_on).was.called(1)
            local body = {
              ["oauth_token"] = "CLIENT_TOKEN",
              ["path"] = "/authenticated",
              ["method"] = "GET",
              ["gateway_id"] = mock_config.gateway_id
            }
            assert.spy(spy_on).was_called_with {
              path = "/v1/oauth2/verify",
              method = "POST",
              body = cjson.encode(body),
              headers = {
                ["Content-Type"] = "application/json",
                ["Cache-Control"] = "no-cache",
                ["Authorization"] = "Token OATUTH_MANAGER TOKEN_IS"
              },
              keepalive_timeout = CONFIG_TIMEOUT,
              keepalive_pool = CONFIG_POOL,
              keepalive_requests = CONFIG_ALIVE_REQUESTS
            }
            assert.spy(spy_keep_alive).was.called(1)
            -- Not call with HMAC
            assert.are.same(nil, kong.ctx.plugin.hmac_verified)
            assert.spy(spy_on_response).was.was_not_called_with(401, unauthorized_error_message, nil)
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.UserID"], "40cf2a40-e64a-408a-adea-59f92a410905")
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.ServiceID"], "PASSENGER")
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.ServiceUserID"], 1)
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.RegisteredOnService"], true)
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.Oauth2ContextID"], "4a6c400b613c439ebca58104aa484039")
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.ClientID"], "clientA")
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.PartnerID"], "partnerA")
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.PartnerUserID"], "partnerUserIDA")
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.TokenID"], "Iqh0Bf8JSP2TwzLUZ3F8kA")
          end
        )

        it(
          "should add header values with prefix 'X-GID-AUX-' from GrabID when security = 'client_credentials'",
          function()
            kong.ctx.shared.x_security.security = "client_credentials"

            function http:request(...)
              spy_on(...)
              local response_200 = get_response_200()
              response_200["headers"] =  {
                ["X-GID-AUX-TEST"] = "valid",
                ["X-GID-INVALID-TEST"] = "invalid"
              }

              return response_200, nil
            end

            grab_handler:access(mock_config)
            assert.spy(spy_on_set_header).was.called_with("X-GID-AUX-TEST", "valid")
            assert.spy(spy_on_set_header).was_not.called_with("X-GID-INVALID-TEST", "invalid")
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.UserID"], "40cf2a40-e64a-408a-adea-59f92a410905")
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.ServiceID"], "PASSENGER")
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.ServiceUserID"], 1)
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.RegisteredOnService"], true)
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.Oauth2ContextID"], "4a6c400b613c439ebca58104aa484039")
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.ClientID"], "clientA")
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.PartnerID"], "partnerA")
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.PartnerUserID"], "partnerUserIDA")
            assert.are.same(kong.ctx.shared.session_user["$ctx.session.TokenID"], "Iqh0Bf8JSP2TwzLUZ3F8kA")
          end
        )

        it(
          "expect send auth_type:client_credentials to datadog",
          function()
            kong.ctx.shared.x_security.security = "client_credentials"

            function http:request(...)
              spy_on(...)
              local response_200 = get_response_200()

              return response_200, nil
            end

            grab_handler:access(mock_config)

            assert.spy(datadog_reporter_mock.tag_request).was.called_with({ ["auth_type"] = "client_credentials" })
          end
        )

      end
    )
  end
)
