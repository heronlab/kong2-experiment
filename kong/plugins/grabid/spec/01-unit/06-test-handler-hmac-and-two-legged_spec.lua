--
-- Created by IntelliJ IDEA.
-- User: hoaian.nguyen
-- Date: 8/18/18
-- Time: 11:09 AM
-- To change this template use File | Settings | File Templates.
--
package.loaded.resty = nil
package.loaded.kong = nil
package.loaded.ssl = nil
package.loaded.cjson = nil

local kong_log_mock = require("kong.spec.utils.kong-log-mock")
package.loaded["kong.plugins.commons.log"] = kong_log_mock
package.loaded["kong.plugins.commons.opentracing.opentracing_bridge_tracer"] = require("kong.spec.utils.opentracing-bridge-tracer-mock")
package.loaded["kong.plugins.commons.profiling"] = require("kong.spec.utils.profiling-mock")

local spy_keep_alive
local httpc

-- Ended Mock Http Client

local mock_redis

package.loaded["kong.plugins.utils.redis_util"] = {
  new_conn = function (...)
    return mock_redis
  end
}

local mock_token_payload = {iat = ngx.time() - 10, exp = ngx.time() + 1000 }
package.loaded["kong.plugins.jwt.jwt_parser"] = {
  new = function()
    return { claims = mock_token_payload }
  end
}

local CONFIG_TIMEOUT, CONFIG_POOL, CONFIG_ALIVE_REQUESTS = 500, 440, 220
local mock_config = {
  host = "https://grabid.com",
  header_name = "Authorization",
  service_key = "OATUTH_MANAGER",
  service_token = "TOKEN_IS",
  ignore_services_list = { "ignore-service-id-1", "ignore-service-id-2" },
  ignore_routes_list = { "/ignore" },
  timeout = CONFIG_TIMEOUT,
  alive_pool = CONFIG_POOL,
  alive_requests = CONFIG_ALIVE_REQUESTS,
  port = 443,
  gateway_id = "50775e244d674d3c9e8381f63c93d0ce",
  pass_headers = { "X-GID-AUX", "User-Agent" }
}

package.loaded["kong.request"] = {
  get_path = function(...)
    return "/authenticated"
  end,
  get_raw_query = function()
    return "x=debug"
  end
}

-- MOCK Kong Cache
local kong_cache = {
  get = function(...)
    local arg = { ... }
    local loader = arg[4]
    local cache_calback = arg[6]
    return loader(arg[5], cache_calback)
  end
}

package.loaded["kong.singletons"] = {
  cache = kong_cache
}
-- END Mock Kong Cache
package.loaded["resty.http"] = {
  new = function()
    return httpc
  end
}
package.loaded["ssl.https"] = {
  request = function(...)
    return '{"signature":"xxx"}', 200
  end
}

package.loaded["cjson"] = {
  encode = function(...)
  end,
  decode = function(...)
  end
}

local datadog_reporter_mock = {}
package.loaded["kong.plugins.commons.analytics"] = {
  get_datadog_reporter = function()
    return datadog_reporter_mock
  end
}

local spy_on_set_header
local spy_on_response
local plugin_handler = require "kong.plugins.grabid.handler"
local nginx_status = "200"

describe(
  "UNITTEST - Handler",
  function()
    local old_ngx = _G.ngx
    local old_kong = _G.kong

    local stubbed_kong
    local stubbed_ngx

    local grab_handler
    local consumer_mock = {
      select_by_custom_id = function()
        return {}
      end,
      cache_key = function()
        return "cache-key-kong"
      end
    }

    before_each(
      function()
        local response_200
        spy_keep_alive = spy.new(
          function(...)
          end
        )
        spy.on(consumer_mock, "select_by_custom_id")
        spy_on_set_header = spy.new(
          function(...)
          end
        )
        spy_on_response = spy.new(
          function(...)
          end
        )
        stubbed_ngx = {
          arg = { "upstream-content", true },
          log = function(...)
            return old_ngx.log(...)
          end,
          req = {
            get_method = function(...)
              return "GET"
            end,
            get_headers = function()
              return {
                ["Authorization"] = "Invalid appId:Signature",
                ["content-type"] = "application/json",
                ["Date"] = "Sun, 06 Nov 2016 08:49:37 GMT"
              }
            end,
            set_header = function(...)
              spy_on_set_header(...)
            end
          },
          ctx = {
            route = {
              host = nil,
              id = "5adb6b69-304b-4e5b-9777-0b72f5b3f035",
              methods = { "GET", "POST", "PUT" },
              paths = { "/authenticated" },
              service = { id = "92cb26b2-3663-4e30-b918-6bafd783ab39" }
            },
            req_uri = "/authenticated",
            service = {
              name = "grabpay"
            }
          },
          status = nginx_status,
          timer = {
            at = function (t, f, ...)
              return f(false, ...)
            end
          }
        }
        stubbed_kong = {
          db = {
            consumers = consumer_mock
          },
          request = {
            get_path = function(...)
              return "/authenticated"
            end,
            get_raw_query = function()
              return "x=debug"
            end,
            get_raw_body = function()
              return '{"name": "request"}'
            end,
            get_header = function(key)
              local headers = { ["Authorization"] = "Invalid appId:Signature" }
              return headers[key]
            end
          },
          response = {
            exit = function(...)
              spy_on_response(...)
            end
          },
          ctx = {
            shared = {
              x_security = {
                security = "hmac+client_credentials"
              }
            },
            plugin = {}
          }
        }
        _G.ngx = setmetatable(stubbed_ngx, { __index = old_ngx })

        _G.kong = setmetatable(stubbed_kong, { __index = old_kong })

        grab_handler = plugin_handler()

        response_200 = {
          status = 200,
          read_body = function(...)
            return [[]]
          end
        }
        httpc = {
          keepalive = true,
          set_timeout = function(...)
          end,
          connect = function(...)
          end,
          ssl_handshake = spy.new(
            function(...)
              return true, nil
            end
          ),
          request = spy.new(
            function()
              return response_200, nil
            end
          ),
          set_keepalive = function(...)
            spy_keep_alive(...)
            return 1, nil
          end
        }
        stub(datadog_reporter_mock, "tag_request")
        mock_redis = {
          get = spy.new(function (o, key) end),
          set = spy.new(function (o, key, value) end),
          del = spy.new(function (o, ...) end),
          set_keepalive = function () end,
          expire = spy.new(function (o, ...) end),
        }
      end
    )

    describe(
      "Unit: Test Client",
      function()
        local match = require("luassert.match")
        local _ = match._
        it(
          "Call 2 times and fail with security = 'hmac+client_credentials'",
          function()
            local consumer = { id = "consumer_uuid", custom_id = "custom_id", username = "user_name" }
            consumer_mock.select_by_custom_id = spy.new(
              function()
                return consumer
              end
            )
            httpc.request = spy.new(
              function()
                return nil, 401
              end
            )
            -- Ensure no setup before
            assert.are.same(nil, kong.ctx.plugin.hmac_verified)

            grab_handler:access(mock_config)

            -- It must call 2 times: "hmac" first and "client_credentials" later.
            assert.spy(httpc.request).was.called(2)

            -- HMAC was called, but fail. So we have "kong.ctx.plugin.hmac_verified" is false
            -- If HMAC was not called, "kong.ctx.plugin.hmac_verified" will be nil, not false
            assert.are.same(false, kong.ctx.plugin.hmac_verified)
          end
        )

        it(
          "expect send auth_type:hmac+client_credentials to datadog",
          function()
            httpc.request = spy.new(
              function()
                return nil, 401
              end
            )

            grab_handler:access(mock_config)

            assert.spy(datadog_reporter_mock.tag_request).was.called_with({ ["auth_type"] = "hmac+client_credentials" })
          end
        )

      end
    )
  end
)
