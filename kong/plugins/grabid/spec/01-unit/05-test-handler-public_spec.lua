--
-- Created by IntelliJ IDEA.
-- User: hoaian.nguyen
-- Date: 8/18/18
-- Time: 11:09 AM
-- To change this template use File | Settings | File Templates.
--

local kong_log_mock = require("kong.spec.utils.kong-log-mock")
package.loaded["kong.plugins.commons.log"] = kong_log_mock
package.loaded["kong.plugins.commons.opentracing.opentracing_bridge_tracer"] =
  require("kong.spec.utils.opentracing-bridge-tracer-mock")

-- Mock Http Client
local http = {
  _VERSION = "0.12"
}

local spy_keep_alive
local mt = {__index = http}
-- Ended Mock Http Client

local CONFIG_TIMEOUT, CONFIG_POOL, CONFIG_ALIVE_REQUESTS = 500, 440, 220
local mock_config = {
  host = "https://grabid.com",
  header_name = "Authorization",
  service_key = "OATUTH_MANAGER",
  service_token = "TOKEN_IS",
  timeout = CONFIG_TIMEOUT,
  alive_pool = CONFIG_POOL,
  alive_requests = CONFIG_ALIVE_REQUESTS,
  port = 443,
  gateway_id = "50775e244d674d3c9e8381f63c93d0ce",
  pass_headers = {"X-GID-AUX", "User-Agent"}
}
package.loaded["kong.request"] = {
  get_path = function(...)
    return "/authenticated"
  end,
  get_raw_query = function()
    return "x=debug"
  end
}
-- MOCK Kong Cache
local kong_cache = {
  get = function(...)
    local arg = {...}
    local loader = arg[4]
    local cache_callback = arg[6]
    return loader(arg[5], cache_callback)
  end
}

package.loaded["kong.singletons"] = {
  cache = kong_cache
}
-- END Mock Kong Cache

local httpc
local spy_on_set_header
local spy_on_response

package.loaded["resty.http"] = {
  new = function()
    local sock = {}
    httpc =
      setmetatable(
      {
        sock = sock,
        keepalive = true,
        set_timeout = function(...)
        end,
        connect = function(...)
        end,
        ssl_handshake = function(...)
          return true, nil
        end,
        set_keepalive = function(...)
          spy_keep_alive(...)
          return 1, nil
        end
      },
      mt
    )
    return httpc
  end
}
package.loaded["ssl.https"] = {
  request = function(...)
    return '{"signature":"xxx"}', 200
  end
}

local datadog_reporter_mock = {}
package.loaded["kong.plugins.commons.analytics"] = {
  get_datadog_reporter = function()
    return datadog_reporter_mock
  end
}

local plugin_handler = require "kong.plugins.grabid.handler"

local nginx_status = "200"
describe(
  "UNITTEST - Handler",
  function()
    local old_ngx = _G.ngx
    local old_kong = _G.kong
    local spy_on

    local snapshot

    local stubbed_kong
    local stubbed_ngx

    local grab_handler

    local unauthorized_error_message = {
      message = "Unauthorized"
    }

    before_each(
      function()
        snapshot = assert:snapshot()
        spy_keep_alive =
          spy.new(
          function(...)
          end
        )
        spy_on =
          spy.new(
          function(...)
          end
        )
        spy_on_set_header =
          spy.new(
          function(...)
          end
        )
        spy_on_response =
          spy.new(
          function(...)
          end
        )
        stubbed_ngx = {
          log = function(...)
            return old_ngx.log(...)
          end,
          print = function(...)
            return old_ngx.print(...)
          end,
          exit = function(...)
            return old_ngx.exist(...)
          end,
          req = {
            get_method = function(...)
              return "GET"
            end,
            get_headers = function()
              return {
                ["content-type"] = "application/json",
                ["Date"] = "Sun, 06 Nov 2016 08:49:37 GMT"
              }
            end,
            set_header = function(...)
              spy_on_set_header(...)
            end
          },
          ctx = {
            route = {
              host = nil,
              id = "5adb6b69-304b-4e5b-9777-0b72f5b3f035",
              methods = {"GET", "POST", "PUT"},
              paths = {"/authenticated"},
              service = {id = "92cb26b2-3663-4e30-b918-6bafd783ab39"}
            },
            req_uri = "/authenticated",
            service = {
              name = "grabpay"
            }
          },
          status = nginx_status
        }
        stubbed_kong = {
          request = {
            get_path = function(...)
              return "/authenticated"
            end,
            get_raw_query = function()
              return "x=debug"
            end,
            get_raw_body = function()
              return '{"name": "request"}'
            end,
            get_header = function(key)
              local headers = {["Authorization"] = "Bearer CLIENT_TOKEN"}
              return headers[key]
            end
          },
          response = {
            exit = function(...)
              spy_on_response(...)
            end
          },
          ctx = {
            shared = {
              x_security = {
                security = nil,
                scheme_name = nil,
                x_provider = nil
              }
            },
            plugin = {
              hmac_verified = false
            }
          },
          service = {
            request = {
              set_headers = spy.new(
                function()
                end
              )
            }
          }
        }
        _G.ngx = setmetatable(stubbed_ngx, {__index = old_ngx})
        _G.kong = setmetatable(stubbed_kong, {__index = old_kong})

        grab_handler = plugin_handler()

        stub(datadog_reporter_mock, "tag_request")
      end
    )

    after_each(
      function()
        snapshot:revert()
        _G.ngx = old_ngx
        _G.kong = old_kong
      end
    )

    describe(
      "Unit: Test Client",
      function()
        it(
          "If config set security is public, then we should not see anything happen in filter and hmac client",
          function()
            kong.ctx.shared.x_security.security = "public"
            grab_handler:access(mock_config)

            assert.spy(spy_on).was.called(0)
            assert.spy(spy_keep_alive).was.called(0)
            assert.spy(spy_on_response).was.was_not_called_with(401, unauthorized_error_message, nil)
          end
        )

        it(
          "expect send auth_type:public to datadog",
          function()
            kong.ctx.shared.x_security.security = "public"

            grab_handler:access(mock_config)

            assert.spy(datadog_reporter_mock.tag_request).was.called_with({ ["auth_type"] = "public" })
          end
        )

      end
    )
  end
)
