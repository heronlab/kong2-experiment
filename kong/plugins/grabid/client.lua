local bridge_tracer = require("kong.plugins.commons.opentracing.opentracing_bridge_tracer")
local pl_table = require("pl.tablex")
local http = require "resty.http"
local cjson = require "cjson"
local logger = require("kong.plugins.commons.log")
local monitor = require("kong.plugins.grabid.monitor")

local pcall = pcall
local function parse_json(body)
  if body then
    local status, res = pcall(cjson.decode, body)
    if status then
      return res
    end
  end
end

local _Retries = {}

function _Retries:new(cb, max)
  local state = { tried = 0, callback = cb, max = max }
  self.__index = self
  return setmetatable(state, self)
end

function _Retries:retry(...)
  self.tried = self.tried + 1
  return self.callback(...)
end

local function GrabIDStatusTransition(status)
  if tonumber(status) == nil then
    return status
  end
  if tonumber(status) >= 500 then
    return 500
  end
  local status_transition = {
    [400] = 401,
    [401] = 401,
    [422] = 401,
    [404] = 500,
    [403] = 403
  }
  local transit = status_transition[status]
  return transit or status
end

local grab_id_res = {
  TIMEOUT = 504,
  CONNECTION_TIMEOUT = 599
}

local function api_call(...)
  local args = { ... }
  local host = args[1]
  local timeout = args[8]
  local httpc = http.new()
  httpc:connect(host, 443)
  httpc:set_timeout(timeout)
  local ok, err = httpc:ssl_handshake(true, host, false)

  if err == "closed" or err == "timeout" then
    logger.err("@TODO Retry for few times via setting: GrabID - socket: " .. err)
    monitor.tag_request(grab_id_res.TIMEOUT)
    return nil, err
  end
  if not ok then
    logger.err("ssl handle shake ", err)
    monitor.tag_request(grab_id_res.CONNECTION_TIMEOUT)
    return nil, 500
  end

  local final_headers = args[5]

  local opentracing_ctx = kong.ctx.shared.x_opentracing
  if opentracing_ctx and opentracing_ctx.binary_context and not bridge_tracer.not_found then
    local tracer = bridge_tracer.new_from_global()
    local parent_context = tracer:binary_extract(opentracing_ctx.binary_context)
    local carrier = {}
    tracer:http_headers_inject(parent_context, carrier)
    final_headers = pl_table.union(final_headers, carrier)
  end

  local pool = args[6]
  local requests = args[7]
  local path = args[2]
  local res, err = httpc:request {
    path = path,
    method = args[3],
    body = args[4],
    headers = final_headers,
    keepalive_timeout = timeout,
    keepalive_pool = pool,
    keepalive_requests = requests
  }
  --    local count, _er = httpc:get_reused_times()
  --    if count ~= nil then
  --        logger.inspect("COUNTING: " .. count)
  --    end

  local api_error, content, grab_id_headers
  if not res then
    api_error = err
  else
    local body = res:read_body()
    local grab_content = parse_json(body)
    grab_id_headers = res.headers

    --err from grabid: [timeout, ???, ???], we will catch `???` in the future
    if err == "timeout" then
      monitor.tag_request(grab_id_res.TIMEOUT)
    else
      monitor.tag_request(res.status)
    end

    if res.status >= 400 then
      api_error = res.status
    end
    if grab_content == nil and api_error == nil then
      api_error = "Can not parse GrabID's content"
    else
      content = grab_content
    end

  end
  local ok, _ = httpc:set_keepalive()
  if ok ~= 1 then
    logger.err("CAN NOT KEEP ALIVE")
  end
  if api_error ~= nil then
    logger.err("error while calling ", host or "", path or "", ": ", api_error)
  end
  return content, GrabIDStatusTransition(api_error), grab_id_headers
end

local _Client = {}

function _Client:new(host, retries)
  local setting = { host = host, retries = retries }
  self.__index = self
  return setmetatable(setting, self)
end

function _Client:send(url, method, body, header, pool, requests, timeout)
  local _timeout = timeout or 2000
  local retry = _Retries:new(api_call, self.retries)
  local grab_id_content, err, grab_id_headers
  repeat
    grab_id_content, err, grab_id_headers = retry:retry(self.host, url, method, body, header, pool, requests, _timeout)
    if grab_id_content or (err ~= "timeout" and err ~= "closed" and err ~= nil) then
      return grab_id_content, err, grab_id_headers
    end
    logger.warn("retry http request to ", url, " because of ", err, ", tried ", retry.tried)
  until retry.tried == retry.max

  return grab_id_content, err, grab_id_headers
end

return _Client
