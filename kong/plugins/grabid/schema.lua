--
-- Created by IntelliJ IDEA.
-- User: hoaian.nguyen
-- Date: 8/15/18
-- Time: 11:12 AM
-- To change this template use File | Settings | File Templates.
--

return {
  no_consumer = true,
  fields = {
    header_name = {type = "string", required = true, default = "Authorization"},
    host = {type = "string", required = true},
    gateway_id = {type = "string", required = true},
    service_key = {type = "string", required = true, default = "OAUTH2_MANAGE"},
    timeout = {type = "number", default = 500},
    port = {type = "number", default = 443},
    alive_pool = {type = "number", default = 440},
    alive_requests = {type = "number", default = 220},
    service_token = {type = "string", required = true},
    retries = {type = "number", default = 5},
    pass_headers = {type = "array"},
    correlation_id_header = {type = "string"},
    fail_away = {type = "boolean", required = false},
    token_cache_ttl = {type = "number", required = false, default = 0}, -- 0 is disabled, uses JWT expiry
    redis_host = {type = "string", required = false},
    redis_port = {type = "number", default = 6379},
    redis_password = {type = "string"},
    redis_timeout = {type = "number", default = 2000},
    redis_database = {type = "number", default = 0},
  },
  self_check = function(schema, plugin, dao, is_updating)
    return true
  end
}
