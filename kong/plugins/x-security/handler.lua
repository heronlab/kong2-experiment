-- Imports
local profiling = require("kong.plugins.commons.profiling")
local BasePlugin = require("kong.plugins.base_plugin")

-- Constants
local trace_obj = profiling.trace_obj

local SecurityHandler = BasePlugin:extend()
SecurityHandler.PRIORITY = 1050
SecurityHandler.VERSION = "0.2.0"

function SecurityHandler:new()
  SecurityHandler.super.new(self, "x-security")
end

function SecurityHandler:access(conf)
  SecurityHandler.super.access(self)
  kong.ctx.shared.x_security = {x_provider = conf.x_provider, scheme_name = conf.scheme_name, security = conf.security}
end

trace_obj("x-security", SecurityHandler, {"access"})
return SecurityHandler
