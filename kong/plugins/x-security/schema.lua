return {
  no_consumer = true,
  fields = {
    x_provider = { type = "string", required = false },
    scheme_name = { type = "string", required = false },
    security = { type = "string", required = true, default = "hmac", enum = { "client_credentials", "authorization_code", "hmac", "public", "hmac+client_credentials" } }
  }
}
