package.loaded["kong.plugins.commons.profiling"] = require("kong.spec.utils.profiling-mock")
package.loaded.resty = nil
package.loaded.kong = nil

local plugin_handler = require "kong.plugins.x-security.handler"

describe(
  "UNITTEST - Handler",
  function()
    local old_kong = _G.kong
    local mock_config = {
      hmac = true,
      route_id = "5adb6b69-304b-4e5b-9777-0b72f5b3f035"
    }

    local snapshot
    local stubbed_kong
    local grab_handler

    before_each(
      function()
        snapshot = assert:snapshot()
        stubbed_kong = {
          ctx = {
            shared = {hmac = false}
          },
          request = {
            get_header = spy.new(
              function()
              end
            )
          }
        }
        _G.kong = setmetatable(stubbed_kong, {__index = old_kong})

        mock_config = {
          security = "public",
          scheme_name = "Auth"
        }
        grab_handler = plugin_handler()
      end
    )

    after_each(
      function()
        snapshot:revert()
        _G.kong = old_kong
      end
    )

    describe(
      "Unit: Test Client",
      function()
        it(
          "kong.ctx.shared.x_security.security reflects config.security",
          function()
            mock_config.security = "hmac"
            grab_handler:access(mock_config)
            assert.are.same("hmac", kong.ctx.shared.x_security.security)
          end
        )
        it(
          "kong.ctx.shared.x_security.scheme_name reflects config.scheme_name",
          function()
            mock_config.scheme_name = "OAuth2"
            grab_handler:access(mock_config)
            assert.are.same("OAuth2", kong.ctx.shared.x_security.scheme_name)
          end
        )
      end
    )
  end
)
