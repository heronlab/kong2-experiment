-- Imports
local timestamp = require "kong.tools.timestamp"
local reports = require "kong.reports"
local redis = require "resty.redis"

-- Globals
local kong = kong
local pairs = pairs
local null = ngx.null
local shm = ngx.shared.kong_rate_limiting_counters -- TODO: Create a separate SHM for quota
local fmt = string.format
local sock_opts = {}

-- Constants
local EMPTY_UUID = "00000000-0000-0000-0000-000000000000"
local EXPIRATIONS = {
  second = 1,
  minute = 60,
  hour = 3600,
  day = 86400,
  month = 2592000,
  year = 31536000,
}

local function is_present(str)
  return str and str ~= "" and str ~= null
end

local get_local_key = function(conf, identifier, period, period_date)
  local service_key = (conf.service and conf.service.name) or conf.service_id or EMPTY_UUID
  local route_key = (conf.route and conf.route.name) or conf.route_id or EMPTY_UUID

  return fmt("x-quota:%s:%s:%s:%s:%s", route_key, service_key, identifier,
    period_date, period)
end

return {
  ["local"] = {
    increment = function(conf, limits, identifier, current_timestamp, value)
      local periods = timestamp.get_timestamps(current_timestamp)
      for period, period_date in pairs(periods) do
        if limits[period] then
          local cache_key = get_local_key(conf, identifier, period, period_date)
          local newval, err = shm:incr(cache_key, value, 0)
          if not newval then
            kong.log.err("could not increment counter for period '", period, "': ", err)
            return nil, err
          end
        end
      end

      return true
    end,
    usage = function(conf, identifier, period, current_timestamp)
      local periods = timestamp.get_timestamps(current_timestamp)
      local cache_key = get_local_key(conf, identifier, period, periods[period])

      local current_metric, err = shm:get(cache_key)
      if err then
        return nil, err
      end

      return current_metric or 0
    end
  },
  ["redis"] = {
    increment = function(conf, limits, identifier, current_timestamp, value)
      local red = redis:new()
      red:set_timeout(conf.redis_timeout)
      -- use a special pool name only if redis_database is set to non-zero
      -- otherwise use the default pool name host:port
      sock_opts.pool = conf.redis_database and
        conf.redis_host .. ":" .. conf.redis_port ..
          ":" .. conf.redis_database
      local ok, err = red:connect(conf.redis_host, conf.redis_port,
        sock_opts)
      if not ok then
        kong.log.err("failed to connect to Redis: ", err)
        return nil, err
      end

      local times, err = red:get_reused_times()
      if err then
        kong.log.err("failed to get connect reused times: ", err)
        return nil, err
      end

      if times == 0 then
        if is_present(conf.redis_password) then
          local ok, err = red:auth(conf.redis_password)
          if not ok then
            kong.log.err("failed to auth Redis: ", err)
            return nil, err
          end
        end

        if conf.redis_database ~= 0 then
          -- Only call select first time, since we know the connection is shared
          -- between instances that use the same redis database

          local ok, err = red:select(conf.redis_database)
          if not ok then
            kong.log.err("failed to change Redis database: ", err)
            return nil, err
          end
        end
      end

      local keys = {}
      local idx = 0
      local periods = timestamp.get_timestamps(current_timestamp)
      local array_periods = {}
      local array_limits = {}
      local params = {}
      for period, period_date in pairs(periods) do
        if limits[period] then
          local cache_key = get_local_key(conf, identifier, period, period_date)
          idx = idx + 1
          keys[idx] = cache_key
          array_periods[idx] = period
          array_limits[idx] = limits[period]
          params[idx] = cache_key
        end
      end

      for i = 1, idx do
        params[idx + i] = array_limits[i]
      end

      local results, err = red:eval("local result = {} local exceeded = 0 for i = 1, #ARGV do local quota = redis.call('GET',KEYS[i]) if quota and tonumber(quota) >= tonumber(ARGV[i]) then result[i] = tonumber(ARGV[i]) + 1 exceeded = 1 else if exceeded == 1 then result[i] = tonumber(quota) else result[i] = redis.call('INCR', KEYS[i]) end end end return result",
        idx, table.unpack(params))

      if err then
        kong.log.err("failed to commit pipeline in Redis: ", err)
        return nil, err
      end

      local res = {}
      for i = 1, idx do
        res[array_periods[i]] = {
          ["key"] = keys[i],
          ["quota"] = results[i],
          ["limit"] = limits[array_periods[i]]
        }
      end
      local ok, err = red:set_keepalive(10000, 100)
      if not ok then
        kong.log.err("failed to set Redis keepalive: ", err)
        return nil, err
      end

      return res, nil
    end,
    expire = function(conf, res)
      local expire_keys = {}
      for k, v in pairs(res) do
        if v.quota == 1 then
          expire_keys[v.key] = EXPIRATIONS[k]
        end
      end
      if next(expire_keys) == nil then
        return nil, nil
      end
      local red = redis:new()
      red:set_timeout(conf.redis_timeout)
      -- use a special pool name only if redis_database is set to non-zero
      -- otherwise use the default pool name host:port
      sock_opts.pool = conf.redis_database and
        conf.redis_host .. ":" .. conf.redis_port ..
          ":" .. conf.redis_database
      local ok, err = red:connect(conf.redis_host, conf.redis_port,
        sock_opts)
      if not ok then
        kong.log.err("failed to connect to Redis: ", err)
        return nil, err
      end

      local times, err = red:get_reused_times()
      if err then
        kong.log.err("failed to get connect reused times: ", err)
        return nil, err
      end

      if times == 0 then
        if is_present(conf.redis_password) then
          local ok, err = red:auth(conf.redis_password)
          if not ok then
            kong.log.err("failed to auth Redis: ", err)
            return nil, err
          end
        end

        if conf.redis_database ~= 0 then
          -- Only call select first time, since we know the connection is shared
          -- between instances that use the same redis database

          local ok, err = red:select(conf.redis_database)
          if not ok then
            kong.log.err("failed to change Redis database: ", err)
            return nil, err
          end
        end
      end

      reports.retrieve_redis_version(red)

      red:init_pipeline()
      for k, v in pairs(expire_keys) do
        red:expire(k, v)
      end

      local _, err = red:commit_pipeline()
      if err then
        kong.log.err("failed to commit expire pipeline in Redis: ", err)
        return nil, err
      end

      local ok, err = red:set_keepalive(10000, 100)
      if not ok then
        kong.log.err("failed to set Redis keepalive: ", err)
        return nil, err
      end
      return nil, nil
    end
  }
}
