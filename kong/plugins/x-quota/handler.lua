-- Imports
local profiling = require("kong.plugins.commons.profiling")
local policies = require("kong.plugins.x-quota.policies")
local response = require("kong.plugins.utils.response")
local BasePlugin = require("kong.plugins.base_plugin")
local logger = require("kong.plugins.commons.log")

-- Globals
local kong = kong
local ngx = ngx
local max = math.max
local time = ngx.time
local pairs = pairs
local tostring = tostring
local timer_at = ngx.timer.at
local trace_obj = profiling.trace_obj

-- Constants
local QUOTA_LIMIT = "X-Quota-Limit"
local QUOTA_REMAINING = "X-Quota-Remaining"

-- Instantiate QuotaHandler plugin
local QuotaHandler = BasePlugin:extend()

-- Plugin priority and version
QuotaHandler.PRIORITY = 902
QuotaHandler.VERSION = "1.0.0"

local function get_identifier(conf)
  if conf.limit_by == "userID" then
    local auth_session = kong.ctx.shared.session_user
    return auth_session and auth_session["$ctx.session.UserID"]
  end

  if conf.limit_by == "serviceUserID" then
    local auth_session = kong.ctx.shared.session_user
    return auth_session and auth_session["$ctx.session.ServiceUserID"]
  end

  if conf.limit_by == "consumer" then
    local consumer = kong.client.get_consumer()
    return consumer and consumer.id
  end

  if conf.limit_by == "partnerID" then
    local auth_session = kong.ctx.shared.session_user
    return auth_session and auth_session["$ctx.session.PartnerID"]
  end

  return nil
end

local function get_usage(conf, identifier, current_timestamp, limits)
  local usage = {}
  local stop

  for period, limit in pairs(limits) do
    local current_usage, err = policies[conf.policy].usage(conf, identifier, period, current_timestamp)
    if err then
      return nil, nil, err
    end

    -- What is the current usage for the configured limit name?
    local remaining = limit - current_usage

    -- Recording usage
    usage[period] = {
      limit = limit,
      remaining = remaining
    }

    if remaining <= 0 then
      stop = period
    end
  end

  return usage, stop
end

local function increment(premature, conf, ...)
  if premature then
    return
  end

  policies[conf.policy].increment(conf, ...)
end

local function expire(premature, conf, ...)
  if premature then
    return
  end

  policies[conf.policy].expire(conf, ...)
end

local function process_local(conf, limits, identifier, current_timestamp)
  local usage, stop, err = get_usage(conf, identifier, current_timestamp, limits)
  if err then
    if conf.fault_tolerant then
      logger.err("failed to get usage: ", tostring(err))
    else
      logger.err(err)
      return kong.response.exit(ngx.HTTP_INTERNAL_SERVER_ERROR, response.newMessage(ngx.HTTP_INTERNAL_SERVER_ERROR))
    end
  end
  if usage then
    -- Adding headers
    if not conf.hide_client_headers then
      local headers = {}
      for k, v in pairs(usage) do
        if stop == nil or stop == k then
          v.remaining = v.remaining - 1
        end

        headers[QUOTA_LIMIT .. "-" .. k] = v.limit
        headers[QUOTA_REMAINING .. "-" .. k] = max(0, v.remaining)
      end

      kong.ctx.plugin.headers = headers
    end

    -- If limit is exceeded, terminate the request
    if stop then
      return kong.response.exit(ngx.HTTP_TOO_MANY_REQUESTS, response.newMessage(nil, "API quota exceeded"))
    end
  end

  --kong.ctx.plugin.timer = function()
  local ok, err = timer_at(0, increment, conf, limits, identifier, current_timestamp, 1)
  if not ok then
    logger.err("failed to create timer: ", err)
  end
  --end
end

local function process_redis(conf, limits, identifier, current_timestamp)
  local res, err = policies[conf.policy].increment(conf, limits, identifier, current_timestamp, 1)
  if err then
    if conf.fault_tolerant then
      logger.err("failed to increment: ", tostring(err))
    else
      logger.err(err)
      return kong.response.exit(ngx.HTTP_INTERNAL_SERVER_ERROR, response.newMessage(ngx.HTTP_INTERNAL_SERVER_ERROR))
    end
  end

  local exceeded = false
  if res then
    local headers = {}
    for k, v in pairs(res) do
      if v.quota > v.limit then
        exceeded = true
      end
      if not conf.hide_client_headers then
        headers[QUOTA_LIMIT .. "-" .. k] = v.limit
        headers[QUOTA_REMAINING .. "-" .. k] = max(0, v.limit - v.quota)

      end
    end

    kong.ctx.plugin.headers = headers

    -- If limit is exceeded, terminate the request
    if exceeded then
      return kong.response.exit(ngx.HTTP_TOO_MANY_REQUESTS, response.newMessage(nil, "API quota exceeded"))
    end
    local ok, err = timer_at(0, expire, conf, res)
    if not ok then
      logger.err("failed to create timer: ", err)
    end
  end

end

function QuotaHandler:new()
  QuotaHandler.super.new(self, "x-quota")
end

function QuotaHandler:access(conf)
  QuotaHandler.super.access(self)

  local current_timestamp = time() * 1000

  -- Consumer is identified by ip address or authenticated_credential id
  local identifier = get_identifier(conf)

  -- Load current metric for configured period
  local limits = {
    second = conf.second,
    minute = conf.minute,
    hour = conf.hour,
    day = conf.day,
    month = conf.month,
    year = conf.year
  }

  if identifier == nil or identifier == "" then
    logger.err("failed to get quota identifier (" .. conf.limit_by .. ")")
    return
  end

  if not type(identifier) == "string" then
    logger.err(
      "invalid type of quota identifier (" ..
        conf.limit_by .. ') expected "string", got "' .. type(identifier) .. '"'
    )
    return
  end

  if conf.policy == "redis" then
    process_redis(conf, limits, identifier, current_timestamp)
  else
    process_local(conf, limits, identifier, current_timestamp)
  end


end

function QuotaHandler:header_filter(_)
  QuotaHandler.super.header_filter(self)

  local headers = kong.ctx.plugin.headers
  if headers then
    kong.response.set_headers(headers)
  end
end

function QuotaHandler:log(_)
  if kong.ctx.plugin.timer then
    kong.ctx.plugin.timer()
  end
end

trace_obj("x-quota", QuotaHandler, {"access"})
return QuotaHandler
