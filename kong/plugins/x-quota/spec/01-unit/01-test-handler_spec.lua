local kong_log_mock = require("kong.spec.utils.kong-log-mock")
package.loaded["kong.plugins.commons.log"] = kong_log_mock
package.loaded["kong.plugins.commons.profiling"] = require("kong.spec.utils.profiling-mock")
local kong_mock = {
  ctx = {
    shared = {
      session_user = {
        ["$ctx.session.UserID"] = "context-session-user-id",
        ["$ctx.session.ServiceUserID"] = "context-service-user-id",
        ["$ctx.session.RegisteredOnService"] = "context-service-registered-on-service",
        ["$ctx.session.Oauth2ContextID"] = "context-service-oauth2-context-id",
        ["$ctx.session.ClientID"] = "context-service-client-id",
        ["$ctx.session.TokenID"] = "context-service-token-id",
        ["$ctx.session.PartnerID"] = "context-service-partner-id"
      }
    },
    plugin = {}
  }
}
_G.kong = kong_mock

describe(
  "x-quota handler",
  function()
    local handler
    local current_test_case
    local shm_mock = {
      ["get"] = function(self, key)
        assert(key:sub(1, #current_test_case.exp_key) == current_test_case.exp_key, "Assertion failed : Key `" .. key .. "` must start with `" .. current_test_case.exp_key .. "`.")
        return 0, nil
      end,
      ["incr"] = function(self, cache_key, value, exp)
        return 0, nil
      end
    }
    local org_shm = ngx.shared.kong_rate_limiting_counters
    ngx.shared.kong_rate_limiting_counters = shm_mock
    local QuotaHandler = require "kong.plugins.x-quota.handler"

    before_each(
      function(...)
        handler = QuotaHandler()
      end
    )

    after_each(
      function()
        kong.ctx.plugin.headers = nil
      end
    )

    teardown(function()
      ngx.shared.kong_rate_limiting_counters = org_shm
    end)


    describe(
      "Test if quota plugin returns proper headers",
      function(...)
        local test_cases = {
          {
            conf = {
              limit_by = "partner",
              second = 1,
              policy = "local",
            },
            name = "Wrong limit_by config",
            exp_headers = nil,
            exp_key = nil
          },
          {
            conf = {
              limit_by = "partnerID",
              second = 1,
              policy = "local",
              service = {
                ["name"] = "service-name"
              },
              route = {
                ["name"] = "route-name"
              },
            },
            name = "Valid limit_by config, quota in sec",
            exp_headers = {
              ["X-Quota-Remaining-second"] = 0,
              ["X-Quota-Limit-second"] = 1,
            },
            exp_key = "x-quota:route-name:service-name:"
          },
          {
            conf = {
              limit_by = "userID",
              minute = 1,
              policy = "local",
              service = {
                ["name"] = "service-name"
              },
              ["route_id"] = "route-id",
            },
            name = "Valid limit_by config, quota in min",
            exp_headers = {
              ["X-Quota-Limit-minute"] = 1,
              ["X-Quota-Remaining-minute"] = 0,
            },
            exp_key = "x-quota:route-id:service-name:"
          },
          {
            conf = {
              limit_by = "serviceUserID",
              hour = 1,
              policy = "local",
              ["service_id"] = "service-id",
              route = {
                ["name"] = "route-name"
              }
            },
            name = "Valid limit_by config, quota in hour",
            exp_headers = {
              ["X-Quota-Limit-hour"] = 1,
              ["X-Quota-Remaining-hour"] = 0,
            },
            exp_key = "x-quota:route-name:service-id:"
          },
          {
            conf = {
              limit_by = "serviceUserID",
              day = 1,
              policy = "local",
              ["service_id"] = "service-id",
              ["route_id"] = "route-id",
            },
            name = "Valid limit_by config, quota in day",
            exp_headers = {
              ["X-Quota-Limit-day"] = 1,
              ["X-Quota-Remaining-day"] = 0,
            },
            exp_key = "x-quota:route-id:service-id:"
          },
          {
            conf = {
              limit_by = "serviceUserID",
              month = 1,
              policy = "local",
              service = {
              },
              route = {
              }
            },
            name = "Valid limit_by config, quota in month",
            exp_headers = {
              ["X-Quota-Limit-month"] = 1,
              ["X-Quota-Remaining-month"] = 0,
            },
            exp_key = "x-quota:00000000-0000-0000-0000-000000000000:00000000-0000-0000-0000-000000000000:"
          },
          {
            conf = {
              limit_by = "serviceUserID",
              year = 1,
              policy = "local",
            },
            name = "Valid limit_by config, quota in year",
            exp_headers = {
              ["X-Quota-Limit-year"] = 1,
              ["X-Quota-Remaining-year"] = 0,
            },
            exp_key = "x-quota:00000000-0000-0000-0000-000000000000:00000000-0000-0000-0000-000000000000:"
          },
        }
        for _, tc in ipairs(test_cases) do
          test(
            tc.name,
            function(...)
              current_test_case = tc
              handler:access(tc.conf)
              assert.are.same(tc.exp_headers, kong.ctx.plugin.headers)
            end
          )
        end
      end
    )

  end
)
