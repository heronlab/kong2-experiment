-- Base handler for shadowing responses to Clair. It's used by x-clair-client-response-log and x-clair-upstream-response-log

local cjson = require("cjson")
local clair = require("kong.plugins.commons.clair")
local Object = require "kong.plugins.commons.object.classic"
local kong = kong
local ngx = ngx

local ResponseLogHandler = Object:extend()

function ResponseLogHandler:new(requestType)
  self._requestType = requestType or "unknown"
end

function ResponseLogHandler:header_filter(conf)
  if not clair.getIdentifier(kong) then
    return
  end

  local client = clair.getClient(conf)

  client:track(
    {
      Type = self._requestType,
      WatcherName = clair.WATCHER_NAME,
      KeyValuePairs = {
        {
          ["Key"] = clair.CORRELATION_ID,
          ["Value"] = clair.getIdentifier(kong)
        },
        {
          ["Key"] = "headers",
          ["Value"] = cjson.encode(kong.response.get_headers())
        }
      }
    }
  )
end

function ResponseLogHandler:body_filter(conf)
  if not clair.getIdentifier(kong) then
    return
  end

  local sequence = kong.ctx.plugin.sequence or 0
  if not ngx.arg[2] then
    local client = clair.getClient(conf)
    client:track(
      {
        Type = self._requestType,
        WatcherName = clair.WATCHER_NAME,
        KeyValuePairs = {
          {
            ["Key"] = clair.CORRELATION_ID,
            ["Value"] = clair.getIdentifier(kong)
          },
          {
            ["Key"] = "body",
            ["Value"] = ngx.arg[1]
          },
          {
            ["Key"] = "sequence",
            ["Value"] = sequence
          }
        }
      }
    )
    kong.ctx.plugin.sequence = sequence + 1
  end
end

return ResponseLogHandler
