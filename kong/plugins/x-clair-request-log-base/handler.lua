-- Base handler for shadowing requests to Clair. It's used by x-clair-client-request-log and x-clair-upstream-request-log

local cjson = require("cjson")
local clair = require("kong.plugins.commons.clair")
local Object = require("kong.plugins.commons.object.classic")
local kong = kong

local function capture(requestType, conf, kong)
    if not clair.getIdentifier(kong) then
        return
    end

    local headers = kong.request.get_headers() or {}

    local client = clair.getClient(conf)

    client:track(
        {
            Type = requestType,
            WatcherName = clair.WATCHER_NAME,
            KeyValuePairs = {
                {
                    ["Key"] = clair.CORRELATION_ID,
                    ["Value"] = clair.getIdentifier(kong)
                },
                {
                    ["Key"] = "method",
                    ["Value"] = kong.request.get_method()
                },
                {
                    ["Key"] = "path",
                    ["Value"] = kong.request.get_path()
                },
                {
                    ["Key"] = "headers",
                    ["Value"] = cjson.encode(headers)
                },
                {
                    ["Key"] = "body",
                    ["Value"] = kong.request.get_raw_body()
                },
                {
                    ["Key"] = "query",
                    ["Value"] = kong.request.get_raw_query()
                }
            }
        }
    )
end

local RequestLogHandler = Object:extend()

function RequestLogHandler:new(requestType)
    self._requestType = requestType or "unknown"
end

function RequestLogHandler:rewrite(conf)
    capture(self._requestType, conf, kong)
end

function RequestLogHandler:access(conf)
    capture(self._requestType, conf, kong)
end

return RequestLogHandler
