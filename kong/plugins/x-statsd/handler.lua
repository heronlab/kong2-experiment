-- Imports
local profiling = require("kong.plugins.commons.profiling")
local BaseAnalyticsPlugin = require("kong.plugins.x-base-analytics.handler")

-- Constants
local trace_obj = profiling.trace_obj

local StatsdHandler = BaseAnalyticsPlugin:extend()
StatsdHandler.PRIORITY = 11
StatsdHandler.VERSION = "0.1.0"

function StatsdHandler:new()
  StatsdHandler.super.new(self, "x-statsd")
end

function StatsdHandler:log(conf)
  if ngx.ctx.service then
    StatsdHandler.super.log(self, conf)
  end
end

trace_obj("x-statsd", StatsdHandler, {"log"})
return StatsdHandler
