local tablex = require "pl.tablex"
local BaseAnalyticsSchema = require "kong.plugins.x-base-analytics.schema"

local default_metrics = {
  {
    name = "status_count",
    stat_type = "counter",
    sample_rate = 1,
    tags = {"app:kong"}
  },
  {
    name = "latency",
    stat_type = "timer",
    tags = {"app:kong"}
  },
  {
    name = "status_count_per_user",
    stat_type = "counter",
    sample_rate = 1,
    consumer_identifier = "custom_id",
    tags = {"app:kong"}
  },
  {
    name = "latency_per_user",
    stat_type = "timer",
    tags = {"app:kong"},
    consumer_identifier = "custom_id"
  }
}

local metrics = tablex.union(BaseAnalyticsSchema.fields.metrics, {default = default_metrics})
local fields = tablex.union(BaseAnalyticsSchema.fields, {metrics = metrics})
local StatsdSchema = tablex.union(BaseAnalyticsSchema, {fields = fields})
return StatsdSchema
