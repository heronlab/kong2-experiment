package.loaded["kong.plugins.commons.profiling"] = require("kong.spec.utils.profiling-mock")
local StatsdHandler = require "kong.plugins.x-statsd.handler"
local StatsdSchema = require "kong.plugins.x-statsd.schema"

local base_analytics_mock = {
  log = function()
  end
}

describe(
  "Statsd handler",
  function()
    before_each(
      function()
        stub(base_analytics_mock, "log")
      end
    )

    _G.kong = {
      request = {
        get_header = spy.new(
          function()
          end
        )
      },
      ctx = {
      },
    }

    it(
      "should not throw exception",
      function()
        StatsdHandler()
      end
    )

    it(
      "should not call log function if ctx service nil",
      function()
        kong.ctx.service = nil
        local conf = {
          host = "host-name",
          service_version_delimiter = "_",
          port = "134",
          metrics = {
            {name = "status_count", ["stat_type"] = "counter", tags = {"app:kong"}}
          }
        }

        local handler = StatsdHandler()
        handler.super = base_analytics_mock
        handler:log(conf)

        assert.spy(base_analytics_mock.log).was_not_called_with()
      end
    )
  end
)

describe(
  "Statsd Schema",
  function()
    it(
      "should override default metrics",
      function()
        assert.are.equal(4, #StatsdSchema.fields.metrics.default)
      end
    )
  end
)
