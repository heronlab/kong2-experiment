local timestamp = require "kong.tools.timestamp"

local function get_utc_in_seconds()
  return math.floor(timestamp.get_utc() / 1000)
end

local function get_utc()
  return timestamp.get_utc()
end

return {
  get_utc_in_seconds = get_utc_in_seconds,
  get_utc = get_utc
}
