local function validate_percent(value)
  if value < 0 or value > 100 then
    return false, "should be in between 0 and 100"
  end
  return true, nil
end

local function not_negative(val)
  if val < 0 then
    return false, "should not be negative"
  end
  return true, nil
end

return {
  no_consumer = true,
  fields = {
    identifier = {type = "string", required = true}, -- keep this for back-compatible, plan to remove.
    error_threshold_percent = {type = "number", required = true, func = validate_percent},
    max_concurrent = {type = "number", required = true, func = not_negative},
    request_volume_threshold = {type = "number", required = true, default = 20, func = not_negative},
    sleep_window_in_seconds = {type = "number", required = true, default = 5, func = not_negative}
  }
}
