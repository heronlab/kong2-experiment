local tostring = tostring
local bit = require("bit")
local timestamp = require "kong.plugins.x-circuit-breaker.timestamp"
local logger = require("kong.plugins.commons.log")

-- constants
local SHM_NAME = "grab_circuit_breaker"
local WINDOW_SIZE = 1 * 1000 --milliseconds
local NUMBER_OF_WINDOWS = 10

local CTR_SUCCESS = 0x00000001
local CTR_FAILURE = 0x00010000

local PHASE_START = 1
local PHASE_END = -1

local function key_for(key_prefix, identifier)
  return key_prefix .. ":" .. identifier
end

local function key_for_with_bucket(key_prefix, identifier, bucket)
  return key_prefix .. ":" .. identifier .. ":" .. bucket
end

local checker = {}

-- Extract the value of the counter at `idx` from multi-counter `multictr`.
-- @param multictr A 32-bit multi-counter holding 2 values.
-- @param idx The shift index specifying which counter to get.
-- @return The 16-bit value extracted from the 32-bit multi-counter.
local function ctr_get(multictr, idx)
  return bit.band(multictr / idx, 0xff)
end

local function get_bucket(self)
  local current_timestamp = timestamp:get_utc()
  local bucket = math.floor(current_timestamp / self.window_size)
  return bucket
end

local function get_request_count(self, identifier, bucket)
  local counter_key = key_for_with_bucket(self.TARGET_COUNTER, identifier, bucket)
  local multictr, err = self.shm:get(counter_key)
  if err then
    return nil, err
  end

  if multictr == nil then
    return {failure = 0, total = 0}, nil
  end

  local failure = ctr_get(multictr, CTR_FAILURE)
  local success = ctr_get(multictr, CTR_SUCCESS)
  return {failure = failure, total = failure + success}, nil
end

-- Get circuit status.
-- Returns circuit status: "open", "closed", "outdated"
-- (outdated: if circuit status has been outdated, client has to re-check upstream status)
function checker:get_status(conf)
  local identifier = conf.identifier
  local open, err = self.shm:get(key_for(self.CIRCUIT_OPEN, identifier))
  if err ~= nil then
    return nil, err
  end

  logger.info("circuit status ", open)

  if open then
    local status = "open"

    local last_check_key = key_for(self.LAST_CHECK_TIMESTAMP, identifier)

    local current_timestamp_in_seconds = timestamp:get_utc_in_seconds()
    local last_check_timestamp_in_seconds = self.shm:get(last_check_key)

    local time_window = current_timestamp_in_seconds - last_check_timestamp_in_seconds

    if time_window > conf.sleep_window_in_seconds then
      local new_val = self.shm:incr(last_check_key, time_window)
      --If new_val is different, upstream was re-checked by other coroutines already.
      if new_val == current_timestamp_in_seconds then
        logger.info("circuit breaker status is outdated")

        status = "outdated"
      end
    end
    return status
  end
  return "closed"
end

local function report_counter(self, ctr_type, conf, reset_on_success)
  local identifier = conf.identifier
  local bucket = get_bucket(self)
  local counter_key = key_for_with_bucket(self.TARGET_COUNTER, identifier, bucket)
  local multictr, err = self.shm:incr(counter_key, ctr_type, 0)
  if err ~= nil then
    return err
  end

  if ctr_type == CTR_FAILURE then
    local failure = ctr_get(multictr, CTR_FAILURE)
    local success = ctr_get(multictr, CTR_SUCCESS)
    local total_requests = failure + success
    for i = 1, self.number_of_windows - 1 do
      local prev_request_count, err = get_request_count(self, conf.identifier, bucket - i)
      if err then
        return nil, err
      end

      total_requests = total_requests + prev_request_count.total
      failure = failure + prev_request_count.failure
    end

    if
      (total_requests >= conf.request_volume_threshold and
        failure * 100.0 / (total_requests) > conf.error_threshold_percent)
     then
      logger.err("open circuit")

      self.shm:set(key_for(self.LAST_CHECK_TIMESTAMP, identifier), timestamp:get_utc_in_seconds())
      self.shm:set(key_for(self.CIRCUIT_OPEN, identifier), true)
    end
  end

  if ctr_type == CTR_SUCCESS and reset_on_success then
    logger.info("close circuit")

    self.shm:set(key_for(self.CIRCUIT_OPEN, identifier), false)
    for i = 0, self.number_of_windows - 1 do
      local key = key_for_with_bucket(self.TARGET_COUNTER, identifier, bucket - i)
      self.shm:set(key, 0)
    end
  end
  return err
end

-- report http status. Returns err if any
function checker:report_http_status(status, conf, reset_on_success)
  if status == ngx.HTTP_GATEWAY_TIMEOUT then
    return report_counter(self, CTR_FAILURE, conf, reset_on_success)
  else
    return report_counter(self, CTR_SUCCESS, conf, reset_on_success)
  end
end

-- return false if should not receive any more request
local function increase_concurrency_counter(self, status, conf)
  local key = key_for(self.TARGET_CONCURRENCY, conf.identifier)
  local total, err = self.shm:incr(key, status, 0)
  if err then
    return nil, err
  end

  logger.info("increase concurrency counter ", status, " total ", total)

  -- status > 0 means increasing concurrency counter
  if status > 0 and total > conf.max_concurrent then
    return false, nil
  end
  return true, nil
end

-- get ticket. Return ok, err
function checker:get_ticket(conf)
  return increase_concurrency_counter(self, PHASE_START, conf)
end

-- return ticket. Return err
function checker:return_ticket(conf)
  local _, err = increase_concurrency_counter(self, PHASE_END, conf)
  return err
end

local _M = {}

function _M:new(conf)
  local self = {
    window_size = (conf and conf.window_size) or WINDOW_SIZE,
    number_of_windows = (conf and conf.number_of_windows) or NUMBER_OF_WINDOWS
  }

  self.shm = ngx.shared[tostring(SHM_NAME)]
  assert(self.shm, ("no shm found by name '%s'"):format(SHM_NAME))

  -- decorate with methods and constants
  for k, v in pairs(checker) do
    self[k] = v
  end
  -- prepare shm keys
  self.TARGET_COUNTER = "status_counter"
  self.CIRCUIT_OPEN = "circuit_open"
  self.TARGET_CONCURRENCY = "concurrency_counter"
  self.LAST_CHECK_TIMESTAMP = "last_check_timestamp"

  return self
end

_M.SHM_NAME = SHM_NAME
_M.CTR_SUCCESS = CTR_SUCCESS
_M.CTR_FAILURE = CTR_FAILURE

return _M
