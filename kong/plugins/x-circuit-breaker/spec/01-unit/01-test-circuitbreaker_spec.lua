local match = require("luassert.match")

local kong_log_mock = require("kong.spec.utils.kong-log-mock")
package.loaded["kong.plugins.commons.log"] = kong_log_mock

local noop = require("kong.spec.utils.noop")
local mock_utils = require("kong.spec.utils.mock")

local WINDOW_SIZE = 5 * 1000 --milliseconds

local shared_mock = {}
local shm_dict_mock = {}
_G["ngx"] =
  setmetatable(
  {
    shared = shared_mock
  },
  {
    __index = ngx
  }
)

_G["kong"] = {
  request = {
    get_header = spy.new(
      function()
      end
    )
  }
}

local timestamp_mock = {}

package.loaded["kong.tools.timestamp"] = timestamp_mock

local CircuitBreaker = require "kong.plugins.x-circuit-breaker.circuitbreaker"

describe(
  "Circuit Breaker",
  function()
    local circuit_breaker
    local timestamp_in_bucket_5 = math.floor(WINDOW_SIZE * 5.9)
    before_each(
      function()
        shm_dict_mock = {
          set = spy.new(noop),
          incr = spy.new(noop),
          get = spy.new(
            function()
              return 0
            end
          )
        }
        shared_mock[CircuitBreaker.SHM_NAME] = shm_dict_mock

        mock_utils.clear_table(timestamp_mock)

        circuit_breaker = CircuitBreaker:new({window_size = WINDOW_SIZE, number_of_windows = 2})
      end
    )

    it(
      "get_ticket should increase counter and return true if not exceed max_concurrent",
      function()
        shm_dict_mock.incr =
          spy.new(
          function()
            return 5
          end
        )
        local conf = {identifier = "my_service", max_concurrent = 5}

        local ok, err = circuit_breaker:get_ticket(conf)

        assert.spy(shm_dict_mock.incr).was.called_with(
          match.is_not_nil(),
          circuit_breaker.TARGET_CONCURRENCY .. ":" .. conf.identifier,
          1,
          0
        )
        assert.are.equal(true, ok)
        assert.is_nil(err)
      end
    )

    it(
      "get_ticket should increase counter and return false if exceed max_concurrent",
      function()
        shm_dict_mock.incr =
          spy.new(
          function()
            return 6
          end
        )
        local conf = {identifier = "my_service", max_concurrent = 5}

        local ok, err = circuit_breaker:get_ticket(conf)

        assert.spy(shm_dict_mock.incr).was.called_with(
          match.is_not_nil(),
          "concurrency_counter:" .. conf.identifier,
          1,
          0
        )
        assert.are.equal(false, ok)
        assert.is_nil(err)
      end
    )

    it(
      "get_ticket should return err if can't increase counter",
      function()
        shm_dict_mock.incr =
          spy.new(
          function()
            return nil, "can't increase counter"
          end
        )
        local conf = {identifier = "my_service", max_concurrent = 5}

        local _, err = circuit_breaker:get_ticket(conf)

        assert.is_not_nil(err)
      end
    )

    it(
      "return_ticket should decrease counter",
      function()
        local conf = {identifier = "my_service"}

        local err = circuit_breaker:return_ticket(conf)

        assert.spy(shm_dict_mock.incr).was.called_with(
          match.is_not_nil(),
          "concurrency_counter:" .. conf.identifier,
          -1,
          0
        )
        assert.is_nil(err)
      end
    )

    it(
      "return_ticket should return err if can't decrease counter",
      function()
        shm_dict_mock.incr =
          spy.new(
          function()
            return 0, "can't incr"
          end
        )
        local conf = {identifier = "my_service"}

        local err = circuit_breaker:return_ticket(conf)

        assert.is_not_nil(err)
      end
    )

    it(
      "report_http_status should increase success counter and close circuit if status code is not 504",
      function()
        timestamp_mock.get_utc =
          spy.new(
          function()
            return timestamp_in_bucket_5
          end
        )
        local bucket_five = 5
        local conf = {identifier = "my_service"}

        local err = circuit_breaker:report_http_status(201, conf, true)

        assert.is_nil(err)
        assert.spy(shm_dict_mock.incr).was.called_with(
          match.is_not_nil(),
          circuit_breaker.TARGET_COUNTER .. ":" .. conf.identifier .. ":" .. bucket_five,
          CircuitBreaker.CTR_SUCCESS,
          0
        )

        -- reset circuit
        assert.spy(shm_dict_mock.set).was.called_with(
          match.is_not_nil(),
          circuit_breaker.CIRCUIT_OPEN .. ":" .. conf.identifier,
          false
        )
        assert.spy(shm_dict_mock.set).was.called_with(
          match.is_not_nil(),
          circuit_breaker.TARGET_COUNTER .. ":" .. conf.identifier .. ":" .. bucket_five,
          0
        )
        assert.spy(shm_dict_mock.set).was.called_with(
          match.is_not_nil(),
          circuit_breaker.TARGET_COUNTER .. ":" .. conf.identifier .. ":" .. bucket_five - 1,
          0
        )
      end
    )

    it(
      "report_http_status should not close circuit if falsy reset_on_success is passed in",
      function()
        timestamp_mock.get_utc =
          spy.new(
          function()
            return timestamp_in_bucket_5
          end
        )
        local conf = {identifier = "my_service"}

        local err = circuit_breaker:report_http_status(201, conf, false)

        assert.is_nil(err)

        assert.spy(shm_dict_mock.set).was_not.called()
      end
    )

    it(
      "report_http_status should increase failure counter if status code is 504",
      function()
        timestamp_mock.get_utc =
          spy.new(
          function()
            return timestamp_in_bucket_5
          end
        )
        local bucket_five = 5

        shm_dict_mock.incr =
          spy.new(
          function()
            return 0
          end
        )
        local conf = {identifier = "my_service", request_volume_threshold = 4}

        local err = circuit_breaker:report_http_status(504, conf)

        assert.is_nil(err)
        assert.spy(shm_dict_mock.incr).was.called_with(
          match.is_not_nil(),
          circuit_breaker.TARGET_COUNTER .. ":" .. conf.identifier .. ":" .. bucket_five,
          CircuitBreaker.CTR_FAILURE,
          0
        )
      end
    )

    it(
      "report_http_status should not open circuit if total request is below request_volume_threshold",
      function()
        timestamp_mock.get_utc =
          spy.new(
          function()
            return timestamp_in_bucket_5
          end
        )
        local conf = {identifier = "my_service", request_volume_threshold = 3}
        shm_dict_mock.incr =
          spy.new(
          function()
            return 0x00020000
          end
        )

        local err = circuit_breaker:report_http_status(504, conf)

        assert.is_nil(err)
        assert.spy(shm_dict_mock.set).was_not.called_with(match.is_not_nil(), circuit_breaker.CIRCUIT_OPEN, true)
      end
    )

    it(
      "report_http_status should not open circuit if error rate doesn't exceed threshold",
      function()
        timestamp_mock.get_utc =
          spy.new(
          function()
            return timestamp_in_bucket_5
          end
        )

        shm_dict_mock.incr =
          spy.new(
          function()
            return 0x00040004
          end
        )

        shm_dict_mock.get =
          spy.new(
          function()
            return 0x00040004
          end
        )
        local conf = {
          identifier = "my_service",
          request_volume_threshold = 2,
          error_threshold_percent = 50
        }

        local err = circuit_breaker:report_http_status(504, conf)

        assert.is_nil(err)
        assert.spy(shm_dict_mock.set).was_not.called()
      end
    )

    it(
      "report_http_status should not open circuit if circuit breaker criteria is met, but last request is success",
      function()
        timestamp_mock.get_utc =
          spy.new(
          function()
            return timestamp_in_bucket_5
          end
        )

        local conf = {
          identifier = "my_service",
          request_volume_threshold = 10,
          error_threshold_percent = 50,
          sleep_window = 2000
        }
        local key_for_bucket_four = circuit_breaker.TARGET_COUNTER .. ":" .. conf.identifier .. ":4"
        local key_for_bucket_five = circuit_breaker.TARGET_COUNTER .. ":" .. conf.identifier .. ":5"

        shm_dict_mock.get =
          spy.new(
          function(_, key)
            if key == key_for_bucket_four then
              return 0x00050003
            end
          end
        )
        shm_dict_mock.incr =
          spy.new(
          function()
            return 0x00010001
          end
        )
        local err = circuit_breaker:report_http_status(200, conf)

        assert.is_nil(err)
        assert.spy(shm_dict_mock.incr).was.called_with(
          match.is_not_nil(),
          key_for_bucket_five,
          CircuitBreaker.CTR_SUCCESS,
          0
        )
        assert.spy(shm_dict_mock.set).was_not_called_with(
          match.is_not_nil(),
          circuit_breaker.CIRCUIT_OPEN .. ":" .. conf.identifier,
          true
        )
        assert.spy(shm_dict_mock.set).was_not_called_with(
          match.is_not_nil(),
          circuit_breaker.LAST_CHECK_TIMESTAMP .. ":" .. conf.identifier,
          match.is_not_nil()
        )
      end
    )

    it(
      "report_http_status should open circuit and update last check timestamp if error rate exceeds threshold",
      function()
        timestamp_mock.get_utc =
          spy.new(
          function()
            return timestamp_in_bucket_5
          end
        )

        local conf = {
          identifier = "my_service",
          request_volume_threshold = 10,
          error_threshold_percent = 50,
          sleep_window = 2000
        }
        local key_for_bucket_four = circuit_breaker.TARGET_COUNTER .. ":" .. conf.identifier .. ":4"

        shm_dict_mock.get =
          spy.new(
          function(_, key)
            if key == key_for_bucket_four then
              return 0x00050004
            end
          end
        )
        shm_dict_mock.incr =
          spy.new(
          function()
            return 0x00040004
          end
        )
        local err = circuit_breaker:report_http_status(504, conf)

        assert.is_nil(err)

        assert.spy(shm_dict_mock.get).was.called_with(match.is_not_nil(), key_for_bucket_four)

        assert.spy(shm_dict_mock.set).was.called_with(
          match.is_not_nil(),
          circuit_breaker.CIRCUIT_OPEN .. ":" .. conf.identifier,
          true
        )
        assert.spy(shm_dict_mock.set).was.called_with(
          match.is_not_nil(),
          circuit_breaker.LAST_CHECK_TIMESTAMP .. ":" .. conf.identifier,
          math.floor(timestamp_in_bucket_5 / 1000) -- seconds of current utc
        )
      end
    )

    it(
      "report_http_status should sum over all windows, reset all counters when calculating error rate and closing circuit",
      function()
        local window_size = 3 * 1000
        local time_stamp = window_size * 5.8
        timestamp_mock.get_utc =
          spy.new(
          function()
            return time_stamp
          end
        )

        local conf = {
          identifier = "my_service",
          request_volume_threshold = 11,
          error_threshold_percent = 20,
          sleep_window = 2000
        }
        local key_for_bucket_six = circuit_breaker.TARGET_COUNTER .. ":" .. conf.identifier .. ":6"
        local key_for_bucket_five = circuit_breaker.TARGET_COUNTER .. ":" .. conf.identifier .. ":5"
        local key_for_bucket_four = circuit_breaker.TARGET_COUNTER .. ":" .. conf.identifier .. ":4"
        local key_for_bucket_three = circuit_breaker.TARGET_COUNTER .. ":" .. conf.identifier .. ":3"
        local key_for_bucket_two = circuit_breaker.TARGET_COUNTER .. ":" .. conf.identifier .. ":2"

        shm_dict_mock.get =
          spy.new(
          function(_, key)
            if key == key_for_bucket_four then
              return 0x00010004
            end
            if key == key_for_bucket_three then
              return 0x00020004
            end
            if key == key_for_bucket_six or key == key_for_bucket_two then
              return 0x00021111
            end
          end
        )
        shm_dict_mock.incr =
          spy.new(
          function()
            return 0x00010004
          end
        )
        local new_circuit_breaker = CircuitBreaker:new({number_of_windows = 3, window_size = window_size})
        local err = new_circuit_breaker:report_http_status(504, conf)

        assert.is_nil(err)

        assert.spy(shm_dict_mock.set).was.called_with(
          match.is_not_nil(),
          circuit_breaker.CIRCUIT_OPEN .. ":" .. conf.identifier,
          true
        )
        assert.spy(shm_dict_mock.set).was.called_with(
          match.is_not_nil(),
          circuit_breaker.LAST_CHECK_TIMESTAMP .. ":" .. conf.identifier,
          math.floor(time_stamp / 1000) -- seconds of current utc
        )

        -- Reset circuit
        assert.spy(shm_dict_mock.set).was_not.called_with(match.is_not_nil(), key_for_bucket_six, 0)
        assert.spy(shm_dict_mock.set).was_not.called_with(match.is_not_nil(), key_for_bucket_five, 0)
        assert.spy(shm_dict_mock.set).was_not.called_with(match.is_not_nil(), key_for_bucket_four, 0)
        assert.spy(shm_dict_mock.set).was_not.called_with(match.is_not_nil(), key_for_bucket_three, 0)
        assert.spy(shm_dict_mock.set).was_not.called_with(match.is_not_nil(), key_for_bucket_two, 0)

        local err = new_circuit_breaker:report_http_status(201, conf, true)

        assert.is_nil(err)

        -- reset circuit
        assert.spy(shm_dict_mock.set).was.called_with(
          match.is_not_nil(),
          circuit_breaker.CIRCUIT_OPEN .. ":" .. conf.identifier,
          false
        )
        assert.spy(shm_dict_mock.set).was_not.called_with(match.is_not_nil(), key_for_bucket_six, 0)
        assert.spy(shm_dict_mock.set).was.called_with(match.is_not_nil(), key_for_bucket_five, 0)
        assert.spy(shm_dict_mock.set).was.called_with(match.is_not_nil(), key_for_bucket_four, 0)
        assert.spy(shm_dict_mock.set).was.called_with(match.is_not_nil(), key_for_bucket_three, 0)
        assert.spy(shm_dict_mock.set).was_not.called_with(match.is_not_nil(), key_for_bucket_two, 0)
      end
    )

    it(
      "report_http_status should return err if can't increase counter",
      function()
        timestamp_mock.get_utc =
          spy.new(
          function()
            return timestamp_in_bucket_5
          end
        )
        shm_dict_mock.incr =
          spy.new(
          function()
            return nil, "can't increase counter"
          end
        )
        local conf = {identifier = "my_service"}

        local err = circuit_breaker:report_http_status(504, conf)

        assert.is_not_nil(err)
      end
    )

    it(
      "get_status should return circuit status",
      function()
        local current_seconds = 35
        local prev_seconds = 32
        timestamp_mock.get_utc =
          spy.new(
          function()
            return current_seconds * 1000 + 123
          end
        )

        local conf = {
          identifier = "my_service",
          request_volume_threshold = 4,
          error_threshold_percent = 50,
          sleep_window_in_seconds = 2
        }

        shm_dict_mock.get =
          spy.new(
          function(_, key)
            if key == circuit_breaker.CIRCUIT_OPEN .. ":" .. conf.identifier then
              return true
            end
            if key == circuit_breaker.LAST_CHECK_TIMESTAMP .. ":" .. conf.identifier then
              return prev_seconds
            end
          end
        )
        shm_dict_mock.incr =
          spy.new(
          function()
            return current_seconds
          end
        )
        local status, err = circuit_breaker:get_status(conf)

        -- time has been over sleep window
        assert.is_nil(err)
        assert.are.equal("outdated", status)

        -- not over sleep window
        conf.sleep_window_in_seconds = 3
        status, err = circuit_breaker:get_status(conf)
        assert.is_nil(err)
        assert.are.equal("open", status)

        -- state was updated by other coroutine
        conf.sleep_window_in_seconds = 2
        shm_dict_mock.incr =
          spy.new(
          function()
            return current_seconds + 1
          end
        )
        status, err = circuit_breaker:get_status(conf)
        assert.is_nil(err)
        assert.are.equal("open", status)
      end
    )

    it(
      "get_status should return closed if circuit is closed",
      function()
        local conf = {
          identifier = "my_service",
          request_volume_threshold = 4,
          error_threshold_percent = 50,
          sleep_window_in_seconds = 2
        }

        shm_dict_mock.get =
          spy.new(
          function(_, key)
            return false
          end
        )
        local status, err = circuit_breaker:get_status(conf)

        assert.are_equal("closed", status)
        assert.is_nil(err)
      end
    )

    it(
      "get_status should return error if can't get counter",
      function()
        timestamp_mock.get_utc =
          spy.new(
          function()
            return 35111 --Should go to bucket 3
          end
        )

        shm_dict_mock.get =
          spy.new(
          function()
            return nil, "can't get counter"
          end
        )
        local conf = {
          identifier = "my_service",
          request_volume_threshold = 4,
          error_threshold_percent = 50
        }

        local _, err = circuit_breaker:get_status(conf)

        assert.is_not_nil(err)
      end
    )
  end
)
