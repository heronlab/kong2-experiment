local match = require("luassert.match")

local kong_log_mock = require("kong.spec.utils.kong-log-mock")
local ngx_log_mock = require("kong.spec.utils.ngx-log-mock")
local response_mock = require("kong.spec.utils.responses-mock")
package.loaded["kong.plugins.commons.profiling"] = require("kong.spec.utils.profiling-mock")
local datadog_reporter_mock = {}
local noop = require("kong.spec.utils.noop")
local old_ngx = ngx
local test_service = {name = "echo"}
local test_route = {name = "root"}

_G["ngx"] =
  setmetatable(
  {
    log = ngx_log_mock
  },
  {__index = old_ngx}
)

_G["kong"] = {
  log = kong_log_mock,
  request = {
    get_header = spy.new(
      function()
      end
    )
  },
  router = {
    get_service = function()
      return test_service
    end,
    get_route = function()
      return test_route
    end
  }
}

local health_check_mock

package.loaded["kong.plugins.x-circuit-breaker.circuitbreaker"] = {
  new = function()
    return health_check_mock
  end
}
package.loaded["kong.plugins.commons.analytics"] = {
  get_datadog_reporter = function()
    return datadog_reporter_mock
  end
}

local CircuitBreakerHandler = require "kong.plugins.x-circuit-breaker.handler"

describe(
  "Circuit Breaker handler",
  function()
    local handler

    before_each(
      function()
        health_check_mock = {}
        stub(datadog_reporter_mock, "tag_request")
        handler = CircuitBreakerHandler()
        response_mock.exit = spy.new(noop)
        ngx.ctx = {}
        kong.ctx = {
          shared = {}
        }
        kong.response = response_mock
        ngx.status = 200
      end
    )

    it (
      "should generate the identifier follows pattern SERVICE (as the plugin is applied at service level)",
      function()
        local conf = {service_id = "test-service-id"}
        local want = test_service.name
        handler:generateIdentifier(conf)
        assert.are.same(want, conf.identifier)
      end
    )

    it (
      "should generate the identifier follows pattern SERVICE_ROUTE (as the plugin is applied at route level)",
      function()
        local conf = {route_id = "test-route-id"}
        local want = test_service.name .. "_" .. test_route.name
        handler:generateIdentifier(conf)
        assert.are.same(want, conf.identifier)
      end
    )

    it(
      "should get ticket at the beginning of request",
      function()
        health_check_mock.get_ticket = spy.new(noop)
        local want = {identifier = test_service.name}

        handler:access({})

        assert.spy(health_check_mock.get_ticket).was.called_with(match.is_not_nil(), want)
      end
    )

    it(
      "should return service unavailable code if can't get ticket",
      function()
        health_check_mock.get_ticket =
          spy.new(
          function()
            return false, nil
          end
        )

        handler:access({})

        assert.spy(response_mock.exit).was.called_with(
          CircuitBreakerHandler.ERRORS.OverMaximumConcurrency.Code,
          CircuitBreakerHandler.ERRORS.OverMaximumConcurrency.Msg
        )
        assert.spy(datadog_reporter_mock.tag_request).was.called_with({circuit_status = "open"})
      end
    )

    it(
      "should return service unavailable if circuit is open",
      function()
        health_check_mock.get_ticket = function()
          return true
        end
        health_check_mock.get_status =
          spy.new(
          function()
            return "open"
          end
        )
        local want = {identifier = test_service.name}
        kong.ctx.plugin = {}
        local exist_tags = {
          existing_tag = "value"
        }
        kong.ctx.shared.x_datadog = {
          tags = exist_tags
        }
        handler:access({})

        assert.spy(health_check_mock.get_status).was.called_with(match.is_not_nil(), want)

        assert.spy(response_mock.exit).was.called_with(
          CircuitBreakerHandler.ERRORS.CircuitOpen.Code,
          CircuitBreakerHandler.ERRORS.CircuitOpen.Msg
        )
        assert.spy(datadog_reporter_mock.tag_request).was.called_with({circuit_status = "open"})
        assert.is_nil(kong.ctx.plugin.checking_upstream)
      end
    )

    it(
      "should not return service unavailable if circuit is closed",
      function()
        health_check_mock.get_ticket = function()
          return true
        end
        health_check_mock.get_status =
          spy.new(
          function()
            return "closed"
          end
        )
        kong.ctx.plugin = {}

        handler:access({})

        assert.spy(response_mock.exit).was_not.called()
        assert.is_nil(kong.ctx.plugin.checking_upstream)
        assert.spy(datadog_reporter_mock.tag_request).was_not_called()
      end
    )

    it(
      "should not return service unavailable if circuit is outdated",
      function()
        health_check_mock.get_ticket = function()
          return true
        end
        health_check_mock.get_status =
          spy.new(
          function()
            return "outdated"
          end
        )
        kong.ctx.plugin = {}

        handler:access({})

        assert.spy(response_mock.exit).was_not.called()
        assert.are_equal(true, kong.ctx.plugin.checking_upstream)
        assert.spy(datadog_reporter_mock.tag_request).was_not_called()
      end
    )

    it(
      "should return ticket at log phase",
      function()
        health_check_mock.return_ticket = spy.new(noop)
        ngx.ctx.KONG_PROXIED = false
        local want = {identifier = test_service.name}

        handler:log({})

        assert.spy(health_check_mock.return_ticket).was.called_with(match.is_not_nil(), want)
      end
    )

    it(
      "should report http status to circuit breaker if request was proxied by Kong",
      function()
        health_check_mock.return_ticket = spy.new(noop)
        health_check_mock.report_http_status = spy.new(noop)
        ngx.ctx.KONG_PROXIED = true
        ngx.status = 504
        kong.ctx = {
          plugin = {
            checking_upstream = true
          }
        }

        local want = {identifier = test_service.name}

        handler:log({})

        assert.spy(health_check_mock.report_http_status).was.called_with(
          match.is_not_nil(),
          ngx.status,
          want,
          kong.ctx.plugin.checking_upstream
        )
      end
    )

    it(
      "should not report http status if request was not proxied by Kong",
      function()
        health_check_mock.return_ticket = spy.new(noop)
        health_check_mock.report_http_status = spy.new(noop)
        ngx.ctx.KONG_PROXIED = false
        ngx.status = 504

        handler:log({})

        assert.spy(health_check_mock.report_http_status).was_not.called()
      end
    )
  end
)
