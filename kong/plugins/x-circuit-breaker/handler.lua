-- Imports
local profiling = require("kong.plugins.commons.profiling")
local BasePlugin = require("kong.plugins.base_plugin")
local CircuitBreaker = require("kong.plugins.x-circuit-breaker.circuitbreaker")
local logger = require("kong.plugins.commons.log")
local analytics = require("kong.plugins.commons.analytics")
local pl_table = require("pl.tablex")

-- Constants
local trace_obj = profiling.trace_obj

local CircuitBreakerHandler = BasePlugin:extend()
CircuitBreakerHandler.PRIORITY = 2100
CircuitBreakerHandler.VERSION = "0.1.0"

local ERRORS = {
  OverMaximumConcurrency = {
    Code = ngx.HTTP_SERVICE_UNAVAILABLE,
    Msg = {message = "Over Maximum Concurrency"}
  },
  CircuitOpen = {
    Code = ngx.HTTP_SERVICE_UNAVAILABLE,
    Msg = {message = "Circuit Is Open"}
  }
}

CircuitBreakerHandler.ERRORS = ERRORS

local exit = function(code, message)
  local dd_reporter = analytics.get_datadog_reporter()
  dd_reporter.tag_request({circuit_status = "open"})
  return kong.response.exit(code, message)
end

function CircuitBreakerHandler:new()
  CircuitBreakerHandler.super.new(self, "x-circuit-breaker")
  self.circuit_breaker = CircuitBreaker:new()
end

function CircuitBreakerHandler:access(conf)
  CircuitBreakerHandler.super.access(self)
  conf = self:generateIdentifier(pl_table.copy(conf))
  local ok, err = self.circuit_breaker:get_ticket(conf)
  if err then
    logger.err("can't get ticket ", err)
  else
    if not ok then
      return exit(ERRORS.OverMaximumConcurrency.Code, ERRORS.OverMaximumConcurrency.Msg)
    end
  end

  local circuit_status, err = self.circuit_breaker:get_status(conf)
  if err then
    logger.err("can't get target status ", err)
  end

  logger.info("circuit_status ", circuit_status)

  if circuit_status == "open" then
    return exit(ERRORS.CircuitOpen.Code, ERRORS.CircuitOpen.Msg)
  end

  if circuit_status == "outdated" then
    kong.ctx.plugin.checking_upstream = true
  end
end

function CircuitBreakerHandler:log(conf)
  CircuitBreakerHandler.super.log(self)
  -- make a new copy of conf to avoid race condition on concurrency requests
  conf = self:generateIdentifier(pl_table.copy(conf))
  local err = self.circuit_breaker:return_ticket(conf)
  if err then
    logger.err("can't return ticket ", err)
  end

  -- If response was produced by an upstream (ie, not by a Kong plugin)
  if ngx.ctx.KONG_PROXIED == true then
    -- Report HTTP status for health checks
    local status = ngx.status
    local reset_on_success = kong.ctx.plugin.checking_upstream
    local err = self.circuit_breaker:report_http_status(status, conf, reset_on_success)
    if err then
      logger.err("error while reporting http status ", err)
    end
  end
end

function CircuitBreakerHandler:generateIdentifier(conf)
  local service = kong.router.get_service()
  local identifier = service and (service.name or service.id)
  -- route level applied
  if conf.route_id ~= nil then
    local route = kong.router.get_route()
    identifier = identifier .. "_" .. (route and (route.name or route.id))
  end
  conf.identifier = identifier
  return conf
end

trace_obj("x-circuit-breaker", CircuitBreakerHandler, {"log", "access"})
return CircuitBreakerHandler
