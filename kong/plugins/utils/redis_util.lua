local redis = require("resty.redis")
local M = {}

local function is_present(str)
  return str and str ~= "" and str ~= ngx.null
end

-- make new redis connection
-- require conf{redis_timeout, redis_host, redis_port, redis_database, redis_password}
M.new_conn = function(conf)
  local red = redis:new()
  local sock_opts = {}
  local ok, err, times

  red:set_timeout(conf.redis_timeout)
  -- setup pool name
  sock_opts.pool = conf.redis_database and
    conf.redis_host .. ":" .. conf.redis_port .. ":" .. conf.redis_database
  ok, err = red:connect(conf.redis_host, conf.redis_port, sock_opts)
  if not ok then
    return nil, err
  end

  times, err = red:get_reused_times()
  if err then
    return nil, err
  end

  if times == 0 then
    -- init on the first time
    if is_present(conf.redis_password) then
      ok, err = red:auth(conf.redis_password)
      if not ok then
        return nil, err
      end
    end

    if conf.redis_database ~= 0 then
      ok, err = red:select(conf.redis_database)
      if not ok then
        return nil, err
      end
    end
  end
  return red, nil
end

return M
