local response = require "kong.plugins.utils.response"

describe(
  "response",
  function(...)
    describe(
      "newMessage",
      function(...)
        it(
          "should use message if provided",
          function()
            local msg = "a message"

            local result1 = response.newMessage(nil, msg)
            assert.are.same({message = msg}, result1)

            local result2 = response.newMessage(ngx.HTTP_INTERNAL_SERVER_ERROR, msg)
            assert.are.same({message = msg}, result2)
          end
        )

        it(
          "should handle 500 error",
          function()
            local result = response.newMessage(ngx.HTTP_INTERNAL_SERVER_ERROR)
            assert.are.same({message = "Internal Server Error"}, result)
          end
        )

        it(
          "should handle 401 error",
          function()
            local result = response.newMessage(ngx.HTTP_UNAUTHORIZED)
            assert.are.same({message = "Unauthorized"}, result)
          end
        )

        it(
          "should handle 403 error",
          function()
            local result = response.newMessage(ngx.HTTP_FORBIDDEN)
            assert.are.same({message = "Forbidden"}, result)
          end
        )

        it(
          "should handle unknown status code",
          function()
            local result = response.newMessage(499)
            assert.are.same({message = "Unknown Error"}, result)
          end
        )
      end
    )
  end
)
