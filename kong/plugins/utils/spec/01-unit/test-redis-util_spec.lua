
local mock_redis
package.loaded["resty.redis"] = {
  new = function(...)
    return mock_redis
  end
}

local conf = {
  fail_away = false,
  redis_host = "redis",
  redis_port = 6379,
  redis_database = 0,
  redis_password = "",
}

describe(
  "redis_util",
  function()
    before_each(function()
      mock_redis = {
        get = spy.new(function (o, key) end),
        set = spy.new(function (o, key, value) end),
        del = spy.new(function (o, ...) end),
        set_timeout = spy.new(function(o, ...) end),
        connect = spy.new(function(o, ...)
          return true, nil
        end),
        get_reused_times = spy.new(function(o, ...)
          return 0, nil
        end),
        auth = spy.new(function(o, ...) return true, nil end),
        select = spy.new(function(o, ...) end),
      }
    end)

    describe(
      "unittest",
      function()
        it("should get connection successfully", function()
          local redis_util = require "kong.plugins.utils.redis_util"
          local conn, err = redis_util.new_conn(conf)
          assert.not_nil(conn)
          assert.is_nil(err)
        end)

        it("should call auth when password set", function()
          conf.redis_password = "123"
          mock_redis.auth = spy.new(function(...)
            return true, nil
          end)
          local redis_util = require "kong.plugins.utils.redis_util"
          local conn, err = redis_util.new_conn(conf)
          assert.not_nil(conn)
          assert.is_nil(err)
          assert.spy(mock_redis.auth).was.called_with(mock_redis, "123")
        end)

        it("should raise error when authorizing failed", function()
          conf.redis_password = "123"
          mock_redis.auth = spy.new(function(...)
            return false, "failed to authorize"
          end)
          local redis_util = require "kong.plugins.utils.redis_util"
          local conn, err = redis_util.new_conn(conf)
          assert.not_nil(err)
          assert.is_nil(conn)
          assert.spy(mock_redis.auth).was.called_with(mock_redis, "123")
        end)

        it("should call select when database set", function()
          conf.redis_database = 7
          conf.redis_password = ""
          mock_redis.select = spy.new(function(...)
            return true, nil
          end)
          local redis_util = require "kong.plugins.utils.redis_util"
          local conn, err = redis_util.new_conn(conf)
          assert.not_nil(conn)
          assert.is_nil(err)
          assert.spy(mock_redis.select).was.called_with(mock_redis, 7)
        end)

        it("should raise error when selecting database failed", function()
          conf.redis_database = 7
          conf.redis_password = ""
          mock_redis.select = spy.new(function(...)
            return false, "failed to select database"
          end)
          local redis_util = require "kong.plugins.utils.redis_util"
          local conn, err = redis_util.new_conn(conf)
          assert.is_nil(conn)
          assert.not_nil(err)
          assert.spy(mock_redis.select).was.called_with(mock_redis, 7)
        end)
      end
    )
  end
)
