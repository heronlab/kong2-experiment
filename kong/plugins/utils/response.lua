local ERROR_MESSAGES = {
  [ngx.HTTP_UNAUTHORIZED] = "Unauthorized",
  [ngx.HTTP_INTERNAL_SERVER_ERROR] = "Internal Server Error",
  [ngx.HTTP_FORBIDDEN] = "Forbidden"
}

local function newMessage(statusCode, message)
  local msg = message or ERROR_MESSAGES[statusCode] or "Unknown Error"
  return {
    message = msg
  }
end

return {
  newMessage = newMessage
}
