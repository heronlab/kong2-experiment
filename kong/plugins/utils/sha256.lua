local sha256 = require "resty.sha256"
local str = require "resty.string"

local function hash(body)
  local digest = sha256:new()
  digest:update(body)
  return str.to_hex(digest:final())
end

return {
  hash = hash,
}
