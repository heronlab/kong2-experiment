-- Copyright (c) 2012-2017 Grab Taxi Holdings PTE LTD (GRAB), All Rights Reserved. NOTICE: All information contained herein
-- is, and remains the property of GRAB. The intellectual and technical concepts contained herein are confidential, proprietary
-- and controlled by GRAB and may be covered by patents, patents in process, and are protected by trade secret or copyright law.

-- You are strictly forbidden to copy, download, store (in any medium), transmit, disseminate, adapt or change this material
-- in any way unless prior written permission is obtained from GRAB. Access to the source code contained herein is hereby
-- forbidden to anyone except current GRAB employees or contractors with binding Confidentiality and Non-disclosure agreements
-- explicitly covering such access.

-- The copyright notice above does not evidence any actual or intended publication or disclosure of this source code,
-- which includes information that is confidential and/or proprietary, and is a trade secret, of GRAB.

-- ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE
-- CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF GRAB IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND
-- INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY
-- OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING
-- THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

local header_mediator = require "kong.plugins.x-mediation.entity.header"
local param_mediator = require "kong.plugins.x-mediation.entity.param"
local path_mediator = require "kong.plugins.x-mediation.entity.path"

local cjson = require "cjson"
local pcall = pcall
local function parse_json(body)
  if body then
    local status, res = pcall(cjson.decode, body)
    if status then
      return res
    end
  end
end

local function contain(t, v)
  for _, _v in pairs(t) do
    if _v == v then
      return true
    end
  end
  return false
end

local MediatorBuilder = {}

function MediatorBuilder:new(service)
  local setting = {service = service}
  self.__index = self
  return setmetatable(setting, self)
end

function MediatorBuilder:create_mediators(config, cycle)
  local mediators = {}
  for key, value in pairs(parse_json(config.mediators)) do
    if contain(config.mediator_index, key) then
      if cycle == "in" then
        local mediator
        if value.type == "param" then
          mediator = param_mediator()
        elseif value.type == "header" then
          mediator = header_mediator()
        elseif value.type == "path" then
          mediator = path_mediator()
        end
        if mediator ~= nil then
          mediator:new(self.service, config, value)
          mediators[key] = mediator
        end
      end
    end
  end
  return mediators
end

return MediatorBuilder
