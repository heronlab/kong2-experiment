-- Imports
local profiling = require("kong.plugins.commons.profiling")
local BasePlugin = require("kong.plugins.base_plugin")
local mediation_builder = require("kong.plugins.x-mediation.builder")
local response = require("kong.plugins.utils.response")

-- Constants
local trace_obj = profiling.trace_obj

local MediatorHandler = BasePlugin:extend()
MediatorHandler.PRIORITY = 995
MediatorHandler.VERSION = "0.1.2"

local function get_entity_id(config)
  local service
  if config.service_id ~= nil then
    service = config.service_id
  else
    service = config.route_id
  end
  return service
end

function MediatorHandler:new()
  MediatorHandler.super.new(self, "x-mediation")
end

function MediatorHandler:access(config)
  MediatorHandler.super.access(self)

  if kong.ctx.shared.is_fallback then
    return
  end

  local _builder = mediation_builder:new(get_entity_id(config))

  local mediators = _builder:create_mediators(config, "in")
  for _, mediator in pairs(mediators) do
    local err = mediator:rewrite()
    if err ~= nil then
      return kong.response.exit(ngx.HTTP_INTERNAL_SERVER_ERROR, response.newMessage(ngx.HTTP_INTERNAL_SERVER_ERROR, nil).message)
    end
    mediator:transform_header()
  end
end

function MediatorHandler:body_filter(config)
  MediatorHandler.super.body_filter(self)

  if kong.ctx.shared.is_fallback then
    return
  end

  local _builder = mediation_builder:new(get_entity_id(config))
  local mediators = _builder:create_mediators(config, "out")
  for _, mediator in pairs(mediators) do
    mediator:transform_body()
  end
end

trace_obj("x-mediator", MediatorHandler, {"body_filter", "access"})
return MediatorHandler
