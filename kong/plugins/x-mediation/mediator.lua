-- Copyright (c) 2012-2017 Grab Taxi Holdings PTE LTD (GRAB), All Rights Reserved. NOTICE: All information contained herein
-- is, and remains the property of GRAB. The intellectual and technical concepts contained herein are confidential, proprietary
-- and controlled by GRAB and may be covered by patents, patents in process, and are protected by trade secret or copyright law.

-- You are strictly forbidden to copy, download, store (in any medium), transmit, disseminate, adapt or change this material
-- in any way unless prior written permission is obtained from GRAB. Access to the source code contained herein is hereby
-- forbidden to anyone except current GRAB employees or contractors with binding Confidentiality and Non-disclosure agreements
-- explicitly covering such access.

-- The copyright notice above does not evidence any actual or intended publication or disclosure of this source code,
-- which includes information that is confidential and/or proprietary, and is a trade secret, of GRAB.

-- ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE
-- CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF GRAB IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND
-- INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY
-- OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING
-- THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

local Object = require "kong.plugins.commons.object.classic"
local Mediator = Object:extend()
local logger = require("kong.plugins.commons.log")

local function log(variable, session_user)
  local sessions = "["
  for k, v in pairs(session_user) do
    sessions = sessions .. k .. ", "
  end
  sessions = sessions .. "]"
  sessions = sessions.gsub(sessions, ", ]", "]")
  logger.err("'", variable, "' not found in $ctx.session.* support: ", sessions)
  return
end

function Mediator:new(service, parent_config, data)
  self._service = service
  self._data = data
  self._config = parent_config
end

function Mediator:rewrite()
end

function Mediator:transform_header()
end

function Mediator:transform_body()
end

function Mediator:context_value(v)
  local var = string.gsub(v, "({{)(%$ctx.session.%w+)(}})", "%2")
  if var == nil then
    return v, nil
  end
  for variable in string.gmatch(var, "%$ctx.session.%w+") do
    if kong.ctx.shared.session_user ~= nil then
      if kong.ctx.shared.session_user[variable] ~= nil then
        var = string.gsub(var, variable, tostring(kong.ctx.shared.session_user[variable]))
      else
        --log ctx.session support
        log(variable, kong.ctx.shared.session_user)

        --return exit 500 error
        return nil, "invalid session_user"
      end
    else
      logger.err("kong.ctx.shared.session_user is nil")

      --return exit 500 error
      return nil, "kong.ctx.shared.session_user is nil"
    end
  end
  return var, nil
end

return Mediator
