-- Copyright (c) 2012-2017 Grab Taxi Holdings PTE LTD (GRAB), All Rights Reserved. NOTICE: All information contained herein
-- is, and remains the property of GRAB. The intellectual and technical concepts contained herein are confidential, proprietary
-- and controlled by GRAB and may be covered by patents, patents in process, and are protected by trade secret or copyright law.

-- You are strictly forbidden to copy, download, store (in any medium), transmit, disseminate, adapt or change this material
-- in any way unless prior written permission is obtained from GRAB. Access to the source code contained herein is hereby
-- forbidden to anyone except current GRAB employees or contractors with binding Confidentiality and Non-disclosure agreements
-- explicitly covering such access.

-- The copyright notice above does not evidence any actual or intended publication or disclosure of this source code,
-- which includes information that is confidential and/or proprietary, and is a trade secret, of GRAB.

-- ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE
-- CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF GRAB IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND
-- INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY
-- OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING
-- THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

local mediator = require "kong.plugins.x-mediation.mediator"
local Params = mediator:extend()

local kong_params = {
  set_params = function(...)
    kong.service.request.set_query(...)
  end,
  get_params = function()
    return kong.request.get_query()
  end
}
local params_mediator = {
  add = function(params, k, v)
    if type(params[k]) == "table" then
      table.insert(params[k], v)
    else
      params[k] = v
    end
    return params
  end,
  set = function(params, k, v)
    params[k] = v
    return params
  end,
  delete = function(params, k, v)
    params[k] = nil
    return params
  end,
  rename = function(params, k, v)
    local old_value = params[k]
    params[v] = old_value
    params[k] = nil
    return params
  end,
  setnx = function(params, k, v)
    local old_value = params[k]
    if old_value ~= nil and #old_value > 0 then
      params[k] = v
    end
    return params
  end
}

function Params:rewrite()
  local config = self._data
  local action = params_mediator[config.action]
  local params = kong_params.get_params()
  for k, v in pairs(config.data) do
    local val, err = self:context_value(v)
    if err == nil then
      params = action(params, k, val)
    else
      return err
    end

  end
  kong_params.set_params(params)
end

return Params
