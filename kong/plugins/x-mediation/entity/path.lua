-- Copyright (c) 2012-2017 Grab Taxi Holdings PTE LTD (GRAB), All Rights Reserved. NOTICE: All information contained herein
-- is, and remains the property of GRAB. The intellectual and technical concepts contained herein are confidential, proprietary
-- and controlled by GRAB and may be covered by patents, patents in process, and are protected by trade secret or copyright law.

-- You are strictly forbidden to copy, download, store (in any medium), transmit, disseminate, adapt or change this material
-- in any way unless prior written permission is obtained from GRAB. Access to the source code contained herein is hereby
-- forbidden to anyone except current GRAB employees or contractors with binding Confidentiality and Non-disclosure agreements
-- explicitly covering such access.

-- The copyright notice above does not evidence any actual or intended publication or disclosure of this source code,
-- which includes information that is confidential and/or proprietary, and is a trade secret, of GRAB.

-- ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE
-- CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF GRAB IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND
-- INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY
-- OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING
-- THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

local mediator = require "kong.plugins.x-mediation.mediator"
local Path = mediator:extend()

local function escape_pattern(pattern)
  return string.gsub(pattern, '([%(%)%.%%%+%-%*%?%[%]%^%$])', '%%%1')
end

local path_action = {
  replace = function(pattern, replace)
    ngx.var.upstream_uri = string.gsub(kong.request.get_path(), pattern, replace)
  end
}

function Path:rewrite()
  local action = path_action[self._data.action]
  for pattern, v in pairs(self._data.data) do
    local basePath = "$basePath"
    if pattern == basePath then
      pattern = '^' .. escape_pattern(self._config.base_path)
    else
      pattern = escape_pattern(pattern)
    end
    local val, err = self:context_value(v)
    if err == nil then
      action(pattern, val)
    else
      return err
    end
  end
end

return Path
