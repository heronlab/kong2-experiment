-- Copyright (c) 2012-2017 Grab Taxi Holdings PTE LTD (GRAB), All Rights Reserved. NOTICE: All information contained herein
-- is, and remains the property of GRAB. The intellectual and technical concepts contained herein are confidential, proprietary
-- and controlled by GRAB and may be covered by patents, patents in process, and are protected by trade secret or copyright law.

-- You are strictly forbidden to copy, download, store (in any medium), transmit, disseminate, adapt or change this material
-- in any way unless prior written permission is obtained from GRAB. Access to the source code contained herein is hereby
-- forbidden to anyone except current GRAB employees or contractors with binding Confidentiality and Non-disclosure agreements
-- explicitly covering such access.

-- The copyright notice above does not evidence any actual or intended publication or disclosure of this source code,
-- which includes information that is confidential and/or proprietary, and is a trade secret, of GRAB.

-- ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE
-- CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF GRAB IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND
-- INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY
-- OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING
-- THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

local mediator = require "kong.plugins.x-mediation.mediator"
local Header = mediator:extend()

local kong_header = {
  get_header = function(...)
    return kong.request.get_header(...)
  end,
  add_header = function(...)
    kong.service.request.add_header(...)
  end,
  set_header = function(...)
    kong.service.request.set_header(...)
  end,
  clear_header = function(...)
    kong.service.request.clear_header(...)
  end
}

local header_mediator = {
  add = function(k, v)
    kong_header.add_header(k, v)
  end,
  set = function(k, v)
    kong_header.set_header(k, v)
  end,
  delete = function(k, v)
    kong_header.clear_header(k)
  end,
  setnx = function(k, v)
    local old_value = kong_header.get_header(k)
    if old_value ~= nil then
      kong_header.set_header(k, v)
    end
  end
}

function Header:transform_header()
  local config = self._data
  local action = header_mediator[config.action]
  for k, v in pairs(config.data) do
    local val, err = self:context_value(v)
    if err == nil then
      action(k, val)
    else
      return err
    end
  end
end

return Header
