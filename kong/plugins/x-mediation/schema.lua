-- Copyright (c) 2012-2017 Grab Taxi Holdings PTE LTD (GRAB), All Rights Reserved. NOTICE: All information contained herein
-- is, and remains the property of GRAB. The intellectual and technical concepts contained herein are confidential, proprietary
-- and controlled by GRAB and may be covered by patents, patents in process, and are protected by trade secret or copyright law.

-- You are strictly forbidden to copy, download, store (in any medium), transmit, disseminate, adapt or change this material
-- in any way unless prior written permission is obtained from GRAB. Access to the source code contained herein is hereby
-- forbidden to anyone except current GRAB employees or contractors with binding Confidentiality and Non-disclosure agreements
-- explicitly covering such access.

-- The copyright notice above does not evidence any actual or intended publication or disclosure of this source code,
-- which includes information that is confidential and/or proprietary, and is a trade secret, of GRAB.

-- ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE
-- CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF GRAB IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND
-- INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY
-- OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING
-- THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

local Errors = require "kong.db.errors"
local cjson = require "cjson"
local pcall = pcall
local function parse_json(body)
  if body then
    local status, res = pcall(cjson.decode, body)
    if status then
      return res
    end
  end
end

local MEDIATOR_TYPES = {"header", "param", "path", "header2query"}
local MEDIATOR_ACTIONS = {"add", "set", "delete", "setnx", "rename", "replace"}

local function contain(t, v)
  for _, _v in pairs(t) do
    if _v == v then
      return true
    end
  end
  return false
end

local mediators_field = "mediators"
return {
  no_consumer = true,
  fields = {
    mediator_index = {type = "array", required = true},
    mediators = {type = "string", required = true},
    base_path = {type = "string", required = true}
  },
  self_check = function(schema, plugin, dao, is_updating)
    local mediators = parse_json(plugin.mediators)
    if mediators == nil then
      local error = {
        [mediators_field] = "Data must provide proper JSON"
      }
      return false, Errors:schema_violation(error)
    end
    for key, mediator in pairs(mediators) do
      if not contain(MEDIATOR_TYPES, mediator.type) then
        local error = {
          [mediators_field] = {
            [key .. ".type"] = "Wrong type " .. mediator.type .. ". Must be one of (header, param, path, header2query)"
          }
        }
        return false, Errors:schema_violation(error)
      end
      if not contain(MEDIATOR_ACTIONS, mediator.action) then
        local error = {
          [mediators_field] = {
            [key .. ".action"] = "Wrong action " ..
              mediator.action .. ". Must be one of (add, set, delete, setnx, rename, replace)"
          }
        }
        return false, Errors:schema_violation(error)
      end
      if mediator.data == nil then
        local error = {
          [mediators_field] = {
            [key .. ".data"] = "Must have json data with at least 1 key:value"
          }
        }
        return false, Errors:schema_violation(error)
      end
    end
    return true
  end
}
