-- Copyright (c) 2012-2017 Grab Taxi Holdings PTE LTD (GRAB), All Rights Reserved. NOTICE: All information contained herein
-- is, and remains the property of GRAB. The intellectual and technical concepts contained herein are confidential, proprietary
-- and controlled by GRAB and may be covered by patents, patents in process, and are protected by trade secret or copyright law.

-- You are strictly forbidden to copy, download, store (in any medium), transmit, disseminate, adapt or change this material
-- in any way unless prior written permission is obtained from GRAB. Access to the source code contained herein is hereby
-- forbidden to anyone except current GRAB employees or contractors with binding Confidentiality and Non-disclosure agreements
-- explicitly covering such access.

-- The copyright notice above does not evidence any actual or intended publication or disclosure of this source code,
-- which includes information that is confidential and/or proprietary, and is a trade secret, of GRAB.

-- ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE
-- CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF GRAB IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND
-- INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY
-- OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING
-- THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

package.loaded["kong.plugins.commons.profiling"] = require("kong.spec.utils.profiling-mock")

local kong_log = require("kong.spec.utils.kong-log-mock")
package.loaded["kong.plugins.commons.log"] = kong_log
local kong_mock = {}
_G["kong"] = kong_mock

local noop = require("kong.spec.utils.noop")

local plugin_handler = require "kong.plugins.x-mediation.handler"

describe(
  "UNITTEST - Rewrite Plugin",
  function()
    local old_ngx = _G.ngx
    local old_kong = _G.kong
    local grab_handler
    local snapshot

    local stubbed_kong
    local stubbed_ngx

    local mock_config = {}

    local mockKong = function(basePath, testPath)
      local response_mock = { exit = spy.new(noop) }

      stubbed_kong = {
        request = {
          get_header = spy.new(
            function()
            end
          ),
          get_path = function(...)
            return basePath .. testPath
          end
        },
        ctx = {
          shared = {
            session_user = {
              ["$ctx.session.UserID"] = "context-session-user-id",
              ["$ctx.session.ServiceUserID"] = "context-service-user-id",
              ["$ctx.session.RegisteredOnService"] = "context-service-registered-on-service",
              ["$ctx.session.Oauth2ContextID"] = "context-service-oauth2-context-id",
              ["$ctx.session.ClientID"] = "context-service-client-id",
              ["$ctx.session.TokenID"] = "context-service-token-id"

            }
          }
        },

        response = response_mock
      }
      _G.kong = setmetatable(stubbed_kong, { __index = old_kong })
    end
    before_each(
      function()
        snapshot = assert:snapshot()

        stubbed_ngx = {
          var = {
            upstream_uri = "/replaced/upstream/me"
          },
          ctx = {
            route = {
              host = nil,
              id = "5adb6b69-304b-4e5b-9777-0b72f5b3f035",
              methods = { "GET", "POST", "PUT" },
              paths = { "/authenticated" },
              service = { id = "92cb26b2-3663-4e30-b918-6bafd783ab39" }
            },
            req_uri = "/authenticated"
          }
        }
        _G.ngx = setmetatable(stubbed_ngx, { __index = old_ngx })
        mockKong("/", "")
        grab_handler = plugin_handler()
      end
    )

    after_each(
      function()
        snapshot:revert()
        _G.ngx = old_ngx
      end
    )

    describe(
      "Unit: Test Client",
      function()
        it(
          "Reflect test from Golang: grab-api/api-gateway/plugins/httpmiddleware/mediation/mediator/url_path_test.go:TestPathMediatorReplaceBasePath",
          function()
            local basePath = "/api/passenger/v2/grabpay"
            local testPath = "/credit/transaction/1232321/?id=12342323"
            local replacedPath = "/passenger"
            mockKong(basePath, testPath)
            mock_config = {
              service_id = "service-uuid",
              mediator_index = { "replace_path" },
              base_path = basePath,
              mediators = [[{"replace_path": {"type": "path", "action": "replace", "data": {"$basePath": "/passenger"}}}]]
            }
            grab_handler:access(mock_config)
            assert.are.same(ngx.var.upstream_uri, replacedPath .. testPath)
          end
        )
        it(
          "Reflect test from Golang: grab-api/api-gateway/plugins/httpmiddleware/mediation/mediator/url_path_test.go:TestPathMediatorReplacePath",
          function()
            local basePath = "/enterprise"
            local testPath = "/graphql"
            local replacedPath = "/graphql/enterprise"
            mock_config = {
              service_id = "service-uuid",
              mediator_index = { "replace_path" },
              base_path = basePath,
              mediators = [[{"replace_path": {"type": "path", "action": "replace", "data": {"/enterprise/graphql": "/graphql/enterprise"}}}]]
            }
            mockKong(basePath, testPath)
            grab_handler:access(mock_config)
            assert.are.same(ngx.var.upstream_uri, replacedPath)
          end
        )
        it(
          "We would like to replace new path for upstream including context.service-id",
          function()
            local basePath = "/api/passenger/v2/grabpay"
            local testPath = "/credit/transaction/1232321/?id=12342323"
            local replacedPath = "/passenger"
            mock_config = {
              service_id = "service-uuid",
              mediator_index = { "replace_path" },
              base_path = basePath,
              mediators = [[{"replace_path": {"type": "path", "action": "replace", "data": {"$basePath": "/passenger/{{$ctx.session.ServiceUserID}}"}}}]]
            }
            mockKong(basePath, testPath)
            grab_handler:access(mock_config)
            assert.are.same(ngx.var.upstream_uri, replacedPath .. "/" .. "context-service-user-id" .. testPath)
          end
        )
        it(
          "We would like to replace new path for upstream including context.service-id without {{}}",
          function()
            local basePath = "/api/passenger/v2/grabpay"
            local testPath = "/credit/transaction/1232321/?id=12342323"
            local replacedPath = "/passenger"
            mock_config = {
              service_id = "service-uuid",
              mediator_index = { "replace_path" },
              base_path = basePath,
              mediators = [[{"replace_path": {"type": "path", "action": "replace", "data": {"$basePath": "/passenger/$ctx.session.ServiceUserID"}}}]]
            }
            mockKong(basePath, testPath)
            grab_handler:access(mock_config)
            assert.are.same(ngx.var.upstream_uri, replacedPath .. "/" .. "context-service-user-id" .. testPath)
          end
        )
        it(
          "should be able to replace ctx variables in the middle of the path template",
          function()
            local basePath = "/api/passenger/v2/grabpay"
            local testPath = "/credit/transaction/1232321/?id=12342323"
            local replacedPath = "/passenger"
            mock_config = {
              service_id = "service-uuid",
              mediator_index = { "replace_path" },
              base_path = basePath,
              mediators = [[{"replace_path": {"type": "path", "action": "replace", "data": {"$basePath": "/passenger/{{$ctx.session.ServiceUserID}}/id"}}}]]
            }
            mockKong(basePath, testPath)
            grab_handler:access(mock_config)
            assert.are.same(ngx.var.upstream_uri, replacedPath .. "/" .. "context-service-user-id" .. "/id" .. testPath)
          end
        )
        it(
          "should be able to replace ctx variables in the middle of the path template without {{}}",
          function()
            local basePath = "/api/passenger/v2/grabpay"
            local testPath = "/credit/transaction/1232321/?id=12342323"
            local replacedPath = "/passenger"
            mock_config = {
              service_id = "service-uuid",
              mediator_index = { "replace_path" },
              base_path = basePath,
              mediators = [[{"replace_path": {"type": "path", "action": "replace", "data": {"$basePath": "/passenger/$ctx.session.ServiceUserID/id"}}}]]
            }
            mockKong(basePath, testPath)
            grab_handler:access(mock_config)
            assert.are.same(ngx.var.upstream_uri, replacedPath .. "/" .. "context-service-user-id" .. "/id" .. testPath)
          end
        )
        it(
          "should be able to replace ctx variables in the middle of the path template",
          function()
            local basePath = "/api/passenger/v2/grabpay"
            local testPath = "/credit/transaction/1232321/?id=12342323"
            local replacedPath = "/passenger"
            mock_config = {
              service_id = "service-uuid",
              mediator_index = { "replace_path" },
              base_path = basePath,
              mediators = [[{"replace_path": {"type": "path", "action": "replace", "data": {"$basePath": "/passenger/{{$ctx.session.ServiceUserID}}{{$ctx.session.UserID}}/id"}}}]]
            }
            mockKong(basePath, testPath)
            grab_handler:access(mock_config)
            assert.are.same(ngx.var.upstream_uri, replacedPath .. "/" .. "context-service-user-id" .. "context-session-user-id" .. "/id" .. testPath)
          end
        )
        it(
          "should be able to replace ctx multi variables with {{}} & without {{}} in the middle of the path template",
          function()
            local basePath = "/api/passenger/v2/grabpay"
            local testPath = "/credit/transaction/1232321/?id=12342323"
            local replacedPath = "/passenger"
            mock_config = {
              service_id = "service-uuid",
              mediator_index = { "replace_path" },
              base_path = basePath,
              mediators = [[{"replace_path": {"type": "path", "action": "replace", "data": {"$basePath": "/passenger/$ctx.session.ServiceUserID{{$ctx.session.UserID}}/id"}}}]]
            }
            mockKong(basePath, testPath)
            grab_handler:access(mock_config)
            assert.are.same(ngx.var.upstream_uri, replacedPath .. "/" .. "context-service-user-id" .. "context-session-user-id" .. "/id" .. testPath)
          end
        )
        it(
          "should be able to replace ctx variable without {{}} and prefix in the middle of the path template",
          function()
            local basePath = "/api/passenger/v2/grabpay"
            local testPath = "/credit/transaction/1232321/?id=12342323"
            local replacedPath = "/passenger"
            mock_config = {
              service_id = "service-uuid",
              mediator_index = { "replace_path" },
              base_path = basePath,
              mediators = [[{"replace_path": {"type": "path", "action": "replace", "data": {"$basePath": "/passenger/prefix$ctx.session.ServiceUserID/id"}}}]]
            }
            mockKong(basePath, testPath)
            grab_handler:access(mock_config)
            assert.are.same(ngx.var.upstream_uri, replacedPath .. "/" .. "prefix" .. "context-service-user-id" .. "/id" .. testPath)
          end
        )
        it(
          "should be able to replace ctx variable with {{}} and prefix in the middle of the path template",
          function()
            local basePath = "/api/passenger/v2/grabpay"
            local testPath = "/credit/transaction/1232321/?id=12342323"
            local replacedPath = "/passenger"
            mock_config = {
              service_id = "service-uuid",
              mediator_index = { "replace_path" },
              base_path = basePath,
              mediators = [[{"replace_path": {"type": "path", "action": "replace", "data": {"$basePath": "/passenger/prefix{{$ctx.session.ServiceUserID}}/id"}}}]]
            }
            mockKong(basePath, testPath)
            grab_handler:access(mock_config)
            assert.are.same(ngx.var.upstream_uri, replacedPath .. "/" .. "prefix" .. "context-service-user-id" .. "/id" .. testPath)
          end
        )
        it(
          "should be able to replace ctx multi variables with {{}} & without {{}} and prefix in the middle of the path template",
          function()
            local basePath = "/api/passenger/v2/grabpay"
            local testPath = "/credit/transaction/1232321/?id=12342323"
            local replacedPath = "/passenger"
            mock_config = {
              service_id = "service-uuid",
              mediator_index = { "replace_path" },
              base_path = basePath,
              mediators = [[{"replace_path": {"type": "path", "action": "replace", "data": {"$basePath": "/passenger/prefix$ctx.session.ServiceUserID{{$ctx.session.UserID}}/id"}}}]]
            }
            mockKong(basePath, testPath)
            grab_handler:access(mock_config)
            assert.are.same(ngx.var.upstream_uri, replacedPath .. "/" .. "prefix" .. "context-service-user-id" .. "context-session-user-id" .. "/id" .. testPath)
          end
        )
        it(
          "should be able to return 500 error with wrong variable",
          function()
            local basePath = "/api/passenger/v2/grabpay"
            local testPath = "/credit/transaction/1232321/?id=12342323"
            mock_config = {
              service_id = "service-uuid",
              mediator_index = { "replace_path" },
              base_path = basePath,
              mediators = [[{"replace_path": {"type": "path", "action": "replace", "data": {"$basePath": "/passenger/$ctx.session.ServiceX/id"}}}]]
            }
            mockKong(basePath, testPath)
            grab_handler:access(mock_config)
            assert.spy(stubbed_kong.response.exit).was.called_with(500, "Internal Server Error")
          end
        )
        it(
          "should be able to return 500 error with wrong variable",
          function()
            local basePath = "/api/passenger/v2/grabpay"
            local testPath = "/credit/transaction/1232321/?id=12342323"
            mock_config = {
              service_id = "service-uuid",
              mediator_index = { "replace_path" },
              base_path = basePath,
              mediators = [[{"replace_path": {"type": "path", "action": "replace", "data": {"$basePath": "/passenger/{{$ctx.session.XServiceX}}/id"}}}]]
            }
            mockKong(basePath, testPath)
            grab_handler:access(mock_config)
            assert.spy(stubbed_kong.response.exit).was.called_with(500, "Internal Server Error")
          end
        )
        it(
          "should be able to replace new ctx multi variables with {{}} & without {{}} and prefix in the middle of the path template",
          function()
            local basePath = "/api/passenger/v2/grabpay"
            local testPath = "/credit/transaction/1232321/?id=12342323"
            local replacedPath = "/passenger"
            mock_config = {
              service_id = "service-uuid",
              mediator_index = { "replace_path" },
              base_path = basePath,
              mediators = [[{"replace_path": {"type": "path", "action": "replace", "data": {"$basePath": "/passenger/prefix$ctx.session.RegisteredOnService{{$ctx.session.Oauth2ContextID}}$ctx.session.ClientID{{$ctx.session.TokenID}}/id"}}}]]
            }
            mockKong(basePath, testPath)
            grab_handler:access(mock_config)
            assert.are.same(ngx.var.upstream_uri, replacedPath .. "/" .. "prefix" .. "context-service-registered-on-service" .. "context-service-oauth2-context-id" .. "context-service-client-id" .. "context-service-token-id" .. "/id" .. testPath)
          end
        )
        it(
          "Properly escape matching pattern when replacing path",
          function()
            local pattern = "/().%+-*?[]^$/"
            local replacement = "/escaped/"

            local testPath = "/grabdoc-sample/().%+-*?[]^$/v1/().%+-*?[]^$/foo"
            local expetedPath = "/grabdoc-sample/escaped/v1/escaped/foo"

            mock_config = {
              service_id = "service-uuid",
              mediator_index = { "replace_path" },
              mediators = [[
                {
                  "replace_path": {
                    "type": "path",
                    "action": "replace",
                    "data": {
                      "]] ..
                pattern ..
                [[": "]] ..
                replacement .. [["
                    }
                  }
                }
              ]]
            }

            mockKong("", testPath)
            grab_handler:access(mock_config)
            assert.are.same(ngx.var.upstream_uri, expetedPath)
          end
        )
        it(
          "Only replace basePath at the start of request path",
          function()
            local basePath = "/grabdoc-sample/v1"
            local replacement = "/replaced"

            local testPath = "/grabdoc-sample/v1/1232321/grabdoc-sample/v1/foo?id=12342323"
            local expectedPath = "/replaced/1232321/grabdoc-sample/v1/foo?id=12342323"

            mock_config = {
              service_id = "service-uuid",
              mediator_index = { "replace_path" },
              base_path = basePath,
              mediators = [[
                {
                  "replace_path": {
                    "type": "path",
                    "action": "replace",
                    "data": {
                      "$basePath": "]] ..
                replacement .. [["
                    }
                  }
                }
              ]]
            }

            mockKong("", testPath)
            grab_handler:access(mock_config)

            assert.are.same(ngx.var.upstream_uri, expectedPath)
          end
        )
      end
    )
  end
)
