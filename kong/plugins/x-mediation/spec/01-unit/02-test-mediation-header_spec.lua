-- Copyright (c) 2012-2017 Grab Taxi Holdings PTE LTD (GRAB), All Rights Reserved. NOTICE: All information contained herein
-- is, and remains the property of GRAB. The intellectual and technical concepts contained herein are confidential, proprietary
-- and controlled by GRAB and may be covered by patents, patents in process, and are protected by trade secret or copyright law.

-- You are strictly forbidden to copy, download, store (in any medium), transmit, disseminate, adapt or change this material
-- in any way unless prior written permission is obtained from GRAB. Access to the source code contained herein is hereby
-- forbidden to anyone except current GRAB employees or contractors with binding Confidentiality and Non-disclosure agreements
-- explicitly covering such access.

-- The copyright notice above does not evidence any actual or intended publication or disclosure of this source code,
-- which includes information that is confidential and/or proprietary, and is a trade secret, of GRAB.

-- ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE
-- CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF GRAB IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND
-- INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY
-- OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING
-- THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.

package.loaded["kong.plugins.commons.profiling"] = require("kong.spec.utils.profiling-mock")
package.loaded.kong = nil

local kong_log = require("kong.spec.utils.kong-log-mock")
package.loaded["kong.plugins.commons.log"] = kong_log
local kong_mock = {}
_G["kong"] = kong_mock

local mediator_handler = require "kong.plugins.x-mediation.handler"

describe(
  "UNITTEST - Mediator Handler",
  function()
    local old_kong = _G.kong
    local mock_config

    local snapshot

    local stubbed_kong
    local spy_on_add_header
    local spy_on_set_header
    local spy_on_clear_header
    local handler
    local headers = {authorization = "Unauthorized", ["x-partner-id"] = "old"}

    before_each(
      function()
        snapshot = assert:snapshot()
        spy_on_add_header =
          spy.new(
          function(...)
          end
        )
        spy_on_set_header =
          spy.new(
          function(...)
          end
        )
        spy_on_clear_header =
          spy.new(
          function(...)
          end
        )
        stubbed_kong = {
          service = {
            request = {
              add_header = function(k, v)
                spy_on_add_header(k, v)
                if headers[k] then
                  headers[k] = headers[k] .. ', ' .. v
                else
                  headers[k] = v
                end
              end,
              set_header = function(k, v)
                spy_on_set_header(k, v)
                headers[k] = v
              end,
              clear_header = function(...)
                spy_on_clear_header(...)
              end
            }
          },
          request = {
            get_header = function(k)
              return headers[k]
            end
          },
          ctx = {
            shared = {
              session_user = {
                ["$ctx.session.UserID"] = "context-session-user-id",
                ["$ctx.session.ServiceUserID"] = "context-service-user-id",
                ["$ctx.session.RegisteredOnService"] = "context-service-registered-on-service",
                ["$ctx.session.Oauth2ContextID"] = "context-service-oauth2-context-id",
                ["$ctx.session.ClientID"] = "context-service-client-id",
                ["$ctx.session.TokenID"] = "context-service-token-id"
              }
            }
          }
        }
        _G.kong = setmetatable(stubbed_kong, {__index = old_kong})
        handler = mediator_handler()
      end
    )

    after_each(
      function()
        snapshot:revert()
        _G.kong = old_kong
      end
    )

    describe(
      "Unit: Test Add header was called",
      function()
        local match = require("luassert.match")
        local _ = match._
        it(
          "this is a fallback request, and should not be modified",
          function()
            stubbed_kong.ctx.shared.is_fallback = true
            mock_config = {
              service_id = "service-uuid",
              mediator_index = {"add_new_header"},
              mediators = [[{"add_new_header": {"type": "header", "action": "add", "data": {"x-partner-id": "something", "sub": "header"}}}]]
            }
            handler:access(mock_config)

            assert.spy(spy_on_set_header).was_not_called_with(_, _)
            assert.spy(spy_on_set_header).was_not_called_with(_, _)
          end
        )
        it(
          "When config has add header feature",
          function()
            mock_config = {
              service_id = "service-uuid",
              mediator_index = {"add_header", "add_header_again"},
              mediators = [[{
                "add_header": {
                  "type": "header",
                  "action": "add",
                  "data": {
                    "Cache-Control": "no-cache"
                  }
                },
                "add_header_again": {
                  "type": "header",
                  "action": "add",
                  "data": {
                    "Cache-Control": "no-store"
                  }
                }
              }]]
            }
            handler:access(mock_config)

            assert.spy(spy_on_add_header).was_called(2)
            assert.spy(spy_on_add_header).was_called_with("Cache-Control", "no-cache")
            assert.spy(spy_on_add_header).was_called_with("Cache-Control", "no-store")
          end
        )
        it(
          "When config has set header feature",
          function()
            headers = {authorization = "Unauthorized", ["x-partner-id"] = "old"}
            mock_config = {
              service_id = "service-uuid",
              mediator_index = {"set_header"},
              mediators = [[{"set_header": {"type": "header", "action": "set", "data": {"x-partner-id": "new", "sub": "header"}}}]]
            }
            handler:access(mock_config)

            assert.spy(spy_on_set_header).was_called(2)
            assert.spy(spy_on_set_header).was_called_with("x-partner-id", "new")
            assert.spy(spy_on_set_header).was_called_with("sub", "header")
          end
        )
        it(
          "When config has delete header feature",
          function()
            headers = {authorization = "Unauthorized", ["x-partner-id"] = "old"}
            mock_config = {
              service_id = "service-uuid",
              mediator_index = {"delete_header"},
              mediators = [[{"delete_header": {"type": "header", "action": "delete", "data": {"x-partner-id": "new", "sub": "header"}}}]]
            }
            handler:access(mock_config)

            assert.spy(spy_on_clear_header).was_called(2)
            assert.spy(spy_on_clear_header).was_called_with("x-partner-id")
            assert.spy(spy_on_clear_header).was_called_with("sub")
          end
        )
        it(
          "When config: setnx header feature",
          function()
            headers = {authorization = "Unauthorized", ["x-partner-id"] = "old"}
            mock_config = {
              service_id = "service-uuid",
              mediator_index = {"setnx_header"},
              mediators = [[{"setnx_header": {"type": "header", "action": "setnx", "data": {"x-partner-id": "new", "sub": "header"}}}]]
            }
            handler:access(mock_config)

            assert.spy(spy_on_set_header).was_called(1)
            assert.spy(spy_on_set_header).was_called_with("x-partner-id", "new")
            assert.are.same(headers, {authorization = "Unauthorized", ["x-partner-id"] = "new"})
          end
        )
        it(
          "When config: add header feature by context.session",
          function()
            headers = {authorization = "Unauthorized"}
            mock_config = {
              service_id = "service-uuid",
              mediator_index = {"add_header"},
              mediators = [[{"add_header": {"type": "header", "action": "add", "data": {"context-user": "$ctx.session.UserID", "service-user-id": "PASSENGER:{{$ctx.session.ServiceUserID}}"}}}]]
            }
            handler:access(mock_config)

            assert.spy(spy_on_add_header).was_called(2)
            assert.spy(spy_on_add_header).was_called_with("context-user", "context-session-user-id")
            assert.spy(spy_on_add_header).was_called_with("service-user-id", "PASSENGER:context-service-user-id")
            assert.are.same(
              headers,
              {
                authorization = "Unauthorized",
                ["context-user"] = "context-session-user-id",
                ["service-user-id"] = "PASSENGER:context-service-user-id",
              }
            )
          end
        )
        it(
          "should add header feature by new context.session",
          function()
            headers = {authorization = "Unauthorized"}
            mock_config = {
              service_id = "service-uuid",
              mediator_index = {"add_header"},
              mediators = [[{"add_header": {"type": "header", "action": "add", "data": {"context-user": "$ctx.session.UserID", "service-user-id": "PASSENGER:{{$ctx.session.ServiceUserID}}", "context-service-registered-on-service": "$ctx.session.RegisteredOnService", "context-service-oauth2-context-id": "{{$ctx.session.Oauth2ContextID}}", "context-service-client-id": "$ctx.session.ClientID", "context-service-token-id": "{{$ctx.session.TokenID}}"}}}]]
            }
            handler:access(mock_config)

            assert.spy(spy_on_add_header).was_called(6)
            assert.spy(spy_on_add_header).was_called_with("context-user", "context-session-user-id")
            assert.spy(spy_on_add_header).was_called_with("service-user-id", "PASSENGER:context-service-user-id")
            assert.spy(spy_on_add_header).was_called_with("context-service-registered-on-service", "context-service-registered-on-service")
            assert.spy(spy_on_add_header).was_called_with("context-service-oauth2-context-id", "context-service-oauth2-context-id")
            assert.spy(spy_on_add_header).was_called_with("context-service-client-id", "context-service-client-id")
            assert.spy(spy_on_add_header).was_called_with("context-service-token-id", "context-service-token-id")
            assert.are.same(
              headers,
              {
                authorization = "Unauthorized",
                ["context-user"] = "context-session-user-id",
                ["service-user-id"] = "PASSENGER:context-service-user-id",
                ["context-service-registered-on-service"] = "context-service-registered-on-service",
                ["context-service-oauth2-context-id"] = "context-service-oauth2-context-id",
                ["context-service-client-id"] = "context-service-client-id",
                ["context-service-token-id"] = "context-service-token-id"
              }
            )
          end
        )
        it(
          "When config: has no proper index, kong wont do anything",
          function()
            headers = {authorization = "Unauthorized"}
            mock_config = {
              service_id = "service-uuid",
              mediator_index = {"non_exist"},
              mediators = [[{"add_header": {"type": "header", "action": "add", "data": {"context-user": "$ctx.session.UserID", "service-user-id": "PASSENGER:{{$ctx.session.ServiceUserID}}"}}}]]
            }
            handler:access(mock_config)

            assert.spy(spy_on_set_header).was_called(0)
          end
        )
      end
    )
  end
)
