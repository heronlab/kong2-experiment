-- Copyright (c) 2012-2017 Grab Taxi Holdings PTE LTD (GRAB), All Rights Reserved. NOTICE: All information contained herein
-- is, and remains the property of GRAB. The intellectual and technical concepts contained herein are confidential, proprietary
-- and controlled by GRAB and may be covered by patents, patents in process, and are protected by trade secret or copyright law.

-- You are strictly forbidden to copy, download, store (in any medium), transmit, disseminate, adapt or change this material
-- in any way unless prior written permission is obtained from GRAB. Access to the source code contained herein is hereby
-- forbidden to anyone except current GRAB employees or contractors with binding Confidentiality and Non-disclosure agreements
-- explicitly covering such access.

-- The copyright notice above does not evidence any actual or intended publication or disclosure of this source code,
-- which includes information that is confidential and/or proprietary, and is a trade secret, of GRAB.

-- ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC PERFORMANCE, OR PUBLIC DISPLAY OF OR THROUGH USE OF THIS SOURCE
-- CODE WITHOUT THE EXPRESS WRITTEN CONSENT OF GRAB IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE LAWS AND
-- INTERNATIONAL TREATIES. THE RECEIPT OR POSSESSION OF THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY
-- OR IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING
-- THAT IT MAY DESCRIBE, IN WHOLE OR IN PART.
package.loaded["kong.plugins.commons.profiling"] = require("kong.spec.utils.profiling-mock")
package.loaded.kong = nil

local kong_log = require("kong.spec.utils.kong-log-mock")
package.loaded["kong.plugins.commons.log"] = kong_log
local kong_mock = {}
_G["kong"] = kong_mock

local mediator_handler = require "kong.plugins.x-mediation.handler"

describe(
  "UNITTEST - Mediator Handler",
  function()
    local old_kong = _G.kong
    local mock_config

    local snapshot

    local stubbed_kong
    local spy_on_set_query
    local mock_params = {["oldmock"] = "old"}
    local handler

    before_each(
      function()
        snapshot = assert:snapshot()
        spy_on_set_query =
          spy.new(
          function(...)
          end
        )
        stubbed_kong = {
          service = {
            request = {
              set_query = function(v)
                spy_on_set_query(v)
              end
            }
          },
          request = {
            get_header = spy.new(
              function()
              end
            ),
            get_query = function()
              return mock_params
            end
          },
          ctx = {
            shared = {
              session_user = {
                ["$ctx.session.UserID"] = "context-session-user-id",
                ["$ctx.session.ServiceUserID"] = "context-service-user-id",
                ["$ctx.session.RegisteredOnService"] = "context-service-registered-on-service",
                ["$ctx.session.Oauth2ContextID"] = "context-service-oauth2-context-id",
                ["$ctx.session.ClientID"] = "context-service-client-id",
                ["$ctx.session.TokenID"] = "context-service-token-id"
              }
            }
          }
        }
        _G.kong = setmetatable(stubbed_kong, {__index = old_kong})
        handler = mediator_handler()
      end
    )

    after_each(
      function()
        snapshot:revert()
        _G.kong = old_kong
      end
    )

    describe(
      "Unit: Test Add header was called",
      function()
        it(
          "When config has add new param feature",
          function()
            mock_params = {}
            mock_config = {
              service_id = "service-uuid",
              mediator_index = {"add_new_params"},
              mediators = [[{"add_new_params": {"type": "param", "action": "add", "data": {"partner-id": "new", "sub": "header"}}}]]
            }
            handler:access(mock_config)

            assert.spy(spy_on_set_query).was_called_with({["partner-id"] = "new", ["sub"] = "header"})
          end
        )
        it(
          "When config has set new param feature",
          function()
            mock_params = {["mock"] = "old"}
            mock_config = {
              route_id = "service-route-uuid",
              mediator_index = {"set_new_params"},
              mediators = [[{"set_new_params": {"type": "param", "action": "set", "data": {"partner-id": "partner", "sub": "header"}}}]]
            }
            handler:access(mock_config)
            assert.spy(spy_on_set_query).was_called_with(
              {["mock"] = "old", ["partner-id"] = "partner", ["sub"] = "header"}
            )
          end
        )
        it(
          "When config has delete new param feature",
          function()
            mock_params = {["partner-id"] = "old", ["resist"] = "not-remove"}
            mock_config = {
              route_id = "service-route-uuid",
              mediator_index = {"delete_new_params"},
              mediators = [[{"delete_new_params": {"type": "param", "action": "delete", "data": {"partner-id": ""}}}]]
            }
            handler:access(mock_config)
            assert.are.same(mock_params["resist"], "not-remove")
            assert.are.same(mock_params["partner-id"], nil)
            assert.spy(spy_on_set_query).was_called_with({["resist"] = "not-remove"})
          end
        )
        it(
          "When config: setnx param feature",
          function()
            mock_params = {["x-partner-id"] = "old"}
            mock_config = {
              service_id = "service-uuid",
              mediator_index = {"setnx_params"},
              mediators = [[{"setnx_params": {"type": "param", "action": "setnx", "data": {"x-partner-id": "new", "sub": "header"}}}]]
            }
            handler:access(mock_config)

            assert.spy(spy_on_set_query).was_called(1)
            assert.are.same(mock_params["x-partner-id"], "new")
            assert.are.same(mock_params["sub"], nil)
            assert.spy(spy_on_set_query).was_called_with({["x-partner-id"] = "new"})
          end
        )
        it(
          "When config: rename param feature",
          function()
            mock_params = {["x-partner-id"] = "old"}
            mock_config = {
              service_id = "service-uuid",
              mediator_index = {"setnx_params"},
              mediators = [[{"setnx_params": {"type": "param", "action": "rename", "data": {"x-partner-id": "renamed-partner-id"}}}]]
            }
            handler:access(mock_config)

            assert.spy(spy_on_set_query).was_called(1)
            assert.are.same(nil, mock_params["x-partner-id"])
            assert.are.same(mock_params["renamed-partner-id"], "old")
            assert.spy(spy_on_set_query).was_called_with({["renamed-partner-id"] = "old"})
          end
        )
        it(
          "When config: add param feature by context.session",
          function()
            mock_params = {["old"] = "param"}
            mock_config = {
              service_id = "service-uuid",
              mediator_index = {"add_header", "rename_header"},
              mediators = [[{"add_header": {"type": "param", "action": "add", "data": {"service-user-id": "PASSENGER-{{$ctx.session.ServiceUserID}}"}}, "rename_header": {"type": "param", "action": "rename", "data": {"old": "new"}}}]]
            }
            handler:access(mock_config)

            assert.spy(spy_on_set_query).was_called(2)
            assert.are.same(mock_params, {["new"] = "param", ["service-user-id"] = "PASSENGER-context-service-user-id"})
            assert.spy(spy_on_set_query).was_called_with(mock_params)
          end
        )
        it(
          "should add param feature by new context.session",
          function()
            mock_params = {["old"] = "param"}
            mock_config = {
              service_id = "service-uuid",
              mediator_index = {"add_header", "rename_header"},
              mediators = [[{"add_header": {"type": "param", "action": "add", "data": {"context-user": "$ctx.session.UserID", "service-user-id": "PASSENGER-{{$ctx.session.ServiceUserID}}", "context-service-registered-on-service": "$ctx.session.RegisteredOnService", "context-service-oauth2-context-id": "{{$ctx.session.Oauth2ContextID}}", "context-service-client-id": "$ctx.session.ClientID", "context-service-token-id": "{{$ctx.session.TokenID}}"}}, "rename_header": {"type": "param", "action": "rename", "data": {"old": "new"}}}]]
            }
            handler:access(mock_config)

            assert.spy(spy_on_set_query).was_called(2)
            assert.are.same(mock_params, {["new"] = "param",
                                          ["context-user"] = "context-session-user-id",
                                          ["service-user-id"] = "PASSENGER-context-service-user-id",
                                          ["context-service-registered-on-service"] = "context-service-registered-on-service",
                                          ["context-service-oauth2-context-id"] = "context-service-oauth2-context-id",
                                          ["context-service-client-id"] = "context-service-client-id",
                                          ["context-service-token-id"] = "context-service-token-id"})
            assert.spy(spy_on_set_query).was_called_with(mock_params)
          end
        )
      end
    )
  end
)
