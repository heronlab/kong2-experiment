-- Dependencies
local typedefs = require "kong.db.schema.typedefs"
local fields = require("kong.plugins.x-clair.fields")

-- shadows responses, which upstreams return to Kong, to Clair
-- only shadows responses for requests marked as to be sent to Clair by x-clair plugin
local ClairSchema = {
  name = "x-clair-upstream-response-log",
  {
    consumer = typedefs.no_consumer
  },
  {
    service = typedefs.no_service
  },
  {
    route = typedefs.no_route
  },
  fields = {
    {
      config = {
        type = "record",
        fields = fields
      }
    }
  }
}

-- Export the schema
return ClairSchema
