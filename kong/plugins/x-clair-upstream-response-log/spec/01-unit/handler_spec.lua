package.loaded["kong.plugins.commons.profiling"] = require("kong.spec.utils.profiling-mock")

_G["kong"] = {}

local mock_ngx = {}
setmetatable(mock_ngx, {__index = ngx})
_G["ngx"] = mock_ngx

local kong_log_mock = require("kong.spec.utils.kong-log-mock")
package.loaded["kong.plugins.commons.log"] = kong_log_mock

package.loaded["kong.plugins.commons.clair"] = {}
local clair = require("kong.plugins.commons.clair")

local ClairHandler = require("kong.plugins.x-clair-upstream-response-log.handler")

describe(
  "x-upstream-response-log handler",
  function()
    local client_mock
    local clairHandler
    before_each(
      function(...)
        -- mock clair package
        client_mock = {}
        stub(client_mock, "track")
        clair.getClient = function()
          return client_mock
        end

        clair.getIdentifier = function(...)
          return "abc-xyz-123"
        end

        -- mock kong object
        kong.response = {
          get_headers = function()
            return {
              ["x-request-id"] = "x-request-id"
            }
          end
        }
        kong.ctx = {
          plugin = {}
        }

        -- mock ngx
        ngx.arg = {}

        clairHandler = ClairHandler()
      end
    )

    it(
      "should send headers to Clair during header_filter phase",
      function()
        clairHandler:header_filter({})

        local expected_clair_params = {
          Type = "UPSTREAM_RESPONSE",
          WatcherName = clair.WATCHER_NAME,
          KeyValuePairs = {
            {
              ["Key"] = clair.CORRELATION_ID,
              ["Value"] = clair.getIdentifier(kong)
            },
            {
              ["Key"] = "headers",
              ["Value"] = '{"x-request-id":"x-request-id"}'
            }
          }
        }

        assert.spy(client_mock.track).was_called_with(match._, match._)
        assert.are.same(expected_clair_params, client_mock.track.calls[1].vals[2])
      end
    )

    it(
      "should send response body to Clair during body_filter phase",
      function()
        ngx.arg = {"upstream-content1"}
        clairHandler:body_filter({})

        local expected_clair_params = {
          Type = "UPSTREAM_RESPONSE",
          WatcherName = clair.WATCHER_NAME,
          KeyValuePairs = {
            {
              ["Key"] = clair.CORRELATION_ID,
              ["Value"] = clair.getIdentifier(kong)
            },
            {
              ["Key"] = "body",
              ["Value"] = "upstream-content1"
            },
            {
              ["Key"] = "sequence",
              ["Value"] = 0
            }
          }
        }

        assert.spy(client_mock.track).was_called_with(match._, match._)
        assert.are.same(expected_clair_params, client_mock.track.calls[1].vals[2])

        ngx.arg = {"upstream-content2"}
        clairHandler:body_filter({})

        local expected_clair_params = {
          Type = "UPSTREAM_RESPONSE",
          WatcherName = clair.WATCHER_NAME,
          KeyValuePairs = {
            {
              ["Key"] = clair.CORRELATION_ID,
              ["Value"] = clair.getIdentifier(kong)
            },
            {
              ["Key"] = "body",
              ["Value"] = "upstream-content2"
            },
            {
              ["Key"] = "sequence",
              ["Value"] = 1
            }
          }
        }

        assert.spy(client_mock.track).was_called_with(match._, match._)
        assert.are.same(expected_clair_params, client_mock.track.calls[2].vals[2])
      end
    )

    it(
      "should not send response body to Clair during body_filter phase if eof",
      function()
        ngx.arg = {nil, true}
        clairHandler:body_filter({})

        assert.spy(client_mock.track).was_not_called(match._, match._)
      end
    )
  end
)
