local BasePlugin = require "kong.plugins.base_plugin"

local OpenTracingHandler = BasePlugin:extend()

OpenTracingHandler.PRIORITY = 150000
OpenTracingHandler.VERSION = "1.0.0"

local UNKNOWN_ROUTE = "kong"

local function get_route_name(route)
  if not route then
    return UNKNOWN_ROUTE
  end

  if (not route.paths or #route.paths == 0) then
    return UNKNOWN_ROUTE
  end

  local path = route.paths[1]
  -- Assuming paths follow the same convention. For example, /grabpay/partner/dax/v1/charge/(?<partnerTxID>[^/]+)/status$
  path = path:gsub("%(.-<(%w+)>.-%)", "{%1}")
  path = path:gsub("%$$", "")
  return path
end

function OpenTracingHandler:new()
  OpenTracingHandler.super.new(self, "x-opentracing")
end

function OpenTracingHandler:rewrite(conf)
  OpenTracingHandler.super.rewrite(self)

  local opentracing_binary_context = ngx.var.opentracing_binary_context
  if opentracing_binary_context then
    local opentracing_ctx = {
      binary_context = opentracing_binary_context,
      enabled_operation_names = conf.enabled_operation_names,
      track_span_actions = conf.track_span_actions
    }
    kong.ctx.shared.x_opentracing = opentracing_ctx
  end
end

function OpenTracingHandler:log(conf)
  OpenTracingHandler.super.log(self)
  local opentracing_ctx = kong.ctx.shared.x_opentracing
  if opentracing_ctx then
    local route_name = kong.router.get_route()
    ngx.var.opentracing_operation_name = get_route_name(route_name)
  end
end

return OpenTracingHandler
