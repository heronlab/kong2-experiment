local typedefs = require "kong.db.schema.typedefs"

return {
  name = "x-opentracing",
  fields = {
    {
      consumer = typedefs.no_consumer
    },
    {
      service = typedefs.no_service
    },
    {
      route = typedefs.no_route
    },
    {
      config = {
        type = "record",
        fields = {
          {
            enabled_operation_names = {
              type = "array",
              elements = {
                type = "string"
              }
            }
          },
          {
            track_span_actions = {
              type = "boolean",
              default = false
            }
          }
        }
      }
    }
  }
}
