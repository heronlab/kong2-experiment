local OpenTracingHandler = require "kong.plugins.x-opentracing.handler"

describe(
  "x-opentracing handler",
  function()
    local handler
    local conf = {
      enabled_operation_names = {"operation1", "operation2"},
      track_span_actions = true
    }
    local expected_ctx
    local route = {
      paths = {"/grabpay/partner/dax/v1/charge/(?<partnerTxID>[^/]+)/status$"}
    }

    before_each(
      function(...)
        handler = OpenTracingHandler()
        _G["ngx"] = {
          var = {
            opentracing_binary_context = "binary_ctx"
          }
        }
        _G["kong"] = {
          ctx = {
            shared = {}
          },
          router = {
            get_route = function(...)
              return route
            end
          }
        }
        expected_ctx = {
          track_span_actions = true,
          binary_context = ngx.var.opentracing_binary_context,
          enabled_operation_names = conf.enabled_operation_names
        }
      end
    )

    describe(
      "should update x_opentracing ctx and opentracing_operation_name variable ",
      function(...)
        local test_cases = {
          {
            route = route,
            operation_name = "/grabpay/partner/dax/v1/charge/{partnerTxID}/status",
            name = "should add x_opentracing ctx and set path to opentracing_operation_name variable"
          },
          {
            route = nil,
            operation_name = "kong",
            name = "should set kong to opentracing_operation_name variable if no route found"
          },
          {
            route = {},
            operation_name = "kong",
            name = "should set kong to opentracing_operation_name variable if no path found"
          }
        }
        for _, tc in ipairs(test_cases) do
          test(
            tc.name,
            function(...)
              kong.router.get_route = function()
                return tc.route
              end

              handler:rewrite(conf)

              assert.are.same(expected_ctx, kong.ctx.shared.x_opentracing)

              handler:log(conf)

              assert.are.same(tc.operation_name, ngx.var.opentracing_operation_name)
            end
          )
        end
      end
    )

    it(
      "should do nothing if opentracing cpp is not on",
      function(...)
        ngx.var = {}

        handler:rewrite(conf)

        assert.are.equal(nil, kong.ctx.shared.x_opentracing)

        handler:log(conf)

        assert.are.equal(nil, ngx.var.opentracing_operation_name)
      end
    )
  end
)
