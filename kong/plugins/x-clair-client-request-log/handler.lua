local BasePlugin = require "kong.plugins.base_plugin"
local profiling = require("kong.plugins.commons.profiling")
local CoreHandler = require("kong.plugins.x-clair-request-log-base.handler")

local trace_obj = profiling.trace_obj

local ClairHandler = BasePlugin:extend()

ClairHandler.PRIORITY = 199000
ClairHandler.VERSION = "1.0.0"

local NAME = "x-clair-client-request-log"

function ClairHandler:new()
  ClairHandler.super.new(self, NAME)
  self._coreHandler = CoreHandler("PARTNER_REQUEST")
end

function ClairHandler:rewrite(conf)
  self._coreHandler:rewrite(conf)
end

trace_obj(NAME, ClairHandler, {"rewrite"})
return ClairHandler
