--
-- Created by IntelliJ IDEA.
-- User: phuong.huynh
-- Date: 10/18/19
-- Time: 15:25
--
local Errors = require("kong.db.errors")

return {
  no_consumer = true,
  fields = {
    redis_host = {type = "string", required = true},
    redis_port = {type = "number", default = 6379},
    redis_password = {type = "string"},
    redis_timeout = {type = "number", default = 2000},
    redis_database = {type = "number", default = 0},
  },
  self_check = function (schema, plugin_t, dao, is_update)
    return true
  end
}
