local BasePlugin = require "kong.plugins.base_plugin"
local logger = require("kong.plugins.commons.log")
local pl_tablex = require("pl.tablex")
local redis_util = require("kong.plugins.utils.redis_util")
local pretty = require("pl.pretty")

local M = BasePlugin:extend()

M.PRIORITY = 100
M.VERSION = "0.1.0"

local function wrap_redis_call(conf, method, ...)
  local red, err = redis_util.new_conn(conf)
  if err then
    return err
  end
  return red[method](red, ...)
end

function M:new()
  M.super.new(self, "z-phuong-demo")
end

local function test_timer(premature, conf)
  kong.log.err("CONF: ", conf)
end

function M:access(conf)
  M.super.access(self)
  conf = pl_tablex.deepcopy(conf)

  local res, err = wrap_redis_call(conf, "get", "test:name")
  if err then
    logger.err("failed to call redis:", err)
  end
  logger.err("name:", res)

  ngx.timer.at(0, test_timer, conf)
end

return M
