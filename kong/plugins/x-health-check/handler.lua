-- Imports
local profiling = require("kong.plugins.commons.profiling")
local BasePlugin = require("kong.plugins.base_plugin")

-- Globals
local kong = _G.kong
local ngx = _G.ngx
local trace_obj = profiling.trace_obj

-- HealthCheckHandler plugin
local HealthCheckHandler = BasePlugin:extend()

-- Plugin priority and version
HealthCheckHandler.PRIORITY = 3000
HealthCheckHandler.VERSION = "1.0.0"

-- Plugin constructor
function HealthCheckHandler:new()
  HealthCheckHandler.super.new(self, "x-health-check")
end

-- Plugin access phase
function HealthCheckHandler:access(conf)
  HealthCheckHandler.super.access(self)

  kong.response.exit(ngx.HTTP_OK, {
    ["message"] = "Healthy",
  })
end

-- Export the handler
trace_obj("x-health-check", HealthCheckHandler, {"access"})
return HealthCheckHandler
