-- Dependencies
local testtools = require("kong.plugins.commons.testtools")

-- Globals
local schema = require("kong.plugins.x-health-check.schema")
local validate_plugin_config = testtools.validate_plugin_config

-- Test suite: x-health-check schema
describe("Plugin: x-health-check (schema)", function()
  it("should success", function()
    assert(validate_plugin_config({}, schema))
  end)
end)
