-- Intialization
-- Since these are unit tests, we do not have _G.kong initialized by default
_G.kong = require('kong.spec.utils.kong-mock')

-- Dependencies
local HealthCheckHandler = require("kong.plugins.x-health-check.handler")

-- Globals
local handler = HealthCheckHandler("x-health-check")
local kong = _G.kong
local ngx = _G.ngx

describe("Plugin: x-health-check (access)", function()
  it("should response HTTP 200 OK", function()
    -- Test preparation
    stub(kong.response, "exit")

    -- Actual test
    handler:access({})
    assert.stub(kong.response.exit).was.called_with(ngx.HTTP_OK, match.is_table())

    -- Test clean up
    kong.response.exit:revert()
  end)
end)
