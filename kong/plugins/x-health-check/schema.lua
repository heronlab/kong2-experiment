-- Dependencies
local typedefs = require "kong.db.schema.typedefs"

-- HealthCheckSchema
local HealthCheckSchema = {
  name = "x-health-check",
  fields = {
    {
      config = {
        type = "record",
        fields = {
        },
      },
    },
  },
}

-- Export the schema
return HealthCheckSchema
