# Change Log

## 1.0.0

* Initial implementation with hard-coded response ([145](https://gitlab.myteksi.net/grab-platform/kong-plugins/merge_requests/145))
