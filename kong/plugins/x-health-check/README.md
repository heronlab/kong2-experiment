# x-health-check

This plugin terminates the processing chain and return a 200 OK response immediately. It's main use-case is to turn any route into a health check endpoint
