local match = require("luassert.match")

local kong_log_mock = require("kong.spec.utils.kong-log-mock")
package.loaded["kong.plugins.commons.log"] = kong_log_mock

local socket_mock = {
  send = function()
  end,
  setpeername = function()
  end,
  close = function()
  end
}
local msg
local service_name = "ridebooking"

_G["ngx"] = setmetatable({
  ctx = {
    service = service_name
  },
  socket = {
    udp = function()
      return socket_mock
    end
  },
  timer = {
    at = function(_, func, ...)
      func(false, ...)
    end
  },
  log = function(...)
  end
}, {__index = ngx})

package.loaded["kong.plugins.log-serializers.basic"] = {
  serialize = function()
    return msg
  end
}

local BaseAnalyticsHandler = require "kong.plugins.x-base-analytics.handler"

describe(
  "Base Analytics handler",
  function()
    local proxied = "true"
    local forwarded_for = "na"
    local endpoint
    local route_paths = {"/grabpay/partner/dax/v1/charge/(?<partnerTxID>[^/]+)/status/(?<statusID>[^/]+)$"}
    local original_route_path = "/grabpay/partner/dax/v1/charge/{partnerTxID}/status/{statusID}"
    local partner_id = "partner_id"
    local client_id = "client_id"
    local default_tags
    local external_tags_table = {
      key = "value"
    }
    local external_tags = "key:value"
    before_each(
      function()
        stub(socket_mock, "send")
        msg = {
          service = {
            name = service_name
          },
          route = {
            paths = route_paths
          },
          request = {
            size = 100,
            method = "GET",
            headers = {}
          },
          response = {
            size = 200,
            status = 200
          },
          latencies = {
            request = 1,
            proxy = 2,
            kong = 3
          },
          consumer = {
            custom_id = "customID"
          }
        }

        endpoint = string.lower(msg.request.method) .. "_" .. original_route_path
        default_tags =
          external_tags ..
          "," ..
            "service_name:" ..
              service_name ..
                ",endpoint:" .. endpoint .. ",partner_id:" .. partner_id .. ",client_id:" .. client_id .. ",app:kong"

        _G.kong = {
          request = {
            get_path = function(...)
            end
          },
          service = {
            response = {
              get_status = function(...)
                return 200
              end
            }
          },
          ctx = {
            shared = {
              partner = {
                id = partner_id,
                client_id = client_id
              },
              x_datadog = {
                tags = external_tags_table
              }
            }
          },
          __index = _G.kong
        }
      end
    )

    it(
      "should send metrics to agent",
      function()
        local handler = BaseAnalyticsHandler("x-datadog")
        handler:log(
          {
            host = "host-name",
            port = "134",
            service_version_delimiter = "_",
            metrics = {
              {name = "request_size", ["stat_type"] = "gauge", tags = {"app:kong"}},
              {name = "response_size", ["stat_type"] = "gauge", tags = {"app:kong"}},
              {name = "latency", ["stat_type"] = "gauge", tags = {"app:kong"}},
              {name = "upstream_latency", ["stat_type"] = "gauge", tags = {"app:kong"}},
              {name = "kong_latency", ["stat_type"] = "gauge", tags = {"app:kong"}},
              {name = "request_count", ["stat_type"] = "counter", tags = {"app:kong"}}
            }
          }
        )

        assert.spy(socket_mock.send).was_called_with(match.is_not_nil(), "kong.request.size:100|g|#" .. default_tags)

        assert.spy(socket_mock.send).was_called_with(match.is_not_nil(), "kong.response.size:200|g|#" .. default_tags)

        assert.spy(socket_mock.send).was_called_with(match.is_not_nil(), "kong.latency:1|g|#" .. default_tags)

        assert.spy(socket_mock.send).was_called_with(match.is_not_nil(), "kong.upstream_latency:2|g|#" .. default_tags)

        assert.spy(socket_mock.send).was_called_with(match.is_not_nil(), "kong.request.count:1|c|#" .. default_tags)
      end
    )

    it(
      "should send status count metric to agent",
      function()
        local handler = BaseAnalyticsHandler("x-datadog")
        local conf = {
          host = "host-name",
          service_version_delimiter = "_",
          port = "134",
          metrics = {
            {name = "status_count", ["stat_type"] = "counter", tags = {"app:kong"}}
          }
        }
        handler:log(conf)

        assert.spy(socket_mock.send).was_called_with(
          match.is_not_nil(),
          "kong.request.status:1|c|#status:200,status_group:2xx,proxied:" ..
            proxied .. ",forwarded_source:" .. forwarded_for .. "," .. default_tags
        )

        kong.service.response.get_status =
          spy.new(
          function(...)
            return nil
          end
        )
        handler:log(conf)

        assert.spy(socket_mock.send).was_called_with(
          match.is_not_nil(),
          "kong.request.status:1|c|#status:200,status_group:2xx,proxied:false,forwarded_source:na," .. default_tags
        )
      end
    )

    it(
      "should send unique user metric to agent",
      function()
        local handler = BaseAnalyticsHandler("x-datadog")
        handler:log(
          {
            host = "host-name",
            port = "134",
            service_version_delimiter = "_",
            metrics = {
              {
                name = "unique_users",
                ["stat_type"] = "set",
                ["consumer_identifier"] = "custom_id",
                tags = {"app:kong"}
              }
            }
          }
        )

        assert.spy(socket_mock.send).was_called_with(
          match.is_not_nil(),
          "kong.consumer.uniques:customID|s|#" .. default_tags
        )
      end
    )

    it(
      "should send request per user metric to agent",
      function()
        local handler = BaseAnalyticsHandler("x-datadog")
        handler:log(
          {
            host = "host-name",
            port = "134",
            service_version_delimiter = "_",
            metrics = {
              {
                name = "request_per_user",
                ["stat_type"] = "counter",
                ["consumer_identifier"] = "custom_id",
                tags = {"app:kong"}
              }
            }
          }
        )

        assert.spy(socket_mock.send).was_called_with(
          match.is_not_nil(),
          "kong.consumer.customID.request.count:1|c|#" .. default_tags
        )
      end
    )

    it(
      "should send status count per user metric to agent",
      function()
        local handler = BaseAnalyticsHandler("x-datadog")
        local conf = {
          host = "host-name",
          port = "134",
          service_version_delimiter = "_",
          metrics = {
            {
              name = "status_count_per_user",
              ["stat_type"] = "counter",
              ["consumer_identifier"] = "custom_id",
              tags = {"app:kong"}
            }
          }
        }

        handler:log(conf)
        assert.spy(socket_mock.send).was_called_with(
          match.is_not_nil(),
          "kong.consumer.customID.request.status:1|c|#status:200,status_group:2xx,proxied:" ..
            proxied .. ",forwarded_source:" .. forwarded_for .. "," .. default_tags
        )

        kong.service.response.get_status =
          spy.new(
          function()
            return nil
          end
        )

        handler:log(conf)
        assert.spy(socket_mock.send).was_called_with(
          match.is_not_nil(),
          "kong.consumer.customID.request.status:1|c|#status:200,status_group:2xx,proxied:false,forwarded_source:na," ..
            default_tags
        )
      end
    )

    it(
      "should send empty service tag to agent if installed globally",
      function()
        local handler = BaseAnalyticsHandler("x-datadog")
        handler:log(
          {
            host = "host-name",
            service_version_delimiter = "_",
            port = "134",
            metrics = {
              {name = "status_count", ["stat_type"] = "counter", tags = {"app:kong"}}
            }
          }
        )
        assert.spy(socket_mock.send).was_called_with(
          match.is_not_nil(),
          "kong.request.status:1|c|#status:200,status_group:2xx,proxied:" ..
            proxied .. ",forwarded_source:" .. forwarded_for .. "," .. default_tags
        )
      end
    )

    it(
      "should send default partner_id and client_id if they are not in context",
      function()
        local handler = BaseAnalyticsHandler("x-datadog")
        local conf = {
          host = "host-name",
          service_version_delimiter = "_",
          port = "134",
          metrics = {
            {name = "status_count", ["stat_type"] = "counter", tags = {"app:kong"}}
          }
        }
        kong.ctx.shared.partner = nil
        handler:log(conf)

        assert.spy(socket_mock.send).was_called_with(
          match.is_not_nil(),
          "kong.request.status:1|c|#status:200,status_group:2xx,proxied:" ..
            proxied ..
              ",forwarded_source:" ..
                forwarded_for ..
                  "," ..
                    external_tags ..
                      "," ..
                        "service_name:" ..
                          service_name .. ",endpoint:" .. endpoint .. ",partner_id:na,client_id:na,app:kong"
        )
      end
    )

    it(
      "should send correct endpoint to stats server",
      function()
        local handler = BaseAnalyticsHandler("x-datadog")
        local conf = {
          host = "host-name",
          service_version_delimiter = "_",
          port = "134",
          metrics = {
            {name = "status_count", ["stat_type"] = "counter", tags = {"app:kong"}}
          }
        }

        msg.route.paths = {"/google$"}
        handler:log(conf)

        assert.spy(socket_mock.send).was_called_with(
          match.is_not_nil(),
          "kong.request.status:1|c|#status:200,status_group:2xx,proxied:" ..
            proxied ..
              ",forwarded_source:" ..
                forwarded_for ..
                  "," ..
                    external_tags ..
                      "," ..
                        "service_name:" ..
                          service_name ..
                            ",endpoint:" ..
                              string.lower(msg.request.method) ..
                                "_/google,partner_id:" .. partner_id .. ",client_id:" .. client_id .. ",app:kong"
        )

        -- should use route id if paths is null
        msg.route = {id = "route_id"}
        handler:log(conf)
        assert.spy(socket_mock.send).was_called_with(
          match.is_not_nil(),
          "kong.request.status:1|c|#status:200,status_group:2xx,proxied:" ..
            proxied ..
              ",forwarded_source:" ..
                forwarded_for ..
                  "," ..
                    external_tags ..
                      "," ..
                        "service_name:" ..
                          service_name ..
                            ",endpoint:" ..
                              string.lower(msg.request.method) ..
                                "_" ..
                                  msg.route.id ..
                                    ",partner_id:" .. partner_id .. ",client_id:" .. client_id .. ",app:kong"
        )

        -- should use route id if paths is empty
        msg.route = {id = "route_id", paths = {}}
        handler:log(conf)
        assert.spy(socket_mock.send).was_called_with(
          match.is_not_nil(),
          "kong.request.status:1|c|#status:200,status_group:2xx,proxied:" ..
            proxied ..
              ",forwarded_source:" ..
                forwarded_for ..
                  "," ..
                    external_tags ..
                      "," ..
                        "service_name:" ..
                          service_name ..
                            ",endpoint:" ..
                              string.lower(msg.request.method) ..
                                "_" ..
                                  msg.route.id ..
                                    ",partner_id:" .. partner_id .. ",client_id:" .. client_id .. ",app:kong"
        )

        -- shouldn't use route name if provided
        msg.route.name = "route_name"
        handler:log(conf)
        assert.spy(socket_mock.send).was_called_with(
          match.is_not_nil(),
          "kong.request.status:1|c|#status:200,status_group:2xx,proxied:" ..
            proxied ..
              ",forwarded_source:" ..
                forwarded_for ..
                  "," ..
                    external_tags ..
                      "," ..
                        "service_name:" ..
                          service_name ..
                            ",endpoint:" ..
                              string.lower(msg.request.method) ..
                                "_/google,partner_id:" .. partner_id .. ",client_id:" .. client_id .. ",app:kong"
        )

        -- should use path from request if route nil
        msg.route = nil
        kong.request.get_path =
        spy.new(
          function(...)
            return "/echo/v1/public1"
          end
        )

        handler:log(conf)
        assert.spy(socket_mock.send).was_called_with(
          match.is_not_nil(),
          "kong.request.status:1|c|#status:200,status_group:2xx,proxied:" ..
            proxied ..
            ",forwarded_source:" ..
            forwarded_for ..
            "," ..
            external_tags ..
            "," ..
            "service_name:" ..
            service_name ..
            ",endpoint:" ..
            string.lower(msg.request.method) ..
            "_/route-not-found" ..
            ",partner_id:" .. partner_id .. ",client_id:" .. client_id .. ",app:kong"
        )

      end
    )

    it(
      "should send latency per consumer to server",
      function()
        local handler = BaseAnalyticsHandler("x-datadog")
        local conf = {
          host = "host-name",
          port = "134",
          metrics = {
            {name = "latency_per_user", ["stat_type"] = "timer", consumer_identifier = "custom_id", tags = {"app:kong"}}
          }
        }
        handler:log(conf)

        assert.spy(socket_mock.send).was_called_with(
          match.is_not_nil(),
          "kong.consumer.customID.latency:1|ms|#status:200," .. default_tags
        )
      end
    )

    it(
      "should send status count metric with forwarded grab-gateway to agent",
      function()
        local handler = BaseAnalyticsHandler("x-datadog")
        local conf = {
          host = "host-name",
          service_version_delimiter = "_",
          port = "134",
          metrics = {
            {name = "status_count", ["stat_type"] = "counter", tags = {"app:kong"}}
          }
        }
        handler:log(conf)

        assert.spy(socket_mock.send).was_called_with(
          match.is_not_nil(),
          "kong.request.status:1|c|#status:200,status_group:2xx,proxied:" ..
            proxied .. ",forwarded_source:" .. forwarded_for .. "," .. default_tags
        )

        kong.service.response.get_status =
          spy.new(
          function(...)
            return nil
          end
        )

        msg.request.headers["X-Grab-ID-OAUTH2-PARTNER-ID"] = "partner_id"

        handler:log(conf)

        assert.spy(socket_mock.send).was_called_with(
          match.is_not_nil(),
          "kong.request.status:1|c|#status:200,status_group:2xx,proxied:false,forwarded_source:grab-gateway," ..
            default_tags
        )
      end
    )

    it(
      "should work even if nil analytics data ",
      function()
        kong.ctx.shared.x_datadog = nil
        local handler = BaseAnalyticsHandler("x-datadog")
        handler:log(
          {
            host = "host-name",
            port = "134",
            service_version_delimiter = "_",
            metrics = {
              {
                name = "unique_users",
                ["stat_type"] = "set",
                ["consumer_identifier"] = "custom_id",
                tags = {"app:kong"}
              }
            }
          }
        )

        assert.spy(socket_mock.send).was_called_with(
          match.is_not_nil(),
          "kong.consumer.uniques:customID|s|#" ..
            "service_name:" ..
              service_name ..
                ",endpoint:" .. endpoint .. ",partner_id:" .. partner_id .. ",client_id:" .. client_id .. ",app:kong"
        )
      end
    )

    it(
      "should work even if nil tags ",
      function()
        kong.ctx.shared.x_datadog = {}
        local handler = BaseAnalyticsHandler("x-datadog")
        handler:log(
          {
            host = "host-name",
            port = "134",
            service_version_delimiter = "_",
            metrics = {
              {
                name = "unique_users",
                ["stat_type"] = "set",
                ["consumer_identifier"] = "custom_id",
                tags = {"app:kong"}
              }
            }
          }
        )

        assert.spy(socket_mock.send).was_called_with(
          match.is_not_nil(),
          "kong.consumer.uniques:customID|s|#" ..
            "service_name:" ..
              service_name ..
                ",endpoint:" .. endpoint .. ",partner_id:" .. partner_id .. ",client_id:" .. client_id .. ",app:kong"
        )
      end
    )
  end
)
