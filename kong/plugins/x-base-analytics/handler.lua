local BasePlugin = require "kong.plugins.base_plugin"
local basic_serializer = require "kong.plugins.log-serializers.basic"
local statsd_logger = require "kong.plugins.x-base-analytics.statsd_logger"
local kong_logger = require("kong.plugins.commons.log")
local pl_table = require("pl.tablex")

local ngx_timer_at = ngx.timer.at
local string_gsub = string.gsub
local pairs = pairs
local string_format = string.format

local BaseAnalyticsHandler = BasePlugin:extend()

local get_consumer_id = {
  consumer_id = function(consumer)
    return consumer and string_gsub(consumer.id, "-", "_")
  end,
  custom_id = function(consumer)
    return consumer and consumer.custom_id
  end,
  username = function(consumer)
    return consumer and consumer.username
  end
}

local ROUTE_NOT_FOUND = "/route-not-found"

local STATUS_LABEL = {
  SERIES_2XX = "2xx",
  SERIES_3XX = "3xx",
  SERIES_4xx = "4xx",
  SERIES_5xx = "5xx"
}

local GRAB_GATEWAY_FORWARD = {
  SOURCE_NAME = "grab-gateway",
  HEADER_NAME = "X-Grab-ID-OAUTH2-PARTNER-ID",
  TAG_NAME = "forwarded_source:"
}

local function status_label(status)
  if status < 300 then
    return STATUS_LABEL.SERIES_2XX
  elseif status < 400 then
    return STATUS_LABEL.SERIES_3XX
  elseif status < 500 then
    return STATUS_LABEL.SERIES_4xx
  else
    return STATUS_LABEL.SERIES_5xx
  end
end

local function concat_table(des, src)
  for i = 1, #src do
    des[#des + 1] = src[i]
  end
  return des
end

local function is_proxied(service_status_code)
  local proxied = true
  if service_status_code == nil then
    proxied = false
  end
  return proxied
end

local function get_forwarded_for(request)
  local forwarded = "na"
  if request.headers[GRAB_GATEWAY_FORWARD.HEADER_NAME] then
    forwarded = GRAB_GATEWAY_FORWARD.SOURCE_NAME
  end
  return forwarded
end

local function get_route_name(route)
  -- Because we don't want to log path datadog & populate metric too much values so convert to not found
  local path = ROUTE_NOT_FOUND
  if route then
    if (not route.paths or #route.paths == 0) then
      return route.id
    end
    path = route.paths[1]
  end

  -- Assuming paths follow the same convention. For example, /grabpay/partner/dax/v1/charge/(?<partnerTxID>[^/]+)/status$
  path = path:gsub("%(.-<(%w+)>.-%)", "{%1}")
  path = path:gsub("%$$", "")
  return path
end

local function generate_default_tags(metric_config, message, partner, external_tags)
  local external_tags = external_tags or {}

  local partner = partner or {id = "na", client_id = "na"}
  local name = ""
  if message.service and message.service.name then
    name = string_gsub(message.service.name ~= ngx.null and message.service.name or message.service.host, "%.", "_")
  end
  name = string.gsub(name, "(%a)_(.*)", "%1")

  local external_statsd_tags =
    pl_table.pairmap(
    function(k, v)
      return k .. ":" .. v
    end,
    external_tags
  )

  local endpoint = string.lower(message.request.method) .. "_" .. get_route_name(message.route)

  local tags = {
    "service_name:" .. name,
    "endpoint:" .. endpoint,
    "partner_id:" .. partner.id,
    "client_id:" .. partner.client_id
  }
  local final_tags = concat_table(external_statsd_tags, tags)
  final_tags = concat_table(final_tags, metric_config.tags)
  return final_tags
end

local metrics = {
  status_count = function(message, metric_config, logger, default_tags, service_status_code)
    local tags = {
      "status:" .. message.response.status,
      "status_group:" .. status_label(message.response.status),
      "proxied:" .. tostring(is_proxied(service_status_code)),
      GRAB_GATEWAY_FORWARD.TAG_NAME .. tostring(get_forwarded_for(message.request))
    }
    tags = concat_table(tags, default_tags)
    local fmt = "request.status"

    logger:send_statsd(fmt, 1, logger.stat_types.counter, metric_config.sample_rate, tags)

    logger:send_statsd(
      string_format("%s.%s", fmt, "total"),
      1,
      logger.stat_types.counter,
      metric_config.sample_rate,
      default_tags
    )
  end,
  unique_users = function(message, metric_config, logger, default_tags)
    local get_consumer_id = get_consumer_id[metric_config.consumer_identifier]
    local consumer_id = get_consumer_id(message.consumer)

    if consumer_id then
      local stat = "consumer.uniques"

      logger:send_statsd(stat, consumer_id, logger.stat_types.set, nil, default_tags)
    end
  end,
  request_per_user = function(message, metric_config, logger, default_tags)
    local get_consumer_id = get_consumer_id[metric_config.consumer_identifier]
    local consumer_id = get_consumer_id(message.consumer)

    if consumer_id then
      local stat = string_format("consumer.%s.request.count", consumer_id)

      logger:send_statsd(stat, 1, logger.stat_types.counter, metric_config.sample_rate, default_tags)
    end
  end,
  status_count_per_user = function(message, metric_config, logger, default_tags, service_status_code)
    local tags = {
      "status:" .. message.response.status,
      "status_group:" .. status_label(message.response.status),
      "proxied:" .. tostring(is_proxied(service_status_code)),
      GRAB_GATEWAY_FORWARD.TAG_NAME .. tostring(get_forwarded_for(message.request))
    }
    tags = concat_table(tags, default_tags)

    local get_consumer_id = get_consumer_id[metric_config.consumer_identifier]
    local consumer_id = get_consumer_id(message.consumer)

    if consumer_id then
      local fmt = string_format("consumer.%s.request.status", consumer_id)
      logger:send_statsd(fmt, 1, logger.stat_types.counter, metric_config.sample_rate, tags)

      logger:send_statsd(
        string_format("%s.%s", fmt, "total"),
        1,
        logger.stat_types.counter,
        metric_config.sample_rate,
        default_tags
      )
    end
  end,
  latency_per_user = function(message, metric_config, logger, default_tags)
    local tags = {
      "status:" .. message.response.status
    }
    tags = concat_table(tags, default_tags)

    local get_consumer_id = get_consumer_id[metric_config.consumer_identifier]
    local consumer_id = get_consumer_id(message.consumer)

    if consumer_id then
      local fmt = string_format("consumer.%s.latency", consumer_id)
      logger:send_statsd(
        fmt,
        message.latencies.request,
        logger.stat_types[metric_config.stat_type],
        metric_config.sample_rate,
        tags
      )
    end
  end
}

local function log(premature, conf, message, service_status_code, partner, external_tags, req_path)
  if premature then
    return
  end

  if not message.route then
    -- Log to ELK for case NotFound in any service
    kong_logger.warn_with_context(message, "route not found: ", req_path)
  end

  local stat_name = {
    request_size = "request.size",
    response_size = "response.size",
    latency = "latency",
    upstream_latency = "upstream_latency",
    kong_latency = "kong_latency",
    request_count = "request.count"
  }

  local stat_value = {
    request_size = message.request.size,
    response_size = message.response.size,
    latency = message.latencies.request,
    upstream_latency = message.latencies.proxy,
    kong_latency = message.latencies.kong,
    request_count = 1
  }

  local logger, err = statsd_logger:new(conf)
  if err then
    kong_logger.err_with_context(message, "failed to create Statsd logger: ", err)
    return
  end

  for _, metric_config in pairs(conf.metrics) do
    local metric = metrics[metric_config.name]
    local default_tags = generate_default_tags(metric_config, message, partner, external_tags)

    if metric then
      metric(message, metric_config, logger, default_tags, service_status_code)
    else
      local stat_name = stat_name[metric_config.name]
      local stat_value = stat_value[metric_config.name]

      logger:send_statsd(
        stat_name,
        stat_value,
        logger.stat_types[metric_config.stat_type],
        metric_config.sample_rate,
        default_tags
      )
    end
  end

  logger:close_socket()
end

function BaseAnalyticsHandler:new(name)
  BaseAnalyticsHandler.super.new(self, name)
  self._ctx_name = name:gsub("-", "_")
end

function BaseAnalyticsHandler:log(conf)
  BaseAnalyticsHandler.super.log(self)

  local req_path = kong.request.get_path()
  -- Although in case of service upstream, the route is NotFound but kong.service.response.get_status() still work
  local service_status = kong.service.response.get_status()
  local message = basic_serializer.serialize(ngx)
  local external_tags = (kong.ctx.shared[self._ctx_name] and kong.ctx.shared[self._ctx_name].tags) or {}
  local partner = kong.ctx.shared.partner

  local ok, err = ngx_timer_at(0, log, conf, message, service_status, partner, external_tags, req_path)
  if not ok then
    kong_logger.err("failed to create timer: ", err)
  end
end

return BaseAnalyticsHandler
