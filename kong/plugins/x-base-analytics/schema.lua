local find = string.find
local pl_utils = require "pl.utils"

local STAT_NAMES = {
  "kong_latency",
  "latency",
  "request_count",
  "request_per_user",
  "request_size",
  "response_size",
  "status_count",
  "status_count_per_user",
  "unique_users",
  "upstream_latency",
  "latency_per_user"
}

local STAT_TYPES = {
  "counter",
  "gauge",
  "histogram",
  "meter",
  "set",
  "timer"
}

local CONSUMER_IDENTIFIERS = {
  "consumer_id",
  "custom_id",
  "username"
}

local DEFAULT_METRICS = {
  {
    name = "request_count",
    stat_type = "counter",
    sample_rate = 1,
    tags = {"app:kong"}
  },
  {
    name = "latency",
    stat_type = "timer",
    tags = {"app:kong"}
  },
  {
    name = "request_size",
    stat_type = "timer",
    tags = {"app:kong"}
  },
  {
    name = "status_count",
    stat_type = "counter",
    sample_rate = 1,
    tags = {"app:kong"}
  },
  {
    name = "response_size",
    stat_type = "timer",
    tags = {"app:kong"}
  },
  {
    name = "unique_users",
    stat_type = "set",
    consumer_identifier = "custom_id",
    tags = {"app:kong"}
  },
  {
    name = "request_per_user",
    stat_type = "counter",
    sample_rate = 1,
    consumer_identifier = "custom_id",
    tags = {"app:kong"}
  },
  {
    name = "upstream_latency",
    stat_type = "timer",
    tags = {"app:kong"}
  },
  {
    name = "kong_latency",
    stat_type = "timer",
    tags = {"app:kong"}
  }
}

-- entries must have colons to set the key and value apart
local function check_tag_value(value)
  if value == nil then
    return true
  end

  for _, entry in ipairs(value) do
    local ok = find(entry, ":")
    if ok then
      local _, next = pl_utils.splitv(entry, ":")
      if not next or #next == 0 then
        return nil, "key '" .. entry .. "' has no value"
      end
    end
  end

  return true
end

local function has_value(tab, val)
  for index, value in ipairs(tab) do
    if value == val then
      return true
    end
  end

  return false
end

local function check_schema(value)
  for _, entry in ipairs(value) do
    if not entry.name or not entry.stat_type then
      return false, "name and stat_type must be defined for all stats"
    end

    if not has_value(STAT_NAMES, entry.name) then
      return false, "unrecognized metric name: " .. entry.name
    end

    if not has_value(STAT_TYPES, entry.stat_type) then
      return false, "unrecognized stat_type: " .. entry.stat_type
    end

    local tag_ok, tag_error = check_tag_value(entry.tags)
    if not tag_ok then
      return false, "malformed tags: " .. tag_error .. ". Tags must be list of key[:value]"
    end

    if entry.name == "unique_users" and entry.stat_type ~= "set" then
      return false, "unique_users metric only works with stat_type 'set'"
    end

    if
      (entry.stat_type == "counter" or entry.stat_type == "gauge") and
        ((not entry.sample_rate) or (entry.sample_rate and type(entry.sample_rate) ~= "number") or
          (entry.sample_rate and entry.sample_rate < 1))
     then
      return false, "sample rate must be defined for counters and gauges."
    end

    if
      (entry.name == "status_count_per_user" or entry.name == "request_per_user" or entry.name == "unique_users") and
        not entry.consumer_identifier
     then
      return false, "consumer_identifier must be defined for metric " .. entry.name
    end

    if
      (entry.name == "status_count_per_user" or entry.name == "request_per_user" or entry.name == "unique_users") and
        entry.consumer_identifier
     then
      if not has_value(CONSUMER_IDENTIFIERS, entry.consumer_identifier) then
        return false, "invalid consumer_identifier for metric '" ..
          entry.name .. "'. Choices are consumer_id, custom_id, and username"
      end
    end

    if
      (entry.name == "status_count" or entry.name == "status_count_per_user" or entry.name == "request_per_user") and
        entry.stat_type ~= "counter"
     then
      return false, entry.name .. " metric only works with stat_type 'counter'"
    end
  end

  return true
end

return {
  no_consumer = true,
  fields = {
    host = {
      required = true,
      type = "string",
      default = "localhost"
    },
    port = {
      required = true,
      type = "number",
      default = 8125
    },
    metrics = {
      new_type = {
        type = "array",
        required = true,
        default = DEFAULT_METRICS,
        elements = {
          type = "record",
          fields = {
            {name = {type = "string", required = true, one_of = STAT_NAMES}},
            {stat_type = {type = "string", required = true, one_of = STAT_TYPES}},
            {tags = {type = "array", elements = {type = "string", match = "^.*[^:]$"}}},
            {sample_rate = {type = "number", between = {0, 1}}},
            {consumer_identifier = {type = "string", one_of = CONSUMER_IDENTIFIERS}}
          },
          entity_checks = {
            {
              conditional = {
                if_field = "name",
                if_match = {eq = "unique_users"},
                then_field = "stat_type",
                then_match = {eq = "set"}
              }
            },
            {
              conditional = {
                if_field = "stat_type",
                if_match = {one_of = {"counter", "gauge"}},
                then_field = "sample_rate",
                then_match = {required = true}
              }
            },
            {
              conditional = {
                if_field = "name",
                if_match = {one_of = {"status_count_per_user", "request_per_user", "unique_users", "latency_per_user"}},
                then_field = "consumer_identifier",
                then_match = {required = true}
              }
            },
            {
              conditional = {
                if_field = "name",
                if_match = {one_of = {"status_count", "status_count_per_user", "request_per_user"}},
                then_field = "stat_type",
                then_match = {eq = "counter"}
              }
            }
          }
        }
      },
      required = true,
      type = "array",
      default = DEFAULT_METRICS,
      func = check_schema
    },
    prefix = {
      type = "string",
      default = "kong"
    }
  }
}
