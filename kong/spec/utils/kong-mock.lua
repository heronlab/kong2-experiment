-- Since most of our unit tests don't have the global _G.kong
-- object instantiated, we often had to create default mock
-- for _G.kong in each test file.
--
-- This object is meant to be the standard default mock for
-- all our tests. It should be used at the top of your test
-- files as follow:
--
-- _G.kong = require("kong.spec.utils.kong-mock")
--
-- NOTE: I don't have enough time to setup a full _G.kong
-- object following the pdk so you'll need to extend this
-- object incrementally as needed until it is fully compatible
return {
  -- kong.ctx
  -- Reference: https://docs.konghq.com/1.0.x/pdk/kong.ctx
  ctx = {
    shared = {},
    plugin = {},
  },
  -- kong.request
  -- Reference: https://docs.konghq.com/1.0.x/pdk/kong.request
  request = {
    get_scheme = function()
    end,
    get_host = function()
    end,
    get_port = function()
    end,
    get_forwarded_scheme = function()
    end,
    get_forwarded_host = function()
    end,
    get_forwareded_port = function()
    end,
    get_http_version = function()
    end,
    get_method = function()
    end,
    get_path = function()
    end,
    get_path_with_query = function()
    end,
    get_raw_query = function()
    end,
    get_query_arg = function()
    end,
    get_query = function(max_args)
    end,
    get_header = function(name)
    end,
    get_headers = function(max_headers)
    end,
    get_raw_body = function()
    end,
    get_body = function(mimetype, max_args)
    end,
  },
  -- kong.response
  -- Reference: https://docs.konghq.com/1.0.x/pdk/kong.response
  response = {
    get_status = function(status, body, headers)
    end,
    get_header = function(name)
    end,
    get_headers = function(max_headers)
    end,
    get_source = function()
    end,
    set_status = function(status)
    end,
    set_header = function(name, value)
    end,
    add_header = function(name, value)
    end,
    clear_header = function(name)
    end,
    set_headers = function(headers)
    end,
    exit = function(status, body, headers)
    end,
  },
}
