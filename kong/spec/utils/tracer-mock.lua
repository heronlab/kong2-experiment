return {
  binary_extract = function(...)
  end,
  http_headers_inject = function(...)
  end,
  start_span = function(...)
    return {
      finish = function(...)
      end
    }
  end
}
