local function clear_table(table)
  for k in pairs(table) do
    table[k] = nil
  end
end

return {
  clear_table = clear_table
}
