local logger = {}

function logger:err()
end

function logger:err_with_context()
end

function logger:warn()
end

function logger:warn_with_context()
end

function logger:debug()
end

function logger:debug_with_context()
end

function logger:notice()
end

function logger:notice_with_context()
end

function logger:info()
end

function logger:info_with_context()
end

function logger:inspect()
end

return logger
