local function mock(spy)
    local call_backs = {}
    local args_list = {}

    local function at(time, cb, ...)
        local i = #call_backs + 1
        call_backs[i] = cb
        local args = {...}
        args_list[i] = args
    end

    local function runAllTimers(premature)
        local premature = premature or false
        for i = 1, #call_backs, 1 do
            local cb = call_backs[i]
            local arg = args_list[i]
            cb(premature, unpack(arg))
        end
        call_backs = {}
        args_list = {}
    end
    ngx.timer.at = spy.new(at)
    ngx.timer.runAllTimers = runAllTimers
end

return {
    mock = mock
}
