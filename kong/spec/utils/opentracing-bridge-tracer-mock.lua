local tracer_mock = require("kong.spec.utils.tracer-mock")

local mock = {
  new_from_global = function(...)
    return tracer_mock
  end
}

return mock
