FROM kong:2.0.0rc1-ubuntu

# Add configurations & plugins
ADD kong /kong-plugins/kong
ADD nginx.template /usr/local/kong/nginx.template
