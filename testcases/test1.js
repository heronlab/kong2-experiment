import http from "k6/http";
import { check } from "k6";

var kongIP = "20.43.170.64";

if (__ENV.KONG_IP) {
  kongIP = __ENV.KONG_IP;
}

const url = "http://" + kongIP + "/greeter";

export default function () {
  let res = http.get(url);
  check(res, {
    "status is 200": r => r.status === 200,
  })
}
