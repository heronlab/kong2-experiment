# Kong2 experiment

This project aims to test Kong2 on K8s. We set up a basic infrastructure on K8s consist of:

- [postgres 10.4](https://hub.docker.com/_/postgres)
- [kong2](https://bintray.com/kong/kong-preview/kong)
- [konga](https://github.com/pantsel/konga)
- [gogreeter (an testing http echo service)](https://gitlab.com/phuonghuynh-eng/gogreeter)

In order to test this stack, you need to spin up a k8s cluster. This guide assumes that you already have a k8s working cluster and take authorized. And make an alias `k` to `kubectl` as well

```shell
alias k=kubectl
```

## Steps to run

- Prepare postgres DB: `k apply -f postgres.k8s.yml` and waiting for the DB is ready
- Deploy kong: `k apply -f postgres.k8s.yml`
- Deploy konga: `k apply -f konga.k8s.yml`
- Deploy test service: `k apply -f http-echo.k8s.yml`

If we deploy konga service as a ClusterIP type, we can forward its port for testing locally

```
k port-forward services/konga 1337:1337
```

Then we can access `localhost:1337` and testing with the full kong/konga stack (obviously kong admin server at address `http://kong2-preview1-internal:8001`)

The kong api will be published at `loadbalancer_ip` (as described in `kong.k8s.yml:service:kong2-preview1`)

## Install ingress controller (to access konga without needing port-forward)

Prefer this document: https://kubernetes.github.io/ingress-nginx/deploy/
